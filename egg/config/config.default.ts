import { EggAppConfig, EggAppInfo, PowerPartial } from 'egg';

export default (appInfo: EggAppInfo) => {
  const config = {} as PowerPartial<EggAppConfig>;

  // override config from framework / plugin
  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1559974590356_695';

  // add your egg config in here
  //添加全局中间件
  config.middleware = ['gzip', 'auth'];
  config.gzip = {
    threshold: 1024, // 小于 1k 的响应体不压缩
  };
  config.auth = {
    match:'/api/auth'
  };

  // cookie 秘钥供加密使用
  config.keys = 'key1, key2';

  // session 的设置
  config.session = {
      key: 'sessionId',          // 表存储 Session 的 Cookie 键值对的 key 是什么
      maxAge: 24 * 3600 * 1000, // 1 天
      httpOnly: true,
      encrypt: true,
  };

  // add your special config in here
  const bizConfig = {
    sourceUrl: `https://github.com/eggjs/examples/tree/master/${appInfo.name}`,
  };
  // 设置启动端口号和host
  config.cluster = {
    listen: {
      path: '',
      port: 8999,
      hostname: '0.0.0.0',
    },
  };
  // close csrf
  config.security = {
    csrf: {
      enable: false,
    },
  };
  // the return config will combines to EggAppConfig
  return {
    ...config,
    ...bizConfig,
  };
};

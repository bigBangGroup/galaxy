
module.exports = _options => {
    return async function gzip(ctx, next) {
        const auth = ctx.header.cookie.indexOf('x-auth') > -1;
        const authUrl = (ctx.request.url === '/api/auth/authorization');
        if(auth || authUrl){
            await next();
        }else {
            ctx.body = {
                errorCode: -999,
                errorMessage: '当前用户暂无权限, 授权之后再操作吧~'
            }
        }
    };
};

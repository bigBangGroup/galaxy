import { Application } from 'egg';
import { initRouter } from 'egg-router-decorator';

export default (app: Application) => {
  initRouter(app, { prefix: '/api' });
  (app.env === 'test' || app.env === 'prod') && app.router.get('/', async ctx => {
    await ctx.render('index');
  });
};

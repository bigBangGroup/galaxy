import { Controller } from 'egg';
import routerDecorator from 'egg-router-decorator';

/*
  业务中的用户信息
*/
@routerDecorator.prefix('/user')
export default class StatisticController extends Controller {

    @routerDecorator.get('/affinity')
    public async getUserAffinity() {
        const { ctx } = this;
        // 获取传递过来的参数
        const { query } = ctx;
        console.log(query);
        const userAffinity = await ctx.service.user.userAffinity();
        ctx.body = {
            errorCode: 0,
            payload: userAffinity,
        };
    }

    @routerDecorator.get('/list')
    public async getUserList() {
        const { ctx } = this;
        // 获取传递过来的参数
        const { query } = ctx;
        console.log(query);
        const userList = await ctx.service.user.userList();
        ctx.body = {
            errorCode: 0,
            payload: userList,
        };
    }

    @routerDecorator.post('/update')
    public async updateUserInfo() {
        const { ctx } = this;
        // 获取传递过来的参数
        const { query } = ctx;
        await ctx.service.user.userUpdate(query)
        ctx.body = {
            errorCode: 0,
            payload: {
                message: '更新用户信息成功',
            },
        };
    }

    @routerDecorator.post('/add')
    public async addUser() {
        const { ctx } = this;
        // 获取传递过来的参数
        const { query } = ctx;
        await ctx.service.user.userAdd(query)
        ctx.body = {
            errorCode: 0,
            payload: {
                message: '新增用户信息成功',
            },
        };
    }

    @routerDecorator.del('/del')
    public async delUserInfo() {
        const { ctx } = this;
        // 获取传递过来的参数
        const { query } = ctx;
        await ctx.service.user.userDel(query.id);
        ctx.body = {
            errorCode: 0,
            payload: {
                message: '删除用户信息成功',
            },
        };
    }

    @routerDecorator.put('/restore')
    public async restoreUserList() {
        const { ctx } = this;
        // 获取传递过来的参数
        const { query } = ctx;
        const initUserList = await ctx.service.user.initUserList(query.type);
        ctx.body = {
            errorCode: 0,
            payload: initUserList
        };
    }
}

import { Controller } from 'egg';
import routerDecorator from 'egg-router-decorator';

/*
  业务中需要验证权限的接口
*/
@routerDecorator.prefix('/auth')
export default class AuthController extends Controller {

    @routerDecorator.get('/authorization')
    public async getAuthorization() {
        const { ctx } = this;
        // 获取传递过来的参数
        ctx.cookies.set(`x-auth`, `auth${+ new Date()}`, {
            httpOnly: false,
            signed: false,
        });
        ctx.body = {
            errorCode: 0,
            payload: {
                message:'授权成功'
            },
        }
    }

    @routerDecorator.get('/remove')
    public async removeAuthorization() {
        const { ctx } = this;
        // 获取传递过来的参数
        ctx.cookies.set(`x-auth`, null as any, {
            httpOnly: false,
            signed: false,
        });
        ctx.body = {
            errorCode: 0,
            payload: {
                message:'移除权限成功'
            },
        }
    }

    @routerDecorator.get('/getData')
    public async getRevenue() {
        const { ctx } = this;
        ctx.body = {
          errorCode: 0,
          payload:{
              message:'success~'
          }
        }
    }
}

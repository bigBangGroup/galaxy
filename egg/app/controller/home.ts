import { Controller } from 'egg';
import routerDecorator from 'egg-router-decorator';

@routerDecorator.prefix('/get')
export default class HomeController extends Controller {
  @routerDecorator.get('/list')
  public async getlist() {
    const { ctx } = this;
    console.log(ctx);
    ctx.body = {
      errCode: 0,
      payload: {
        list: [{
          name: 'yang',
          age: 1,
        },
        {
          name: 'cyril',
          age: 22,
        },
        ],
      },
    };
  }
}

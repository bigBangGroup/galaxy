import { Controller } from 'egg';
import routerDecorator from 'egg-router-decorator';

/*
  cookie 模块
*/

@routerDecorator.prefix('/session')
export default class SessionController extends Controller {

    @routerDecorator.get('/set')
    public async addCookie() {
        const { ctx } = this;
        ctx.session.sessionId = ctx.session.sessionId ? `session_id_${+new Date()}` : 'session_id';
        ctx.body = {
            errorCode: 0,
            payload: {
                message:'设置 session 成功'
            },
        }
    }

    @routerDecorator.get('/destroy')
    public async destroyCookie() {
        const { ctx } = this;
        ctx.session.sessionId = null;
        ctx.body = {
            errorCode: 0,
            payload: {
                message: '服务端清空 session 成功'
            },
        }
    }

}

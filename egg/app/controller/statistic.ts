import { Controller } from 'egg';

/*
  业务中的统计模块功能
*/

/*** company revenue
 * @param year：得到该年的收益情况.
*/
export default class StatisticController extends Controller {
  public async getRevenue() {
    const { ctx } = this;
    // 获取传递过来的参数
    const { query } = ctx;
    console.log(query)
    ctx.body = await ctx.service.revenue.getRevenueByYear(query.year);
    // ctx.body = {
    //   errorCode: 1,
    //   errorMsg: 'errorCode'
    // }
  }
}

import { Controller } from 'egg';
import routerDecorator from 'egg-router-decorator';

/*
  cookie 模块
*/

@routerDecorator.prefix('/cookie')
export default class CookieController extends Controller {

    @routerDecorator.get('/set')
    public async addCookie() {
        const { ctx } = this;
        ctx.cookies.set(`x-token`, `testUser_${+ new Date()}`, {
            httpOnly: false,
            signed: false,
        });
        ctx.body = {
            errorCode: 0,
            payload: '设置cookie 成功',
        }
    }

    @routerDecorator.get('/encrypt')
    public async encryptCookie() {
        const { ctx } = this;
        ctx.cookies.set(`x-token`, `testUser_${+ new Date()}`, {
            httpOnly: false,
            encrypt: true, // 加密传输
        });
        ctx.body = {
            errorCode: 0,
            payload: '设置cookie 成功',
        }
    }
}

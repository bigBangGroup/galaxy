import { Service } from 'egg';

interface IUserInfo {
  id?: number;
  name: string;
  role: string;
  age: number;
  country: string;
}

/**
 * User Service
 */
export default class Test extends Service {

  /**
   * get User affinity
   */
  public async userAffinity() {
    return userAffinity;
  }

  /**
   * get User list
   */
  public async userList() {

    return mockUserList;
  }

  /**
   * add User info
   */
  public async userAdd(userInfo: IUserInfo) {
    mockUserList.unshift(userInfo);
    mockUserList.map((userInfo:IUserInfo, index:number) => userInfo.id = (index + 1))
  }

  /**
   * update User info
   */
  public async userUpdate(userInfo: IUserInfo) {
    mockUserList.map((uInfo: IUserInfo, index: number) => {
      if (userInfo.id == uInfo.id) {
        mockUserList[index] = userInfo;
      }
    });
  }

  /**
   * del User info
   */
  public async userDel(uid: number) {
    mockUserList.map((userInfo: IUserInfo, index: number) => {
      if (userInfo.id == uid) {
        mockUserList.splice(index, 1);
      }
    });
  }

  /**
   * init user list
   */
  public async initUserList(type: string) {
    if(type !== 'userList') return;
    mockUserList = [...initUserList];
    return initUserList
  }
}

let userAffinity = {
  affinity:100
};

const initUserList: IUserInfo[] = [
  { id: 1, name: '曹操', role: '主公', age: 53, country: '魏国' },
  { id: 2, name: '刘备', role: '主公', age: 43, country: '蜀国' },
  { id: 3, name: '孙权', role: '主公', age: 33, country: '吴国' },
  { id: 4, name: '诸葛', role: '文臣武将', age: 23, country: '蜀国' },
  { id: 5, name: '司马懿', role: '文臣武将', age: 56, country: '魏国' },
  { id: 6, name: '张飞', role: '武将', age: 26, country: '蜀国' },
  { id: 7, name: '关羽', role: '武将', age: 27, country: '蜀国' },
  { id: 8, name: '鲁肃', role: '文臣', age: 25, country: '吴国' },
  { id: 9, name: '黄忠', role: '武将', age: 58, country: '蜀国' },
  { id: 10, name: '马超', role: '武将', age: 35, country: '蜀国' },
  { id: 11, name: '子龙', role: '武将', age: 23, country: '蜀国' },
  { id: 12, name: '张辽', role: '武将', age: 23, country: '魏国' },
  { id: 13, name: '周瑜', role: '儒将', age: 23, country: '吴国' },
  { id: 14, name: '许褚', role: '武将', age: 23, country: '魏国' },
  { id: 15, name: '夏侯渊', role: '武将', age: 23, country: '魏国' },
];

let mockUserList: IUserInfo[] = [...initUserList];

import { Service } from 'egg';

/**
 * Test Service
 */
export default class Revenue extends Service {

    /**
     * company revenue
     * @param year - number
     */
    public async getRevenueByYear(year: number) {
        return mockRevenue[year];
    }
}

// mock
const mockRevenue = {
    2019: {
        income: {
            budget: 100,
            actual: 120,
            target: 80,
        },
        rate: {
            budget: 35,
            actual: 50,
            target: 65,
        },
        user: {
            budget: 1200,
            actual: 930,
            target: 900,
        },
    },
    2018: {
        a: 11,
        b: 22,
        c: 33,
    },
}

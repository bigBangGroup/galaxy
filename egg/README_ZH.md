# egg note

## init project
run command
```
egg-init egg --type=ts
```

## app文件目录结构

app的文件目录结构遵循egg的约定。每个文件夹都有其特定的作用。
按照约定的文件结构去写，在别的地方无需引入，直接用即可。

### router.ts
路由配置文件，所有路由匹配的模块都需要写在控制器中（即controller中）
一个路由对应到某个控制器.

### controller （负责处理业务逻辑 MVC中的 C ）
控制器文件夹
约定：
+ 控制器的class名称首字母大写 
+ 和文件名保持一致
+ 加后缀 Controller
+ 继承egg中的 Controller

point:
通过 
```
this.ctx.body = '返回给用户响应的信息'
```

### middleware
该文件夹也不是egg脚手架生成的，是自己加上去的。
中间件，匹配路由之前或者之后做的一些事情。

### extend
也非脚手架生成的文件夹
扩展功能的集合

### 定时任务
egg中的定时任务

### public
放置静态资源，例如 css, js, image...
可以直接将前端打包成静态资源，然后丢入public文件夹下。
该例子可以通过 http://localhost:8999/public/build/index.html#/
访问打包后的前端文件

### service （和数据的交互,查询数据库，请求数据 MVC中的 M mode表模型，egg中用服务来创建模型）

### view （视图，模板，负责页面的展示 MVC中的 V）
该文件夹不是egg脚手架生成的
后面尝试将前端打包之后的文件放在下面
放置前端模板

## config
配置文件

## logs
日志文件，一般不用管.

## run
运行项目的时候生成的需要的一些配置文件，一般不用管

## test
测试文件,测试用例

## .autod.conf.js
egg调用的配置文件，一般也无需管

## .travis.yml && appveyor.yml
egg夸平台的配置文件，一般也不用管
 
## post 请求关闭 csrf 报错
config/config.default.ts
中增加如下代码
```$xslt
 // close csrf 
  config.security = {
    csrf: {
      enable: false,
    },
  };
``` 
 



// This file is created by egg-ts-helper@1.25.3
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportAuth from '../../../app/controller/auth';
import ExportCookie from '../../../app/controller/cookie';
import ExportHome from '../../../app/controller/home';
import ExportSession from '../../../app/controller/session';
import ExportStatistic from '../../../app/controller/statistic';
import ExportUser from '../../../app/controller/user';

declare module 'egg' {
  interface IController {
    auth: ExportAuth;
    cookie: ExportCookie;
    home: ExportHome;
    session: ExportSession;
    statistic: ExportStatistic;
    user: ExportUser;
  }
}

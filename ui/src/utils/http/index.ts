import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import ErrHandler from './../error';


ErrHandler.init();
enum IHttpMethods {
    GET = 'get',
    POST = 'post',
    DELETE = 'delete',
    PUT = 'put',
}

const methods = ["get", "post", "delete", "put", "patch"];

const withDataMethods = ['post', 'put', 'patch'];

interface IHttpFn {
    <T = any>(url?: string, config?: AxiosRequestConfig): Promise<T>
}

type IHttp = Record<IHttpMethods, IHttpFn>;

const http: IHttp = methods.reduce((map: any, method: string) => {
    map[method] = (url: string, options: AxiosRequestConfig = {}, payLoad:any={}) => {
        const { data, ...config } = options;

        // 添加request headers 认证信息
        config.headers = {
            glaxy: 'glaxy_121221',
            bigBang: 'bangBang_21212mark',
            'x-csrf-token': 'x-csrf-token_12122'
        };

        // 配置axios超时的时间，需要在下面的promise 之前配置
        config.timeout = 10000;

        // axios的请求分两种，一种是带data的，另一种是不带data的，如果有参数是放在config中的params中携带的。
        const promise = withDataMethods.indexOf(method) > -1
            ? (axios as any)[method](url, data, config)
            : (axios as any)[method](url, config);

        return promise
            .then((res: AxiosResponse) => {
               // 正常业务返回结果
               if (res.data.status === 1) {
                    //留个口子，防止需要返回整个axios的结果
                    const { entirety } = payLoad
                    return entirety ? res : res.data.data;
                }
                // 其余情况就全部抛出异常，自然在error中会捕获到
                // 包括http code 错误码是500 不需要在这里使用第二个参数捕获，
                // http code 500 可以在error中处理
                throw res.data;
            });
    };
    return map
}, {});

export default http;

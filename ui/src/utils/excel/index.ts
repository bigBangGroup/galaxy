import { utils, write } from 'xlsx';

//传入的 dataList 是二维数组
export async function exportListToXlsx (dataList: string[][],merges?:any, filename = 'test.xlsx', sheetName = 'sheet1') {
    // json_to_sheet 将对象数组转化为sheet
    console.log(dataList);
    // 将二维数组转化为sheet

    const ws = utils.aoa_to_sheet(dataList);
    // const ws = utils.json_to_sheet(data);

    if(merges){
        ws["!merges"] = merges;
    }
    console.log(ws);
    ws.A1 = {
        v:'<img src="http://apps.danlu.netease.com:12102/dsp/static/media/afire-logo-white.7feeabeb.png">', t:"cellHTML"
    };
    ws['A1'].l = { Target:"https://www.163.com", Tooltip:"Find us @ www.163.com!" };
    ws['A1'].l = { Target:"#12e" }; /* link to cell E2 */

    console.log(ws);
    // 创建虚拟的workbook.
    const wb = utils.book_new();
    // 将 sheet 添加到workbook中
    utils.book_append_sheet(wb, ws, sheetName);
    const wbout = write(wb, { bookType: 'xlsx', bookSST: false, type: 'binary' });

    downloadBlob(new Blob([s2ab(wbout)], { type: 'application/octet-stream' }), filename);
}

export function downloadBlob (blob: Blob, filename: string) {
    let a: HTMLAnchorElement | null = document.createElement('a');
    const url = URL.createObjectURL(blob);

    a.href = url;
    a.download = filename;
    a.click();

    // don't forget to free memory up when you're done (you can do this as soon as image is drawn to canvas)
    URL.revokeObjectURL(url);
    a = null;
}

// 将字符串转ArrayBuffer
function s2ab (s: any) {
    /* eslint-disable */
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);

    for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;

    return buf;
}

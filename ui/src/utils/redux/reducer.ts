

/**
 * 生成reducer函数
 * @export
 * @param {any} initState 初始state
 * @param {any} handlers 处理各个action type 的函数集合，以键值对形式表示。键：action的type，值：reducer纯函数
 * @returns 整合好的reducer
 */
export function createReducer (initState:any, handlers:any) {
    return function (state = initState, action:any) {
        if (handlers.hasOwnProperty(action.type)) {
            return handlers[action.type](state, action);
        }
        return state;
    }
}

/**
 *  捕获 http 请求返回结果中 promise 中抛出的异常
 *  错误码，错误信息统一处理
 *
 * */

import { message } from 'antd';

class ErrHandler {

    static init = ()=>{
        window.addEventListener('unhandledrejection', async (event:any) => {
            event.preventDefault();
            const reason = event.reason || {};

            // 服务端业务报错的业务错误码都是负数
            if(reason.status && reason.status<0){
                message.error(`服务端业务报错, 错误码：${reason.status}, 错误信息:${reason.data}`);
                return
            }
            if(reason.code === "ECONNABORTED"){
                message.error(`接口超时:${reason.config.url}`);
                return
            }
             // http code 500
             if(reason.response && reason.response.status === 500){
                message.error(`接口报错${reason.message}`);
                return
            }
        })
    }

}

export default ErrHandler

import React, { Component } from "react";
import './../scss/Form.scss';

interface FormLearnProps {}

interface FormLearnStatus {
    inpVal0: string
    inpVal: string
    userName: string
}

class FormLearn extends Component<FormLearnProps, FormLearnStatus> {
  constructor(props: any) {
    super(props);

    this.state = {
      inpVal0: "无法输入,不信试试？",
      inpVal: "inpVal",
      userName: "输入框中改变我的值试试"
    };
  }

  inputChange = (e: any) => {
    // let val = this.refs.inputRef.value;
    let val = e.target.value;
    this.setState({
      inpVal: val
    });
  };

  inputKeyDown = (e:any) => {
    console.log(e.keyCode);
    if (e.keyCode == 13) {
      alert(e.target.value);
    }
  };

  inputKeyUp = (e:any) => {
    console.log(e.keyCode);
    if (e.keyCode == 13) {
      alert(e.target.value);
    }
  };

  changeName = (e:any) => {
    let val = e.target.value;
    this.setState({
      userName: val
    });
  };

  render() {
    return (
      <div className="form-learn-container container">
        <div className="demo-wrap">
          <div className="title">1：react中的表单元素的使用</div>
          <div className="demo-con">
            <div>
              在react中如果一个表单控件没有做相应的处理，你会发现是无法输入的，以下面为例子
            </div>
            <div>
              <p>
                这是一个普通的input
                输入框，绑定了this.state.val的值，可是我无法被输入：
              </p>
              <input
                className="cls-input"
                type="text"
                value={this.state.inpVal0}
              />
              <p>
                此时你也可以打开控制台浏览器会发现已近报错了，会得到一个如下的错误:
              </p>
              <p className="red">
                index.js:1446 Warning: Failed prop type: You provided a `value`
                prop to a form field without an `onChange` handler. This will
                render a read-only field. If the field should be mutable use
                `defaultValue`. Otherwise, set either `onChange` or `readOnly`.
              </p>
            </div>
          </div>

          <div className="demo-con">
            <p>正确的使用方式如下：</p>
            <p>
              1：给input表单控件绑定this.state上的初始变量（这里是 inpVal ）
            </p>
            <p>
              2：给input表单控件绑定 onChange 事件，在回调方法中去改变 inpVal
              的值
            </p>
            <p>
              3：这里说明下，在上述 onChange 的回调事件中可以通过 e.target.value
              的方式来获取，也可以通过
              this.refs.inputRef.value;的方式来获取（inputRef是通过
              ref="inputRef"的方式绑定到表单元素上的）
            </p>
            <div>
              <input
                type="text"
                ref="inputRef"
                value={this.state.inpVal}
                onChange={this.inputChange}
                className="mt10"
              />
            </div>
          </div>
        </div>

        <div className="demo-wrap">
          <div className="title">2：表单元素的键盘事件</div>
          <div className="demo-con">
            <div>这里也是以input的表单元素为例说明</div>
            <div>
              <p>
                我是一个普通的表单元素，绑定了onKeyDown事件。可以按下Enter键，或者打开控制台查看输出
              </p>
              <input
                onKeyDown={this.inputKeyDown}
                type="text"
                className="mg10"
              />
            </div>
            <div>
              <p>
                我是一个普通的表单元素，绑定了onKeyUp事件。可以按下Enter键，或者打开控制台查看输出
              </p>
              <input onKeyUp={this.inputKeyUp} type="text" className="mg10" />
            </div>
          </div>
        </div>

        <div className="demo-wrap">
          <div className="title">3：实现一个简单的双向数据绑定</div>
          <div className="demo-con">
            <p>
              双向数据绑定：model层改变影响view层展示，view改变反过来又会影响model层
            </p>
            <p>
              在vue中实现双向数据绑定，只需要简单的v-model即可实现，但是在react中没有类似的提供，所以需要自己实现，这里简单来个demo实现一下
            </p>
            <p>
              下面展示userName的值,并改变input输入框的值，来查看效果:{" "}
              <span className="val-show">{this.state.userName}</span>
            </p>
            <input
              type="text"
              value={this.state.userName}
              onChange={this.changeName}
              className="mg10"
            />
          </div>
        </div>
      </div>
    );
  }
}

export default FormLearn;

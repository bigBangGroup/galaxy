import React, { Component } from 'react';
import SonComponent from './DefaultProps/SonComponent'
import SonComponent2 from './DefaultProps/SonComponent2'
import './../scss/DefaultProps.scss';


interface DefaultPropsProps {

}
interface DefaultPropsStatus {
    pVal:string
    str:string
    pVal2:string
}
class DefaultProps extends Component<DefaultPropsProps, DefaultPropsStatus> {
    constructor(props:any){
        super(props);
        this.state = {
            pVal:'我是父组件中的值pVal',
            str:'子类.propTypes = {name: PropTypes.string}',
            pVal2:'我是父组件中的 pVal2 ，传递给子组件，但是我是一个字符串而非数值。'
        }

    }
    render() {
        return (
           <div className='default-props-container container'>
               <div className="item-demo">
                   <div className='title'>1：默认属性值 defaultProps</div>
                   <div className='demo-content pl20 mt10'>
                       这里讲述一下 默认属性值 defaultProps的作用，即在子组件中如果父组件传递过来了这个属性值，那么就使用父组件传递过来的值，如果
                       父组件没有传递过来的话，那就使用一个默认值来代替。
                       在这里设计的demo如下思路，在这个组件中(父组件)会实例化一个子组件，一个是传值（prop）给子组件，一个是不传值给子组件，那么不传之的子组件就
                       会显示一个默认的 defaultProps 值

                       <div className='item-wrap'>
                           <p>传值给子组件的demo: </p>
                           <SonComponent pVal={this.state.pVal} />
                       </div>

                       <div className='item-wrap'>
                           <p>不传值给子组件的demo: </p>
                           <SonComponent />
                       </div>

                   </div>
               </div>

               <div className="item-demo">
                   <div className="title">2：propTypes 验证父组件传值的类型合法性</div>
                   <div className='demo-content pl20 mt10'>
                       <p>这个功能主要是在父组件中调用子组件，给子组件传值的时候验所传值的合法性，使用起来很easy，按照下面步骤使用即可：</p>
                       <p>1、子组件引入import PropTypes from 'prop-types'; 不必重新安装，默认就有这个，所以直接引用即可。</p>
                       <div>
                           <p>2、子组件中按照如下方式使用即可</p>
                           <p className='ml10'>{this.state.str}</p>
                       </div>
                       <div className='item-wrap'>
                           <p>设计的demo思路是，在改组件中(父组件)调用子组件，并给子组件传递一个字符串，
                               但是子组件会在内部对该属性做是否是数字的验证，这样在子组件中是会爆红
                           </p>
                           <div>
                               <SonComponent2 pVal2={this.state.pVal2}></SonComponent2>
                           </div>
                           <div>
                               这个时候打开控制台，你会发现报如下错误：
                               <p className='red'>index.js:1446 Warning: Failed prop type: Invalid prop `pVal2` of type `string` supplied to `SonComponent2`, expected `number`.</p>
                               <p>原因就是父组件传递够来的数据类型和子组件验证的不一致导致的，如果将父组件中传递过来的值改成number类型，就不会报该错误。</p>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
        );
    }
}

export default DefaultProps;

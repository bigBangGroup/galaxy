import React, { Component } from 'react';

interface SonComponentProps {
    title?:string
}

interface SonComponentStatus {
    style:any
}
class SonComponent extends Component<SonComponentProps, SonComponentStatus> {
    constructor(props:any){
        super(props);
       this.state = {
           style:{
               backgroundColor: '#5bc0de',
               padding:'10px'
           }
       }
    }

    // 在父组件里面改变props传值的时候触发的
    componentWillReceiveProps(){
        console.log('父子组件传值，父组件里面改变了props的值触发的方法')
    }

    //组件销毁的时候触发的生命周期函数   用在组件销毁的时候执行操作 父组件销毁子组件 会在子组件中触发该生命周期
    componentWillUnmount(){
        console.log('我是子组件，我在父组件中被销毁了。');
    }

    render() {
        return (
           <div className='son-component-container' style={this.state.style}>
               <p> 我是用来测试的子组件,可点击上述按钮来切换是否 挂在/销毁 我。查看相应的生命周期</p>
               <p className='mt10'><span className='val-show'>{this.props.title}</span></p>
           </div>
        );
    }
}

export default SonComponent;

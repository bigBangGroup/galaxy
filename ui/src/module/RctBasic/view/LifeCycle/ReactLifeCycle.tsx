import React, { Component } from 'react';
import SonComponent from './SonComponent'

interface ReactLifeCycleProps{

}
interface ReactLifeCycleStatus {
    flag:boolean
    msg:string
    title:string
}
class ReactLifeCycle extends Component<ReactLifeCycleProps, ReactLifeCycleStatus> {
    constructor(props:any){
        super(props);
        console.log('1: 触发构造函数(我不属于生命周期函数)');
        this.state = {
            flag:true,
            msg:'我是初始的msg的值',
            title:'我是父组件传递给子组件的title值'
        }
    }

    changeMsg = () => {
        this.setState({
            msg:'我是更新之后的msg的值'
        })
    }

    toggleFlag = () => {
        this.setState({
            flag: !this.state.flag
        })
    }

    changeTitle = () => {
        this.setState({
            title: '我是父组件传递给子组件改变后的title值 ' + (+new Date())
        })
    }

    //组件将要挂载的时候触发的生命周期函数
    componentWillMount(){
        console.log('2: 组件将要挂载的时候触发的生命周期函数');
    }

    //组件挂载完成的时候触发的生命周期函数
    componentDidMount(){
        //dom操作放在这个里面    请求数据也放在这个里面
        console.log('4: 组件挂载完成的时候触发的生命周期函数');
    }

    //是否要更新数据  如果返回true才会执行更新数据的操作， 返回false的话就不会更新了。
    shouldComponentUpdate(nextProps:any, nextState:any){
        console.log(nextProps);
        console.log(nextState);
        console.log('01是否要更新数据');
        return true;
    }

    //将要更新数据的时候触发
    componentWillUpdate(){
        console.log('02：组件将要更新');
    }

    //组件更新完成
    componentDidUpdate(){
        console.log('04：组件数据更新完成');
    }

    // 在父组件里面改变props传值的时候触发的
    componentWillReceiveProps(){
        console.log('父子组件传值，父组件里面改变了props的值触发的方法')
    }

    //组件销毁的时候触发的生命周期函数   用在组件销毁的时候执行操作 父组件销毁子组件 会在子组件中触发该生命周期
    componentWillUnmount(){
        console.log('组件销毁了');
    }

    render() {
        console.log('3: 数据渲染render(我不属于生命周期函数)');
        return (
           <div className='life-cycle-container'>
               <div className="demo-wrap">
                   <p className='title'>
                       react中的生命周期函数
                   </p>
                   <div className="demo-content pl20 mt10">
                        <p>
                            组件加载之前，组件加载完成，以及组件更新数据，组件销毁。
                            触发的一系列的方法 ，这就是组件的生命周期函数，下面按照时间的这个维度，罗列一下所有的生命周期函数
                        </p>

                       <div className='item mt20'>
                           <p>组件加载的时候触发的函数： </p>
                           <p>constructor 、componentWillMount、 render 、componentDidMount</p>
                       </div>

                       <div className='item mt20'>
                           <p>组件数据更新的时候触发的生命周期函数： </p>
                           <p>shouldComponentUpdate、componentWillUpdate、render、componentDidUpdate</p>
                       </div>

                       <div className='item mt20'>
                           <p>在父组件里面改变props传值的时候触发的： </p>
                           <p>componentWillReceiveProps</p>
                       </div>

                       <div className='item mt20'>
                           <p>组件销毁的时候触发的： </p>
                           <p>componentWillUnmount</p>
                       </div>

                       <div className='item mt20 pb10'>
                           <p>比较重要的（经常使用的）几个生命周期函数罗列如下： </p>

                           <div className='sub-item mt10'>
                               <p>加载的时候：</p>
                               <p>componentWillMount、 render 、componentDidMount（dom操作）</p>
                           </div>

                           <div className='sub-item mt10'>
                               <p>更新的时候：</p>
                               <p>componentWillUpdate、render、componentDidUpdate</p>
                           </div>

                           <div className='sub-item mt10 mb10'>
                               <p>销毁的时候：</p>
                               <p>componentWillUnmount</p>
                           </div>

                       </div>


                       <p className='mt10'>补充说明下：构造函数和render函数
                           <span className='red'>不属于</span>
                           生命周期函数</p>
                   </div>

                   <div className='demo-show mt20'>
                       <div className='sub-demo'>
                           设计的第一个demo可以打开控制台，查看页面载入到渲染完成会依次触发的四个生命周期：
                           <p className='val-show pl15 ml10'>
                               constructor -> componentWillMount -> render -> componentDidMount
                           </p>
                       </div>

                       <div className="mt20 sub-demo">
                          <p>
                              设计的第二个demo是先显示初始化 this.state.msg 的值，然后通过点击按钮来改变 this.state.msg 的值,
                              以此来观察会触发的四个生命周期(可以通过打开浏览器控制台来查看)
                          </p>
                           <p className='val-show pl15 ml10'>
                               shouldComponentUpdate -> componentWillUpdate -> render -> componentDidUpdate
                           </p>
                           <p className='val-show pl15 ml10 mb10 mt10'>
                               <span>{this.state.msg}</span>
                           </p>
                           <button className='pl20' onClick={this.changeMsg}>点击我改变上面的值</button>
                       </div>

                       <div className="mt20 sub-demo">
                           <p>设计的第三个demo 展示组件销毁的时候会触发 componentWillUnmount 生命周期</p>
                           <p>
                               下面有一个按钮，点击会切换是否展示一个子组件，如果子组件原本有，那就会触发销毁的生命周期钩子
                               如果原本子组件没有就会 走一遍 组件重新渲染的几个生命周期函数(可打开控制台查看变化)
                           </p>

                           <div>
                                <button onClick={this.toggleFlag}>点击我来切换是否 挂载/销毁 下面的子组件</button>
                               {
                                   this.state.flag ? <SonComponent /> : null
                               }
                           </div>
                       </div>


                       <div className="mt20 sub-demo">

                           <p>
                               设计的最后一个demo 父组件传值给子组件，并在父组件中改变所传的值会触发 componentWillReceiveProps 生命周期(可打开控制台查看变化)
                           </p>
                           <div>
                               <p>下面有一个按钮，点击会改变传递给子组件的title这个值</p>
                               <SonComponent title={this.state.title} />

                               <p className='mt10'>
                                   <button onClick={this.changeTitle}>点击我修改传递给子组件中的title值</button>
                               </p>
                           </div>

                       </div>

                   </div>

               </div>
           </div>
        );
    }
}

export default ReactLifeCycle;

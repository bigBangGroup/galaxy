import React, { Component } from 'react';

class SonComponent2 extends Component<any, any> {
    constructor(props:any){
        super(props);
        this.state = {
            sVal:'我是子组件中的值sVal'
        }
    }

    sonFoo = (txt:string) =>{
        alert('我是子组件中的方法sonFoo,显示父组件传递过来的参数: ' + txt);
    }

    render() {
        return (
           <div className='son-component2-container'>

           </div>
        );
    }
}

export default SonComponent2;

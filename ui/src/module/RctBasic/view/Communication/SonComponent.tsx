import React, { Component } from 'react';

class SonComponent extends Component<any, any> {
    constructor(props:any){
        super(props);
    }

    showParentComponent = () =>{
        console.log({
            pConmonent:this.props.pComponent,
            pVal:this.props.pComponent.state.pVal,
            pMethod:this.props.pComponent.pFoo
        })
    }
    render() {
        return (
           <div className='son-component-container'>
                <div>
                    <p>我是子组件,在这里我要展示从父组件传递过来的一个属性值和方法：</p>
                    <div className='mg10'>
                        父组件的pVal的值：<span className='val-show'>{this.props.pVal}</span>
                        <p>
                            <button onClick={this.props.pMethod.bind(this, '我是子组件调用父组件的方法传递的参数')} className='mg10'>点击我调用父组件传递过来的pFoo方法</button>
                        </p>
                        <p>
                            <button onClick={this.showParentComponent} className='mg10'>点击我在控制台查看从父组件传递过来的整个父组件的实例</button>
                        </p>
                    </div>
                </div>
           </div>
        );
    }
}

export default SonComponent;

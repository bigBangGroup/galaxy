import React, {Component} from 'react';
import './../scss/Constraint.scss';

interface ConstraintProps {

}
interface ConstraintStatus {
    defVal:string
    defVal2:string
    sex:string
    city:string
    citys:string[]
    hobby:any[]
    info:string
}

class Constraint extends Component<ConstraintProps, ConstraintStatus> {
    constructor(props:any) {
        super(props);

        this.state = {
            defVal:'defaultValue',
            defVal2:'defaultValue2',
            sex: "1",
            city:'北京',
            citys:[
                '北京','上海','深圳'
            ],
            hobby:[
                {
                    'title':"睡觉",
                    'checked':true
                },
                {
                    'title':"吃饭",
                    'checked':false
                },
                {
                    'title':"敲代码",
                    'checked':true
                }
            ],
            info:''
        };

        this.handleInfo=this.handleInfo.bind(this);
    }

    changeDefVal2 = (e:any) =>{
        let val = e.target.value;
        this.setState({
            defVal2:val
        })
    }

    handelSubmit=(e:any)=>{
        //阻止submit的提交事件
        e.preventDefault();

        console.log([
            {'sex': this.state.sex},
            {'city': this.state.city},
            {'hobby': this.state.hobby},
            {'info': this.state.info}
        ]);
    }

    handelSex=(e:any)=>{
        this.setState({
            sex:e.target.value
        })
    }

    handelCity=(e:any)=>{
        this.setState({
            city:e.target.value
        })

    }

    handelHobby = (key:number) =>{
        let hobby=this.state.hobby;

        hobby[key].checked=!hobby[key].checked;

        this.setState({
            hobby:hobby
        })
    }

    handleInfo(e:any){

        this.setState({

            info:e.target.value
        })
    }

    render() {
        return (
            <div className='constraint-container container'>
                <div className='demo-wrap'>
                   <div className="title">1：react中的约束性组件和非约束性组件</div>
                   <div className="demo-content">
                       <div className='item'>
                          <p>
                              所谓的非约束性组：以input组件为例，input组件中的 defaultValue 属性，其实就是原生DOM中的 value 属性。
                              这样写出的来的组件，其value值就是用户输入的内容，React完全不管理输入的过程。
                          </p>
                           <p>
                               可以看下面的input输入框，绑定的属性值是 用defaultValue 来绑定this.state.defVal。
                               那就不会出现报错提示你没绑定onChange事件，这一步只实现了MV.(即你可以输入值，但是实际上this.state.defVal这个变量的值还是没有被改变)
                           </p>
                           <div className="item-demo">
                               <span>this.state.defVal的值: <span className='val-show'>{this.state.defVal}</span></span>
                               <input type="text" defaultValue={this.state.defVal}/>
                           </div>
                       </div>
                       <div className='item'>
                           <div>约束性组件：input组件中的 value 属性不再是一个写死的值，他是由react中在组件初始化this.state.xxx和对应的绑定事件onChange来管理的</div>
                           <div>这个时候实际上 input 的 value 根本不是用户输入的内容。而是onChange 事件触发之后，由于 this.setState 导致了一次重新渲染。
                               不过React会优化这个渲染过程。看上去有点类似双休数据绑定
                           </div>
                           <div className='item-demo'>
                               <p className='mb10'>下面这个input标签是绑定了value属性，并同时监听了onChange事件。来实现 MVVM的功能</p>
                               <span>this.state.defVal2 的值: <span className='val-show'>{this.state.defVal2}</span></span>
                               <input type="text" value={this.state.defVal2} onChange={this.changeDefVal2}/>
                           </div>
                       </div>
                   </div>
                </div>

                <div className="demo-wrap">
                    <div className="title">2：其他form表单补充</div>
                    <div className="demo-content">

                        <div className='item'>
                            <form onSubmit={this.handelSubmit}>
                                <div className='item-demo'>
                                    <span> radioBox - 性别:    </span>
                                    <input className='mg5' type="radio" value="1" checked={this.state.sex === "1"}  onChange={this.handelSex}/>男
                                    <input className='mg5' type="radio"  value="2" checked={this.state.sex === "2"}  onChange={this.handelSex}/>女
                                </div>

                                <div className="item-demo">
                                    <span>select标签 - 居住城市:</span>
                                    <select value={this.state.city} onChange={this.handelCity} className='mg5'>
                                        {

                                            this.state.citys.map(function(value,key){

                                                return <option key={key}>{value}</option>
                                            })
                                        }
                                    </select>
                                </div>

                                <div className="item-demo">
                                    <span>checkbox标签 - 爱好:</span>

                                    {
                                        // 注意this指向
                                        this.state.hobby.map((value,key)=>{
                                            return (
                                                <span key={key}>
                                                    <input className='mg5' type="checkbox"  checked={value.checked}  onChange={this.handelHobby.bind(this,key)}/>
                                                    {value.title}
                                                </span>
                                            )
                                        })
                                    }

                                </div>

                                {/*<div className="item-demo">*/}
                                {/*    <span>textarea标签 - 备注:</span>*/}
                                {/*    <textarea className='text-area' vlaue={this.state.info}  onChange={this.handleInfo} />*/}
                                {/*</div>*/}

                                <div>
                                    <p className='mg5'>点击下面按钮，打开控制台查看提交的表单内容: </p>
                                    <input type="submit"  defaultValue="提交" className='mt10'/>
                                </div>

                            </form>
                        </div>


                    </div>
                </div>
            </div>
        );
    }
}

export default Constraint;

import React, { Component } from 'react';
import Img from './../../../images/img1.jpg';
import './../scss/Basic.scss';

interface BasicProps {

}
interface BasicStatus {
  count:number
  cls:string
  age:number
  arr:number[]
  imgUrl:string
}
class Basic extends Component<BasicProps, BasicStatus> {
  constructor(props:any){
    super(props);

    let imgUrl="http://pic37.nipic.com/20140113/8800276_184927469000_2.png";
    this.state = {
      count:1,
      cls:'red',
      age:20,
      arr:[111,222,333],
      imgUrl:imgUrl
    }

    this.addAge = this.addAge.bind(this);
    // this.incAge = this.incAge.bind(this);
  }


  addAge(){
    this.setState({
      age: this.state.age + 1
    })
  }
  incAge(nAge:number){
    this.setState({
      age: nAge
    })
  }
  initAge = () => {
    this.setState({
      age: 20
    })
  }
  render() {

    let liUI =  this.state.arr.map(function (item,index) {
      return (
          <li key={index}>{item}</li>
      )
    });


    return (
        <div className='basic-learn-container container'>
          {/*demo1*/}
          <div className='demo demo1' id='demo1'>
            <p className='title'>1：数据绑定/属性绑定</p>
            <div className='content'>
              用法是在constructor的时候初始化state对象的时候，赋值count初始值为1，并在jsx语法中使用花括号包裹变量的的方式，读取到初始化的变量,设计的demo如下:
              <div className='demo-wrap'>
                将在组件初始化的时候定义一个变量msg,并将绑定的msg的值展现在这里(鼠标hover上可查看该被绑定的元素)：
                <span className='text' title={`this.state.count`}>{this.state.count}</span>
              </div>
            </div>
          </div>

          {/*demo2*/}
          <div className='demo demo2' id='demo2'>
            <p className='title'>2：class属性绑定</p>
            <div className="content">
              class/style两个属性是在实际应用开发过程中用到的最多的两个属性。所以在这里拿出来单独说明一下。
              其实用起来就和普通的属性绑定一样,和上面的title属性绑定没区别，常用的方式 是定义一个变量，这个变量的值可以在逻辑层做一些业务的判断。
              以此来增加或改变class的值，然后在render函数中的view层通过花括号获取变量的方式获取class的值即可。
              这里需要注意的一点是在jsx中需要使用className来代替class。
              <div className='demo-wrap'>
                <p>设计的demo如下，在初始化的时候定义一个变量名称cls,这个值返回的就是class的名称，
                  然后将其绑定到需要的元素上（当然在这里可以加上一些业务判断来决定是否增加或减少一些class的名称）：</p>
                <p className={this.state.cls}>我的class样式是通过this.data.cls来获取的</p>
              </div>
            </div>
          </div>

          {/*demo3*/}
          <div className='demo demo3' id='demo3'>
            <p className='title'>3：图片的引入</p>
            <div className="content">
              jsx中图片的引入也是常用的场景之一，img是一个替换标签，在jsx中不能像写html一样，直接把url(不管是相对的还是绝对的)写在img中的src属性中，
              这样做是无效的。
              <p>正确的做法如下（本地图片）</p>
              <p>1：使用import 方式引入本地图片</p>
              <p>2：将引入的图片对应的值绑定到img的src上即可</p>
              <div className='demo-wrap'>
                <img src={Img} alt="" className='img'/>
              </div>
              <p>或者使用如下的方式，直接在img标签上的src属性使用es5的require来根据本地相对路径来引入图片，不常用，推荐上述方式来实现。</p>
              <div className='demo-wrap'>
                <img className='img-box' src={require('./../../../images/img1.jpg')} alt=""/>
              </div>

              <div>远程图片的引入：</div>
              <p>对于远程图片的引入直接将远程绝对地址的url赋值给img的src属性即可，实例如下:</p>
              <img className='img-box' src="http://pic37.nipic.com/20140113/8800276_184927469000_2.png" alt=""/>
              <p>或者也可以将地址放在state对象的某个变量（这里是 imgUrl ）然后将这个变量和img的src属性绑定即可。示例如下：</p>
              <img className='img-box' src={this.state.imgUrl} alt=""/>
            </div>
          </div>

          {/*demo4*/}
          <div className='demo demo4' id='demo4'>
            <p className='title'>4：循环数组来渲染数据</p>
            <div className="content">
              应用开发中，经常会根据一个服务端返回的数组，来根据这个数组渲染页面。下面说明下
              <p>1：在逻辑层处理好数据用一个变量承接经过业务处理好的数据</p>
              <p>2：在render方法返回的view层中将上述的变量引入进来，即可</p>
              <p>3：需要注意的是，每一次的循环给子组件都要设置一个唯一的一个key</p>
              <p>说明一下，当然在jsx中可以使用'{}'的形式，在里面封装逻辑，但是这样会显得比较冗余，
                不清晰，建议业务逻辑部分都放在逻辑层处理好之后，直接在view层直接渲染</p>
              <p>设计的demo如下：在js逻辑层已近处理好一个单纯的数组[111,222,333]，然后在view层使用li标签包裹数组内荣。</p>

              <div className="demo-wrap">
                <ul>
                  {liUI}
                </ul>
              </div>
            </div>
          </div>

          {/*demo5*/}
          <div className='demo demo5' id='demo5'>
            <p className='title'>5：react定义方法的几种方式</p>
            <div className='content'>
              <div>
                <p>方法的定义一般常用的有如下三种方式，下面一个个来说明下：</p>
                <p>1：给指定的dom元素添加事件一般是 onClick onChange，注意绑定事件的时候不可在方法后面追加()，否则就是方法的运行了。</p>
                <p>2：定义函数，并绑定this的指向，关于this的指向有三种方式。所有才说方法的定义有三种。</p>
                <p> 2.1：在constructor中去用bind给函数绑定this,下面这个demo中的addAge就是采用这个方式</p>
                <p> 2.2：在class中定义函数的地方直接使用es6的箭头函数，因为箭头函数没有自己的this,在其中的this就是上下文定义的词法定义时候的this。见下initAge方法</p>
                <p> 2.3：在render的view中直接绑定this在下面的demo中 incAge 就是这种方式</p>
                <p>设计的demo是先展示绑定的属性age，然后分别有两个按钮会增加年龄和减少年龄。示例如下:</p>
                <div>
                  这里展示绑定的数据age:
                  <div className='demo-wrap'>
                    <span className='btn' onClick={this.addAge}>add age</span>
                    <span className='btn' onClick={this.incAge.bind(this, this.state.age - 1)}>inc age</span>
                    <span className='btn' onClick={this.initAge}>init age 20</span>
                    <span className='text'>{this.state.age}</span>
                  </div>
                </div>

              </div>
              <div>
              </div>
            </div>
          </div>

        </div>
    );
  }
}

export default Basic;

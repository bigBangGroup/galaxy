import React, { Component } from 'react';
import PropTypes from 'prop-types';

interface SonComponent2Props {
    pVal2?:string
}

class SonComponent2 extends Component<SonComponent2Props, any> {
    constructor(props:any){
        super(props);

    }
    render() {
        return (
           <div className='son-component-container2'>
               <p>我是子组件，我接受了来自父组件的传值，但是我会验证传递过来的时候是数值类型。</p>
               <p className='mt10'>
                   <span className='val-show'>{this.props.pVal2}</span>
               </p>
           </div>
        );
    }
}

//defaultProps   如果父组件调用子组件的时候不给子组件传值，可以在子组件中使用defaultProps定义的默认值
(SonComponent2 as any).propTypes={
    pVal2:PropTypes.number
}

export default SonComponent2;

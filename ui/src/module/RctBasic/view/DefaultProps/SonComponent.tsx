import React, { Component } from 'react';


interface SonComponentProps {
    pVal?:string
}


class SonComponent extends Component<SonComponentProps, any> {
    constructor(props:any){
        super(props);
    }
    render() {
        return (
           <div className='son-component-container'>
               <p>我是子组件，我要在下面显示父组件传递过来的值，如果父组件没有给我值，我就会使用 defaultProps 来展示</p>
               <p className='mt10'>
                   <span className='val-show'>{this.props.pVal}</span>
               </p>
           </div>
        );
    }
}

//defaultProps   如果父组件调用子组件的时候不给子组件传值，可以在子组件中使用defaultProps定义的默认值
(SonComponent as any).defaultProps={
    pVal:'如果父组件没有传值，就会使用我.'
}

export default SonComponent;

import React, { Component } from 'react';
import SonComponent from './Communication/SonComponent';
import SonComponent2 from './Communication/SonComponent2';
import './../scss/Communication.scss';

interface CommunicationProps {

}

interface CommunicationStatus {
    pVal:string
}
class Communication extends Component<CommunicationProps, CommunicationStatus> {
    constructor(props:any){
        super(props);

        this.state = {
            pVal:'我是父组件中的值pVal'
        }
    }

    pFoo = (param:string) => {
        alert('我是父组件中的方法pFoo,被子组件调用并显示从子组件传递过来的参数: ' + param);
    }

    showSonFoo = () => {
        (this.refs.sonComponent2 as any).sonFoo('我是父组件传递给子组件的参数')
    }

    showSonVal = () => {
        alert((this.refs.sonComponent2 as any).state.sVal)
    }

    showSonComponent = () => {
        const com2:any = this.refs.sonComponent2;
        console.log(
            {
                "整个子组件实例: ": com2,
                "子组件的 sVal: ": com2.state.sVal,
                "子组件的 sonFoo方法: ":  com2.sonFoo
            }
        )
    }
    render() {
        return (
           <div className='communication-container container'>
                <div className='item'>
                    <p className='title'>1：react中父子组件的互相通信。</p>
                    <div className='demo-content'>
                        <p>在react中经常涉及到两个组件之间相互传值的使用场景，下面设计了几个demo演示一下在react中父子组件的通信</p>
                        <div className='demo1'>
                            <p>子组件调用父组件的值和方法</p>
                            <p>这里设计一个父组件，父组件传递给子组件的属性值或者方法供子组件调用</p>
                            <div>
                                <SonComponent pVal={this.state.pVal} pMethod={this.pFoo} pComponent={this}></SonComponent>
                                <div className='ml10'>
                                    <p>上面的demo显示了在子组件中显示了父组件传递过去的值，和在子组件中传递过去的方法</p>
                                    <p>使用很简单，两步即可</p>
                                    <p>1: 在父组件调用子组件的时候，将需要传递的值或者方法（这里是pVal && pMethod）放在特定的属性中。</p>
                                    <p>2: 子组件（类）中使用this.props.xx (这里是 this.props.pVal && this.props.pMethod)的方式来调用即可</p>
                                    <p>还有一种简单粗暴的方式，可以在父组件调用子组件的时候，将整个父组件的实例传递给子组件，这样子组件就可以获取
                                        父组件上的所有属性和方法了。
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

               <div className="item mt20">
                   <p className='title'>2：父组件获取子组件的方法和值</p>
                   <div className='demo-content'>
                       <p>父组件想主动调去子组件上的属性也很方便，只需两步即可</p>
                       <p>1：父组件实例化子组件的时候，给子组件加一个ref钩子(这里加的钩子是sonComponent2)，方便父组件获取子组件</p>
                       <p>2：使用this.refs.xxx（这里是this.refs.sonComponent2）来从子组件中获取对应的属性即可（包括一般属性值和方法都可）</p>
                       <p>设计的demo是在本组件（父组件）中会实例化一个子组件，然后在子组件中定义sVal的值和SonFoo方法，然后在父组件中去获取子组件的属性，
                       调用子组件的时候并传对应的参数给子组件</p>
                       <p>最后一个按钮也展示了从父组件中如何获取整个子组件的实例。</p>
                       <SonComponent2 ref='sonComponent2'></SonComponent2>
                       <p>
                           <button onClick={this.showSonVal} className='mg10'>点击查看子组件中的属性值sVal</button>
                       </p>
                       <p>
                           <button onClick={this.showSonFoo} className='mg10'>点击调用子组件中的方法</button>
                       </p>

                       <p>
                           <button onClick={this.showSonComponent} className='mg10'>点击我在控制台查看从子组件获取的的整个子组件的实例</button>
                       </p>

                   </div>
               </div>
           </div>
        );
    }
}

export default Communication;

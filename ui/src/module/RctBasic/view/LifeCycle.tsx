import React, { Component } from 'react';
import ReactLifeCycle from './LifeCycle/ReactLifeCycle'
import './../scss/LifeCycle.scss';

class LifeCycle extends Component<any, any> {
    constructor(props:any){
        super(props);
    }

    render() {
        return (
           <div className='react-life-cycle-container container'>
               <ReactLifeCycle />
           </div>
        );
    }
}

export default LifeCycle;

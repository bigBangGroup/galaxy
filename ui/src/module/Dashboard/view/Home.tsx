import React from "react";
import "../scss/Home.scss";
import IncomeBar from './component/IncomeBar'
import VisitorViews from './component/VisitorViews';
import watermark from 'watermark-webpage';
import cookie from 'js-cookie';
import { Link } from "react-router-dom";

interface IDash {
  name:string
  hash:string
  bgColor:string
}
const dashList:IDash[] = [
  {name:'水印', hash:'/admin/extension/watermark', bgColor:'#409EFF'},
  {name:'动画', hash:'/admin/Animation/animation-table', bgColor:'#67C23A'},
  {name:'富文本', hash:'/admin/extension/richtxt', bgColor:'#E6A23C'},
  {name:'地图', hash:'/admin/chart/map', bgColor:'#F56C6C'},
];

const DashBoard = () =>{
  return(
      <div className='dash-board-container'>
        {
          dashList.map((item:IDash, index:number) => {
            return (
                <Link to={item.hash} className='dash-item' key={index} style={{backgroundColor:item.bgColor}}>
                  <div >
                      <span className='txt'>{item.name}</span>
                  </div>
                </Link>
            )
          })
        }
      </div>
  )
};

class Dashboard extends React.Component<any, any> {
  state = {
    collapsed: false
  };

  addWatermark = (wmContent:string) =>{
    let _waterMark = (window as any)._waterMark;
    _waterMark && (_waterMark.destroy());
    const waterContainer:HTMLElement = (document.getElementsByClassName('router-wrap')[0] as HTMLElement);
    (window as any)._waterMark = new watermark({
      container: waterContainer ||document.body,
      content: wmContent,
      fillStyle: "rgba(184, 184, 184, 0.4)",
      width: 250,
      height: 200
    });
  };


  componentDidMount() {
    const userName:string = localStorage.getItem("userName")!;
    this.addWatermark(userName);

    // axios.get("api/getlist", { params: { id: 123 } }).then((res: any) => {
    //   console.log(res);
    // });
  }

  render() {
    return (
      <React.Fragment>
        <div className="dashboard-container">
          <DashBoard />
          <IncomeBar />
          <VisitorViews />
        </div>
      </React.Fragment>
    );
  }
}

export default Dashboard;

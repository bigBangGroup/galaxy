import React ,{useContext} from 'react';
import { LangContext, ZH_LANG, EN_LANG } from './../ReducerHooks'
import { Button } from "antd";

function ChangeLantBtns(){
    const { lang, dispatch } = (useContext(LangContext) as any)
    const changeZh = ()=>{
        dispatch({type:ZH_LANG,payload:"other Params"})
    }
    return (
        <div className="mt10" style={{backgroundColor:"aqua", padding:"5px"}}>
            <div className="des mb10">{lang.txt}</div>
            <Button type="primary" onClick={changeZh} className="mr10">{lang.zh}</Button>
            <Button type="primary" onClick={()=>{dispatch({type:EN_LANG,payload:"other Params"})}}>{lang.en}</Button>
        </div>
    )
}

export default ChangeLantBtns
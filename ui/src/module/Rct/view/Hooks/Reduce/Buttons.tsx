import React ,{useContext} from 'react';
import {ColorContext,UPDATE_COLOR} from './Color'
import { Button } from "antd";

function Buttons(){
    // 结构赋值根组件共享出来的 dispatch 来改变状态
    const { dispatch } = (useContext(ColorContext) as any)
    return (
        <div className="mt10" style={{backgroundColor:"aqua", padding:"5px"}}>
            <div className="des">控制按钮的子组件</div>
            <Button type="primary" onClick={()=>{dispatch({type:UPDATE_COLOR,color:"red"})}} className="mr10">红色</Button>
            <Button type="primary" onClick={()=>{dispatch({type:UPDATE_COLOR,color:"yellow"})}}>黄色</Button>
        </div>
    )
}

export default Buttons
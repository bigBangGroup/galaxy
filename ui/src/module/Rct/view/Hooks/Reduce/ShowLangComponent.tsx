import React ,{useContext} from 'react';
import { LangContext, ZH_LANG, EN_LANG } from './../ReducerHooks'
import { Button } from "antd";

function ShowLangComponent(){
    // 结构赋值根组件共享出来的 dispatch 来改变状态
    const { lang } = (useContext(LangContext) as any)
    return (
        <div className="mt10" style={{backgroundColor:"aqua", padding:"5px"}}>
             <div className="des">{lang.txt}</div>
            <div>{lang.txt1}</div>
            <div>{lang.txt2}</div>
        </div>
    )
}

export default ShowLangComponent
import React , { useContext } from 'react';
import { ColorContext } from './Color';

function ShowArea(){
    // 结构赋值根组件共享出来的 color 值
    const {color} = useContext(ColorContext) as any
    return (
        <div style={{color:color, padding:"5px", backgroundColor:"pink"}}>
            <div className="des">显示字体颜色的子组件</div>
            字体颜色为{color}
        </div>
    )
}

export default ShowArea
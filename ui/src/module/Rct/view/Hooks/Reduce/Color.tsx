import React, { createContext, useReducer } from 'react';

export const ColorContext = createContext({})

export const UPDATE_COLOR = "UPDATE_COLOR"

const reducer= (state:any,action:any)=>{
    switch(action.type){
        case UPDATE_COLOR:
            return action.color
        default:
            return state
    }
}


export const Color = (props:any)=>{
    const [color,dispatch]=useReducer(reducer,'blue')
    return (
        //  这个是根组件,需要把共享出去的值(这里是color 变量)在这里登.
        //  dispatch 也需要在这里共享出去，由需要的子组件 import 进来，然后子组件可以分发出去供其他子组件接受.
        <ColorContext.Provider value={{color,dispatch}}>
            <div className="des mb10">下面是两个子组件，button子组件通过 useReducer 来控制颜色的切换，showArea子组件显示当前所切换的颜色</div>
            {props.children}
        </ColorContext.Provider>
    )
}
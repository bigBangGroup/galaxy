import React, { createContext } from 'react';

import { Color } from './Reduce/Color';
import ShowArea from './Reduce/ShowArea';
import Buttons from './Reduce/Buttons';

export const ColorContext = createContext({})

const ReducerHook = (props:any)=>{
    return (
        <div>
            <Color>
                <ShowArea />
                <Buttons />
            </Color>
        </div>
    )
}

export default ReducerHook
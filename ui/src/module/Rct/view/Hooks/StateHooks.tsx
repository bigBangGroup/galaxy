import React, { useState } from 'react';
import { Button } from "antd";

// ?react组件的首单词大写,小写则无效
function StateHooks(){
    // 第一个是函数组件状态， 第二个是修改第一个参数的方法， useState 内可传入初始值
    const initCount = 0;
    const initAge = 20;
    const [count, setCount] = useState(initCount);
    const [age, setAge] = useState(initAge);

    const addCount = () =>{
       setCount(count+1);
    }

    return (
        <div className="state-fn-container">
            <div className="des">
                这个实例展示了使用 useState 的hook可以让函数组件具有自己的状态。并可以设置/修改自己的状态.
            </div>
            <div className="showVal">show count: {count}</div>
            <div className="showVal mb10">show age :{age}</div>
            <Button type="primary" onClick={addCount}>add account</Button>
            <Button type="primary" onClick={()=>setAge(age+1)} className='ml10'>add age</Button>
        </div>
    )
}

export default StateHooks

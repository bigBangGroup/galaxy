import React, { useState } from 'react';
import ChildrenCmponent from './Memo/ChildrenComponent';
import { Button } from "antd";

const MemoHook = ()=>{

    const initCount = 0;
    const initAge = 20;
    const [count, setCount] = useState(initCount);
    const [age, setAge] = useState(initAge);

    const addCount = () =>{
        setCount(count+1);
    }

    return (
        <div className='memo-hooks-container'>
            <div className="des">使用 useMome解决的问题是父组件更新的时候子组件也会跟着频繁更新，所以父组件更新的时候需要控制子组件有些方法是不运行的以此节约性能</div>
            <div className="des mb10">下面这个例子就是父组件两个按钮可更新父组件中的 count 和 age,但是希望父组件更新age的时候子组件 ageChange 方法才会执行</div>
            <Button type="primary" onClick={()=>setAge(age+1)} className='mr10 mb10'>add age</Button>
            <Button type="primary" onClick={addCount}>add count</Button>
            <ChildrenCmponent age={age} count={count}/>
        </div>
    )
}

export default MemoHook
import React, { createContext, useReducer } from 'react';
import ChangeLantBtns from './Reduce/ChangeLang';
import ShowLangComponent from './Reduce/ShowLangComponent';

const zhLang:any = {
    zh:"中文",
    en:"英文",
    rootTxt:'这是父组件，下面是两个子组件，一个子组件是按钮组可控制语言的显示，另一个子组件是文案显示，使用Reduce Hook来实现状态管理',
    txt:'下面显示的语言由上面按钮组件通过 reducerHooks 控制',
    txt1: "今天真是一个好天气呀~",
    txt2: "检查一下我的邮件。"
};

const enLang:any = {
    zh:"Chiness",
    en:"English",
    rootTxt:'This is the parent component, below are two child components, a child component is the button group can control the language display, the other is the copy display, using Reduce Hook to achieve state management',
    txt:'The language shown below is controlled by the above button component via reducerHooks',
    txt1: "Today is a good day~",
    txt2: "Please check my email."
}

export const LangContext = createContext(zhLang);
export const ZH_LANG = "ZH_LANG";
export const EN_LANG = "EN_LANG";

/*
    param1: state 上一个状态的值。
    param2: action 是子组件dispatch 方法中的参数，一般是一个对象，里面 action表行为，可加上payload表其他参数
*/
const reducer = (state: any, action: any) => {
    switch (action.type) {
        case ZH_LANG:
            return zhLang;
        case EN_LANG:
            return enLang
        default:
            return zhLang
    }
}

const Reducer2Hooks = () => {
    const [lang, dispatch] = useReducer(reducer, zhLang);

    return (
        <LangContext.Provider value={{lang, dispatch}}>
            <div className="reducer-container">
                <div className="des">{lang.rootTxt}</div>
                <ChangeLantBtns />
                <ShowLangComponent />
            </div>
        </LangContext.Provider>
    )
}

export default Reducer2Hooks
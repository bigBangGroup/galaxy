import React from "react";
import Demo1 from './CallBackHook/Demo1';
import Demo2 from './CallBackHook/Demo2';

export default function Demo() {
  return (
    <div className="sub-container">
      <div className='des'>
        callBack Hook是为了解决缓存函数的问题。比如这样一个场景
        <div>
        父组件props传递给了子组件一个函数A->子组件会业务触发调用这个函数A->调用运行父组件函数组件中的A->
        父组件可能有值更新->父组件更新(父组件更新自然导致这个函数A定义会重新运行,会得到一个新的函数A)->父组件传递给子组件的props变了->子组件更新
        </div>
        那其实这里由于父组件的更新会使得函数体A重新运行一遍导致传递给子组件的props变化，导致子组件又更新
      </div>
      <Demo1 />
      <Demo2 />
    </div>
  );
}

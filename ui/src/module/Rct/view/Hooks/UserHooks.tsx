import React from 'react';
import useWinSize from './UserWinSize';

// 自定义hook函数需要 use 开头，不然分不清 react组件还是自定义 hooks 函数
const UserHooks = ()=>{
    const size = useWinSize()
    return (
        <div className="user-hooks-container">
            <div className="des">用户的自定义 hooks, 下面这个例子中就使用用户自定义 hooks 来实现获取浏览器页面宽高的hooks函数</div>
            <div className="des">获取浏览器匡高的hooks 函数功能是和业务无关的，哪里需要的话，哪里直接引入就可以直接使用了.</div>
            <div className="des">需要注意的是自定义hooks是一个函数功能而非函数组件.</div>
            <div>页面Size:{size.width}x{size.height}</div>
        </div>
    )
};

export default UserHooks;
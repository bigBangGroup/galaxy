import React, { useState, useEffect } from 'react';
import { Button } from "antd";


const EffectHooks = ()=>{
    const initAge = 26;
    const initCount = 0;
    const [count, setCount] = useState(initCount);
    const [age, setAge] = useState(initAge);
    
    // 函数组件初始执行的方法, 这里自我约定为 init
    // return 是该函数组件销毁时执行的方法, 这里自我约定为 destory
    const init = ()=>{
        console.log('EffectHooks init');
        return destory
    }

    const destory = ()=>{
        console.log('EffectHooks destory')
    }

    /* 
        1: 第一个参数是该函数组件渲染时候执行的方法，类似 componentDidMount，在函数组件刚渲染的时候一定会执行一次
        2: 第二个参数是标识哪些参数状态变化第一个函数会执行，第一个函数在最初刚渲染组件时候已近执行过一次，所以是先执行 destory(如果有的话) 再重新init。
        3: 当第二个参数为空的时候组件将被销毁时才运行 destory
    */
    // 该函数组件有两个状态的参数 age、count，将age 这个状态传入第二个数组中，表只有age变化的时候该函数组件才会重新渲染，而count变化该组件则不会重新渲染
    useEffect(init, [age])

    const addCount = () =>{
        setCount(count+1);
    }

    return (
        <div className="effect-hooks-container">
            <div className="des">
                使用 Effect Hooks 来代替 class component 组件中的各种生命周期
                (componentDidMount/shouldComponentUpdate/componentWillUnmount)
            </div>
            <div>打开控制台可查看该函数组件初始化的时候打印的信息</div>
            <div className="showVal">count: {count}</div>
            <div className="showVal mb10">age: {age}</div>
            <Button type="primary" onClick={addCount}>add account</Button>
            <Button type="primary" onClick={()=>setAge(age+1)} className='ml10'>add age</Button>
        </div>
    )
}

export default EffectHooks;

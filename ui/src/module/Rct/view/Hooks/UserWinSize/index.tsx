import React, { useState ,useEffect ,useCallback } from 'react';

// 自定义hook函数需要 use 开头，不然分不清 react组件还是自定义 hooks 函数
const useWinSize = ()=>{
    const [ size , setSize] = useState({
        width:document.documentElement.clientWidth,
        height:document.documentElement.clientHeight
    })

    const onResize = useCallback(()=>{
        setSize({
            width: document.documentElement.clientWidth,
            height: document.documentElement.clientHeight
        })
    },[]) 

    const init = ()=>{
        window.addEventListener('resize',onResize);
        return destory
    }

    const destory = ()=>{
        window.removeEventListener('resize',onResize)
    }
    useEffect(init,[])

    return size;
};

export default useWinSize;
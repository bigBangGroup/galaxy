import React, { useContext } from 'react';
import { StoreContext } from './ContextHooks';

const ChildContext = ()=>{
    const shareObj:any = useContext(StoreContext);  //一句话就可以得到count
    console.log(shareObj);
    return (
        <div className="child-container demo">
            <div className="des">子组件，用于接收上层组件传递过来的参数</div>
            <div className="val-container">上层组件通过 context 传递过来的值：
                <div>age: <span className="showV">{shareObj.age}</span></div>
                <div>count: <span className="showV">{shareObj.count}</span></div>
             </div>
        </div>
    )
}
export default ChildContext
import React, { useState, memo } from "react";
import { Button } from "antd";

const Parent = () => {
  const initCount = 0;
  const initAge = 20;
  const [count, setCount] = useState(initCount);
  const [age, setAge] = useState(initAge);

  const addCount = () => {
    setCount(count + 1);
  };

  return (
    <div className="container react-memo-demo">
      <div className="title">React.memo Demo2</div>
      <div>
        这是父组件,父组件更新的时候通过使用memo缓存了子组件，子组件没有变化的时候，
        就不会跟着父组件一起更新，从而减少了性能的开销。打开控制台会发现子组件函数不会执行.
      </div>
      <div className="showVal">
        show count: <span className="val">{count}</span>
      </div>
      <div className="showVal mb10">
        show age :<span className="val">{age}</span>
      </div>
      <Button type="primary" onClick={addCount}>
        add account
      </Button>
      <Button type="primary" onClick={() => setAge(age + 1)} className="ml10">
        add age
      </Button>
      <MemoChild />
    </div>
  );
};

const MemoChild = memo(() => {
  console.log("子组件函数MemoChild...");
  return (
    <div className="child-container mt10">
      这是子组件，内容固定，父组件更新的时候，我可以不用更新
    </div>
  );
});

export default Parent;

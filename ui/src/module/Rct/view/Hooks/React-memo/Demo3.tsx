import React, { useState, memo } from "react";
import { Button } from "antd";

const Parent = () => {
  const initCount = 0;
  const initAge = 20;
  const [count, setCount] = useState(initCount);
  const [age, setAge] = useState(initAge);

  const addCount = () => {
    setCount(count + 1);
  };

  return (
    <div className="container react-memo-demo">
      <div className='title'>React.memo Demo3</div>
      <div>
        这是父组件,父组件更新的时候通过使用memo缓存了子组件，父组件变化的时候，
        可以根据子组件第二个函数参数的返回值来控制子组件是否更新，
        有点类似 shouldComponentUpdate 的作用一样
      </div>
      <div className="showVal">
        show count: <span className="val">{count}</span>
      </div>
      <div className="showVal mb10">
        show age :<span className="val">{age}</span>
      </div>
      <Button type="primary" onClick={addCount}>
        add account
      </Button>
      <Button type="primary" onClick={() => setAge(age + 1)} className="ml10">
        add age
      </Button>
      <MemoChild age={age} />
    </div>
  );
};

const MemoChild:any = memo(
  () => {
    console.log("子组件函数MemoChild...");
    return (
      <div className="child-container mt10">
        这是子组件，内容固定，父组件更新的时候，我可以根据 React.memo 的第二个参数来选择是否更新
        返回的是false则表示前后的属性值不相等则会更新，返回的是true则表示相等则不更新
      </div>
    );
  },
  (pre:any, next:any) => {
    console.log(pre, next);
    console.log(pre.age === next.age);
    return pre.age === next.age;
  }
);

export default Parent;

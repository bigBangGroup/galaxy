import React, { useState } from "react";
import { Button } from "antd";

const Parent = () => {
  const initCount = 0;
  const initAge = 20;
  const [count, setCount] = useState(initCount);
  const [age, setAge] = useState(initAge);

  const addCount = () => {
    setCount(count + 1);
  };

  return (
    <div className="container react-memo-demo">
      <div className="title">React.memo Demo1</div>
      <div>
        这是父组件,父组件更新的时候子组件也会随着一起更新，打开控制台可以发现父组件
        更新的时候，子组件函数也会再次运行。
      </div>
      <div className="showVal">
        show count: <span className="val">{count}</span>
      </div>
      <div className="showVal mb10">
        show age :<span className="val">{age}</span>
      </div>
      <Button type="primary" onClick={addCount}>
        add account
      </Button>
      <Button type="primary" onClick={() => setAge(age + 1)} className="ml10">
        add age
      </Button>
      <Child />
    </div>
  );
};

const Child = () => {
  console.log("子组件函数Child...");
  return (
    <div className="child-container mt10">
      这是子组件，内容固定，父组件更新的时候，我可以不用更新,但默认情况下我也会更新
    </div>
  );
};

export default Parent;

import React, { useState } from "react";
import { Input } from 'antd';

export default function Parent() {

  const [inputTxt, setInputTxt] = useState('');  
  const handleChange = (e:any)=>{
    setInputTxt(e.target.value);
  }  
  return (
    <div className='parent-container'>
      <div className="des">
        这是父组件，父组件会传递给子组件一个方法，子组件在输入框输入内容的时候会调用这个方法。
        父组件同步显示在子组件输入框输入的内容，会导致父组件更新 进而使得handleChange 方法会随着输入内容的变化而不断运行，
        这个时候传递给子组件的 props.onChange其实就变化了，就会导致子组件也随着输入内容变化而更新。
      </div>
      <div className="show">子组件输入框输入的内容:{inputTxt}</div>
      <Child onChange={handleChange}/>
    </div>
  );
}

const Child:any = (props:any) => {
  console.log('子组件渲染...')
  return (
    <div className="child-container mt10">
        <div className="des mb10">这是子组件, 在子组件输入框中输入内容，父组件同步显示子组件输入的内容，会发现子组件会一直重复更新</div>
        <Input placeholder="input" onChange={props.onChange} />
    </div>
  );
};

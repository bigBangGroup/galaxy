import React, { useState, memo, useCallback } from "react";
import { Input } from 'antd';

export default function Parent() {

  const [inputTxt, setInputTxt] = useState('');  
  const handleChange = useCallback(
    (e:any)=>{
        setInputTxt(e.target.value);
    },[],
  )
  return (
    <div className='parent-container'>
      <div className="des">
        这是父组件，父组件会传递给子组件一个方法，子组件在输入框输入内容的时候会调用这个方法。
        父组件同步显示在子组件输入框输入的内容，但是因为使用了callBack这个Hook，所以子组件你不会重复渲染
      </div>
      <div className="show">子组件输入框输入的内容:{inputTxt}</div>
      <Child onChange={handleChange}/>
    </div>
  );
}

const Child:any = memo((props:any) => {
    console.log('子组件渲染Demo2...')
    return (
      <div className="child-container mt10">
          <div className="des mb10">这是子组件, 在子组件输入框中输入内容，父组件同步显示子组件输入的内容，但是子组件使用memo + 父组件使用useCallBack之后子组件不会一直重复渲染</div>
          <Input placeholder="input" onChange={props.onChange} />
      </div>
    );
  });

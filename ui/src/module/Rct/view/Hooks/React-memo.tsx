import React, { useState, memo } from "react";
import ReactMemo1 from './React-memo/Demo1';
import ReactMemo2 from './React-memo/Demo2';
import ReactMemo3 from './React-memo/Demo3';
const Parent = () => {
  const initCount = 0;
  const initAge = 20;
  const [count, setCount] = useState(initCount);
  const [age, setAge] = useState(initAge);

  const addCount = () => {
    setCount(count + 1);
  };

  return (
    <div className="container">
        <ReactMemo1 />
        <ReactMemo2 />
        <ReactMemo3 />
    </div>
  );
};

export default Parent;

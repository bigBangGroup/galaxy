import React, { useState, createContext } from 'react';
import { Button } from "antd";
import ChildContext from './ChildContext'

//关键代码, 创建一个共享的组件 StoreContext, 需要初始化一个值.
// 那个子组件需要这里面的值。直接 import 这个 StoreContext，然后 useContext(StoreContext) 就可得到这里共享出去的值
export const StoreContext = createContext({})

// context 可以跨越组件层级直接传递变量，实现共享
const ContextHooks = ()=>{
    // 初始化变量常量化
    const initAge = 26;
    const initCount = 0;
    
    // 相当于给函数组件声明 count、age 两个变量
    const [count, setCount] = useState(initCount);
    const [age, setAge] = useState(initAge);

    // 创建共享给子组件的对象
    const StoreObj = {
        age,count,
    }

    const addCount = () =>{
        setCount(count+1);
    }

    return (
        <div className="demo">
            {/* 该组件之下的所有子组件都可直接获取 StoreObj 中共享的值 */}
            <StoreContext.Provider value={StoreObj}>
                <div className="des">这是父组件，通过 context 传递给下面子组件一些参数</div>
                <div className="showVal">父组件中的count: {count}</div>
                <div className="showVal mb10">父组件中的age: {age}</div>
                <Button type="primary" onClick={addCount}>add account</Button>
                <Button type="primary" onClick={()=>setAge(age+1)} className='ml10'>add age</Button>
                {/* 在下面这个子组件中去获取该父组件共享出去的值 */}
                <ChildContext />
            </StoreContext.Provider>
        </div>
    )
}
export default ContextHooks
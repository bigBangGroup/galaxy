import React, { useMemo } from 'react';

const ChildrenComponent = ({age, count}:any)=>{

    const ageChange = (age:any)=>{
        console.log(`age的值发生改变了${age}`);
        return age;
    }

    // 只有 父组件传递过来的 age 值变化是，才会运行 ageChange 而 count 变化的时候 ageChange 是不会运行的
    const ageVal = useMemo(()=>ageChange(age),[age]) 

    return (
        <div className='memo-hooks-container child-container'>
            <div className="des">这个是子组件，展示父组件传递来的 age 和count的值，但是只有age变的时候子组件才会执行 ageChange 方法，count变的话子组件是不会运行 ageChange 方法的</div>
            <div className="des">可以打开控制台查看日志</div>
            <div className="showVal mb10">父组件传递过来的age:{ageVal}</div>
            <div className="showVal">父组件传递过来的count:{count}</div>
        </div>
    )
}

export default ChildrenComponent
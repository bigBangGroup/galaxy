import React from "react";
import withTimer from './withTimer';

class BasicComponent extends React.Component<any, any> {
    state = {
    };

    render() {
        return (
            <React.Fragment>
                <div className="react-basci-component-container">
                    <p>我是一个简单的组件，并没有显示时间的功能，经过withTimer HOC组件包装之后，我就能显示出时间了</p>
                    <p className='red'>经过 withTimer 包装之后，我得props除了来自父组件，也来自 withTimer 的传递</p>
                    <p>在这个demo中 withTimer HOC 只负责给包装的组件赋予一个展示时间的功能，不会修改包装组件的任何逻辑</p>
                    <p>其中显示的时间属性 time 就是 withTimer 组件赋予的，下面显示的时间来自于 withTimer 传递的props</p>
                    <h2 className='red'>{this.props.time.toLocaleString()}</h2>
                </div>
            </React.Fragment>
        );
    }
}

export default withTimer(BasicComponent);

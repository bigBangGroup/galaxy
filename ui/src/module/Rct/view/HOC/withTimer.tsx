import React from "react";

interface withTimerComponentProps {

}

interface withTimerComponentStates {
    time: Date
}

export default function withTimer(WrappedComponent: any) {
    return class withTimerComponent extends React.Component<withTimerComponentProps, withTimerComponentStates>  {
        state = {
            time: new Date()
        };
        componentDidMount() {
            (this as any).timerID = setInterval(() => this.tick(), 1000);
        }

        componentWillUnmount() {
            clearInterval((this as any).timerID);
        }

        tick() {
            this.setState({
                time: new Date()
            });
        }
        render() {
            return <WrappedComponent time={this.state.time} {...this.props} />;
        }
    };
}

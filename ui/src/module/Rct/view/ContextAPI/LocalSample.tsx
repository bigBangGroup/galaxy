import React from "react";
import { Button } from 'antd'

const enLang = {
    submit: "Submit",
    cancel: "Cancel"
};

const zhLang = {
    submit: "提交",
    cancel: "取消"
};

interface ILangPack {
    submit:string,
    cancel:string
}


//设置一个 context 上下文，并设置默认语言包为英文
const LocaleContext = React.createContext(enLang);


interface LocaleProviderProps{

}

interface LocaleProviderStatus {
    locale:string,
    langPack:ILangPack
}

class LocaleProvider extends React.Component<LocaleProviderProps, LocaleProviderStatus> {

    state = {
        locale:'zh',
        langPack:zhLang,
    };

    toggleLocale = () => {
        const { locale } = this.state;
        const pack  = (locale == 'zh' ? enLang : zhLang);
        this.setState({
            locale:(locale == 'zh' ? 'en' : 'zh'),
            langPack:pack
        })

    };


    render(){
        const {locale, langPack} = this.state;
       return(
           <React.Fragment>
               <LocaleContext.Provider value={langPack}>

                   <Button onClick={this.toggleLocale}
                     type="primary" className='mr10'>
                       {locale == 'zh' ? '切换语言' : 'switch language'}
                   </Button>

                   {/*将 LocaleProvider 的子组件直接显示在下面*/}
                   <div className="wrap">
                       {this.props.children}
                   </div>
               </LocaleContext.Provider>
           </React.Fragment>
       )
    }

}



interface LocaledButtonsProps {

}

interface LocaledButtonsStatus {

}

class LocaledButtons extends React.Component<LocaledButtonsProps, LocaledButtonsStatus> {
    render() {
        return (
            <LocaleContext.Consumer>
                {locale => (
                    // 函数作为子组件的方式来消费数据
                    console.log(locale),
                    <div>
                        <Button type="primary" className='mr10 w90'>{locale.cancel}</Button>
                        <Button type="primary" className='mr10 w90'>{locale.submit}</Button>
                    </div>
                )}
            </LocaleContext.Consumer>
        );
    }
}


export default () => (
    <div>
        <LocaleProvider>
            <div className='son-wrap'>
                <br />
                {/*Consumer 一定要在 Provider 层级之内*/}
                <LocaledButtons />
            </div>
        </LocaleProvider>
        {/* LocaledButtons 组件内部的 Consumer 在 Provider 之外，
        那只能获取 React.createContext(enLang) 的默认值*/}
        <div className="mt20 inner-demo">
            <p>下面的 LocaledButtons 组件内部的 Consumer 在 Provider 层级之外</p>
            <p>那只能获取 React.createContext(enLang) 的默认值，且并不能监听到 provider 语言的变化</p>
            <LocaledButtons />
        </div>

    </div>
);

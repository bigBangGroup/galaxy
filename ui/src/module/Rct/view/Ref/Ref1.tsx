

import React from "react";

interface xxxProps {

}

interface xxxStatus {

}

class Ref1 extends React.Component<xxxProps, xxxStatus> {

    render() {
        return (
            <React.Fragment>
                <div className="ref1">
                    我是一个用于测试的子组件
                </div>
            </React.Fragment>
        );
    }
}

export default Ref1;

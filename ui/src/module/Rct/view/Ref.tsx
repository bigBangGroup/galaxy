import React, {ReactInstance, RefObject} from "react";
import Ref1 from './Ref/Ref1';
import { Button, message } from 'antd';
import './../scss/Ref.scss';
import {string} from "prop-types";

interface RefProps {

}

interface RefStatus {

}

class Ref extends React.Component<RefProps, RefStatus> {

  methodRef:any = ''
  ref2:ReactInstance | null = null
  objRef:RefObject<any>  = {
    current:null
  }
  objRef1:RefObject<any>  = {
    current:null
  }
  constructor(props:any) {
    super(props);
    this.objRef = React.createRef();  //初始返回的是 { current: null}
    this.objRef1 = React.createRef();
  }

  componentDidMount(): void {

  }

  getDemo1 = () =>{
    const divDom : any = this.refs.refDiv;
    const comRef1:any = this.refs.ref1;
    divDom.textContent = `change divDom content ${+new Date()}`;
    console.log(comRef1)
  }

  getDemo2 = () =>{
    console.log(this.methodRef);
    console.log(this.ref2)
  }

  getDemo3 = () =>{
    console.log(this.objRef)
    console.log(this.objRef1)
  }

  render() {
    return (
      <React.Fragment>
        <div className="ref-container container">

          <div className='intro-container'>
            <h3 className='title'><strong>Ref</strong></h3>
            <div className="content indent1">
              <p><strong>要点：</strong></p>
              <p>1、用于获取 某个dom 节点，或者某个子组件的实例,对其进行一些手动的操作，
                而不仅仅是props 和 state的方式去更新这个节点。</p>
              <p className='indent1'>比如获取dom节点，自己去绑定某个事件一共三种使用方式</p>
              <p>2、string ref 的方式，最常用的方式，也是最不推荐的方式，下个大版本会被干掉</p>
              <p className='indent1'>如果是一个function component,正常是会失败的，拿到的是undefined，原因是函数组件是没有实例 </p>
              <p>3、function ref 的方式 会传入一个参数，该参数就是对应的实例，如果是 dom节点就是dom实例，如果是组件，就是该组件对应的实例</p>
              <p className='indent1'>然后自己可以在this上挂载在某个属性下，此处是 this.methodRef </p>
              <p>4、create ref, react提供的一个api, React.createRef()初始会返回一个对象，该对象下面有属性 current，初始值会null, </p>
              <p className='indent1'>之后将该对象传给某一个节点或组件，那就会在组件渲染完成之后，将该节点/组件的实例挂载在 该对象下的 current 属性值上</p>
              <p className='indent1'>就可以通过 this.xxx.current的形式调用该节点或属性即可</p>
            </div>
          </div>

          <div className="demo">
            <p><strong>Demo1 string ref 获取子组件或者某个dom节点:</strong></p>
            <div ref='refDiv' className='mg10'>这是一个普通div dom 节点，通过 this.refs.refDiv得到该dom节点 </div>
            <Ref1 ref='ref1'/>
            <div>
              <p className='ml10'>可以点击下面的按钮改变上述div的内容，和得到 ref1子组件的实例（console台查看）</p>
              <Button className='ml10' onClick={this.getDemo1}>click me</Button>
            </div>
          </div>

          <div className="demo">
            <p><strong>Demo2 function ref 获取子组件或者某个dom节点:</strong></p>
            <div ref={ ele => (this.methodRef = ele)} className='mg10'>这是一个普通div dom 节点 </div>
            <Ref1 ref={ ele => (this.ref2 = ele)}/>
            <div>
              <p className='ml10'>可以点击下面的按钮可通过 function ref 的方式得到上面的 div节点和得到 ref1子组件的实例(console台查看)</p>
              <Button className='ml10' onClick={this.getDemo2}>click me</Button>
            </div>
          </div>

          <div className="demo">
            <p><strong>Demo3 create ref 获取子组件或者某个dom节点:</strong></p>
            <div ref={ this.objRef } className='mg10'>这是一个普通div dom 节点，通过 this.refs.refDiv得到该dom节点 </div>
            <Ref1 ref={ this.objRef1 }/>
            <div>
              <p className='ml10'>可以点击下面的按钮可通过 create ref 的方式得到上面的 div节点和得到 ref1子组件的实例(console台查看)</p>
              <Button className='ml10' onClick={this.getDemo3}>click me</Button>
            </div>
          </div>

        </div>
      </React.Fragment>
    );
  }
}

export default Ref;

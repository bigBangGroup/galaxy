import React from "react";
import './../scss/ContextAPI.scss';
import LocalSample from './ContextAPI/LocalSample';

interface ContextAPIProps {

}

interface ContextAPIStatus {

}

class ContextAPI extends React.Component<ContextAPIProps, ContextAPIStatus> {
    state = {
        selectedColor: null,
        selectedAnimal: null,
    };

    render() {
        return (
            <React.Fragment>
                <div className="context-api-container container">
                    <div className='intro-container'>
                        <h3 className='title'><strong>ContextAPI</strong></h3>
                        <div className="content indent1">
                            <p><strong>要点</strong></p>
                            <p>1、解决的问题：</p>
                            <p className='indent'>1.1、组件间通信的问题</p>
                            <p className='indent'>1.2、全局的状态，很多的组件都需要去使用，通过属性一层层传递下来，那就非常麻烦了</p>
                            <p className='indent'>1.3、redux react-router 都重度依赖了这个API，所以说作用非常大</p>
                            <p>2、在react中一直存在，直到16.3版本中作为一个public 的api对外开发开发者就可直接使用这个API去实现一些功能，比如共享全局的一些状态</p>
                            <p>3、父组件(根组件)需要给各层次组件提供数据，而不管该组件是那一层次的组件</p>
                            <p>4、节点分为两种类型。根节点称之为 provide 下面所有子节点称之为 consume（无需上层节点传递数据，如下图展示）</p>
                            <div className="img-box"></div>
                        </div>
                    </div>

                    <div className="use-container">
                        <p><strong>使用方式</strong></p>
                        <p>1、通过 React.createContext(param) 创建一个上下文 context，接受一个任意类型的值 param</p>
                        <p>2、定义 provide 给上下文设置值，</p>
                        <p>3、定义 consume 使用上下文，consume 一定是在 provide 下面，层级上一定是属于 provide 下的某一层的子组件 </p>
                    </div>

                    <div className="demo">
                        <p><strong>Demo:</strong></p>

                        <div className='demo'>
                            <div className="demo-intro">
                                <p>下面切换语言的demo中，先创建一个localContext的上下文，将语言包存在于上下文之中</p>
                                <p>再创建一个 provide，来给其所有的子节点提供当前的语言包</p>
                                <p>当语言切换的时候，提供的语言包也会随之切换，其 provide 的 value 也会随着一起变换，(即动态改变context的数据)那么子组件就会更新其语言</p>
                                <p>provide 层级下的 consume 组件消费根据上层 provide 提供的语言包展现当前语言 </p>
                            </div>
                            <LocalSample />
                        </div>
                    </div>

                </div>
            </React.Fragment>
        );
    }
}

export default ContextAPI;

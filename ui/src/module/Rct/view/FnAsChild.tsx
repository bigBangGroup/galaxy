import React from "react";
import './../scss/FnAsChild.scss';
import SelectTab from './FnAsChild/SelectTab';

const colors: IItemTab[] = [
    { name: "click me show Red", value: "red" },
    { name: "click me show Blue", value: "blue" },
    { name: "click me show Orange", value: "orange" }
];

const animals: IItemTab[] = [
    { name: "click me show Tiger", value: "tiger" },
    { name: "click me show Elephant", value: "elephant" },
    { name: "click me show Cow", value: "cow" }
];


interface IItemTab {
    name: string,
    value: string
}

interface FnAsChildProps {

}

interface FnAsChildStatus {
    selectedColor: string | null,
    selectedAnimal: string | null
}

class FnAsChild extends React.Component<FnAsChildProps, FnAsChildStatus> {
    state = {
        selectedColor: null,
        selectedAnimal: null,
    };

    render() {
        return (
            <React.Fragment>
                <div className="fn-as-child-container container">
                    <div className='intro-container'>
                        <h3 className='title'><strong>函数作为子组件</strong></h3>
                        <div className="content indent1">
                            <p><strong>要点</strong></p>
                            <p>1、是一种设计模式，而非 react 自身的特性</p>
                            <p>2、不是把节点作为child，而是把一个函数作为child</p>
                            <p>3、将组件 render 出的 UI 一部分交给传入的函数去渲染。组件本身负责通用的逻辑部分+通用的UI部分</p>
                        </div>
                    </div>
                    <div className="demo">
                        <p><strong>Demo:</strong></p>
                        <p>下面两个demo公用的逻辑部分是会根据所点击的按钮去显示并渲染对应的内容</p>
                        <p>不同的部分是demo1是显示一个色块，而demo2是需要显示选择的图片，所以这部分的渲染可以根据父组件传入的方法来达到使用同一个子组件部分不同渲染的效果</p>
                        <div className='demo1'>
                            <p>demo1:</p>
                            <SelectTab
                                options={colors}
                                value={this.state.selectedColor}
                                onChange={c => this.setState({ selectedColor: c })}
                            >
                                {(color: string | null) => (
                                    <span
                                        className='select-span'
                                        style={{
                                            display: "inline-block",
                                            backgroundColor: color || '#000',
                                            width: "40px",
                                            height: "40px"
                                        }}
                                    />
                                )}
                            </SelectTab>
                        </div>
                        <div className="demo2">
                            <p>demo2:</p>
                            <SelectTab
                                options={animals}
                                value={this.state.selectedAnimal}
                                onChange={c => this.setState({ selectedAnimal: c })}
                            >
                                {animal => (
                                    <img width="100px" src={require(`../../../images/${animal}.png`)} />
                                )}
                            </SelectTab>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default FnAsChild;

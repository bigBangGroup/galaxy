import React from "react";

interface IItemTab {
    value: string,
    name: string
}

interface SelectTabProps {
    options: IItemTab[],
    value: string | null,
    onChange: (val: string) => any,
    children: (val: string | null) => any,
}

interface SelectTabStatus {

}

class SelectTab extends React.Component<SelectTabProps, SelectTabStatus> {

    render() {
        const { options, value, onChange } = this.props;

        return (
            <React.Fragment>
                <div className="select-tab-container">
                    <ul>
                        {options.map((opt: IItemTab) => (
                            <li
                                key={opt.value}
                                className={`tab-item curp ${
                                    opt.value === this.props.value ? "selected" : ""
                                    }`}
                                onClick={() => onChange(opt.value)}
                            >
                                {opt.name}
                            </li>
                        ))}
                    </ul>

                    {this.props.value && this.props.children(value)}
                </div>
            </React.Fragment>
        );
    }
}

export default SelectTab;

import React from "react";
import './../scss/HOC.scss';
import BasicComponent from './HOC/BasicComponent';

interface HOCProps {
    
}

interface HOCStatus {
    
}

class HOC extends React.Component<HOCProps, HOCStatus> {
    state = {
    };

    render() {
        return (
            <React.Fragment>
                <div className="react-hoc-container container">
                    <div className='intro-container'>
                        <h3 className='title'><strong>React 高阶组件</strong></h3>
                        <div className="content indent1">
                            <p><strong>要点：</strong></p>
                            <p>1、HOC 自身不是 React API 的一部分，它是一种基于 React 的组合特性而形成的<span className='red'>设计模式</span></p>
                            <p>2、有点类似js中的高阶函数(接受另一个函数为参数的函数，称之为高阶函数)，高阶组件是一个会接受另一个组件为参数的函数，返回新的组件</p>
                            <p>3、HOC 不会修改传入的组件，也不会使用继承来复制其行为。相反，HOC 通过将组件包装在容器组件中来组成新组件。<span className='red'>HOC 是纯函数</span>，没有副作用</p>
                            <p>4、不要在 HOC 中修改组件原型（或以其他方式改变它）。</p>
                            <p>5、高阶组件一般不会有自己的UI展现，为其所封装的组件提供额外的功能和数据</p>
                            <p>6、经 HOC 组件封装后，被封装的组件的 props 除了来自其父组件，也来自 包装他的 HOC 组件传递的 属性值，即props</p>
                            <p>7、常见的HOC的例子，比如redux 中的connect函数</p>
                        </div>
                    </div>

                    <div className="demo indent1">
                        <p><strong>Demo:</strong></p>
                        <BasicComponent />
                    </div>

                </div>
            </React.Fragment>
        );
    }
}

export default HOC;

import React from "react";

interface ReactRenderingProps {

}

interface ReactRenderingStatus {

}

class ReactRendering extends React.Component<ReactRenderingProps, ReactRenderingStatus> {

  render() {
    return (
      <React.Fragment>
        <div className="interface-container container">

          <div className='intro-container'>
            <h3 className='title'><strong>React 异步渲染</strong></h3>
            <div className="content indent1">
              <p><strong>要点：</strong></p>
              <p>1、</p>
              <p>2、</p>
              <p>3、</p>
            </div>
          </div>

          <div className="demo">
            <p><strong>Demo:</strong></p>
          </div>

        </div>
      </React.Fragment>
    );
  }
}

export default ReactRendering;

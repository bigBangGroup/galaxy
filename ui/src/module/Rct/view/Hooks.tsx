import React from 'react';
import StateHooks from './Hooks/StateHooks';
import EffectHooks from './Hooks/EffectHooks';
import ContextHooks from './Hooks/ContextHooks';
import ReducerHooks from './Hooks/ReducerHooks';
import ReducerHooks2 from './Hooks/ReducerHooks2';
import MemoHooks from './Hooks/MemoHooks';
import UserHooks from './Hooks/UserHooks';
import ReactMemo from "./Hooks/React-memo";
import CallBackHooks from './Hooks/CallBack';
import './../scss/Hooks.scss';

const ReactHooks = ()=>{
    return(
        <div className="react-hook-container container">
            <div className="demo1">
                <div className="title">React hooks</div>
                <div className="demo demo-container">
                    <div className="title mb10">useState</div>
                    <StateHooks />
                </div>
                <div className="demo demo-container">
                    <div className="title mb10">useEffect</div>
                    <EffectHooks />     
                </div>  
                <div className="demo demo-container">
                    <div className="title mb10">useContext</div>
                    <ContextHooks />     
                </div>  
                <div className="demo demo-container">
                    <div className="title mb10">useReducer</div>
                    <ReducerHooks />     
                </div>   
                <div className="demo demo-container">
                    <div className="title mb10">useReducer demo2</div>
                    <ReducerHooks2 />     
                </div> 
                <div className="demo demo-container">
                    <div className="title mb10">useMemo</div>
                    <MemoHooks />     
                </div>
                <div className="demo demo-container">
                    <div className="title mb10">callBack Hook</div>
                    <CallBackHooks />     
                </div>
                <div className="demo">
                <div className="title mb10">React.memo</div>
                    <ReactMemo />
                </div>
                <div className="demo demo-container">
                    <div className="title mb10">用户自定义 Hook</div>
                    <UserHooks />     
                </div>
            </div>
        </div>
    )
}

export default ReactHooks;
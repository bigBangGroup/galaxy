import React from "react";
import { legendOpt, commonOpt, incomeOpt, rateOpt, userOpt } from "./config";
import "../scss/Bullet.scss";
import Highcharts from "highcharts";
import Exporting from "highcharts/modules/exporting";
import Bullet from "highcharts/modules/bullet";

// 初始化导出模块
Exporting(Highcharts);
// 初始化子弹图模块
Bullet(Highcharts);

// const register
const revenueApi = `/api/revenue`;
const colors:string[] = ["#20b3f7", "#6ed0fd", "#95dcfd", "#c8eeff", "#ffff00"];
const yAxisScaleScope:number[] = [0, 0.2, 0.5, 0.7, 1];
const mock2019 = {
  income: {
    budget: 100,
    actual: 120,
    target: 80,
  },
  rate: {
    budget: 35,
    actual: 50,
    target: 65,
  },
  user: {
    budget: 1200,
    actual: 930,
    target: 900,
  },
};

const RenderLegend = () => {
  return (
    <>
      {legendOpt.map((item: any, index: number) => {
        return (
          <span key={index}>
            <span
              className="color-zone"
              style={{ backgroundColor: item.color }}
            />
            <span className="txt-zone">{item.txt}</span>
          </span>
        );
      })}
    </>
  );
};

interface BulletChartProps {}

enum DescField {
  earnMoney = "income",
  addRate = "rate",
  addUser = "user"
}

interface IRevenue {
  budget: number;
  actual: number;
  target: number;
}

type finialReport = Record<DescField, IRevenue>;

interface BulletChartStates {
  revenue2019: finialReport;
}

class BulletChart extends React.Component<BulletChartProps, BulletChartStates> {
  state = {
    revenue2019: {
      income: {
        budget: 0,
        actual: 0,
        target: 0
      },
      rate: {
        budget: 0,
        actual: 0,
        target: 0
      },
      user: {
        budget: 0,
        actual: 0,
        target: 0
      }
    }
  };

  componentDidMount(): void {
    Highcharts.setOptions(commonOpt);
    this.get2019Revenue();
  }

  get2019Revenue = async () => {
    const revenue2019 = mock2019;
    await this.setState({
      revenue2019
    });
    this.render2019RevenueChart();
  };

  render2019RevenueChart = () => {
    const { revenue2019 } = this.state;
    const { income, rate, user } = revenue2019;
    this.renderBulletByField(income, "income-container", incomeOpt);
    this.renderBulletByField(rate, "rate-container", rateOpt);
    this.renderBulletByField(user, "user-container", userOpt);
  };

  renderBulletByField = (field: IRevenue, domSelect: string, opt: any) => {
    const { budget, target, actual } = field;
    this.calculateScale(budget, opt);
    this.caculateTargetAndActual(target, actual, opt);
    Highcharts.chart(domSelect, opt);
  };

  caculateTargetAndActual = (
    targetVal: number,
    actualVal: number,
    opt: any
  ) => {
    let plotBands: any[] = opt.series;
    plotBands[0] = {
      data: [
        {
          y: actualVal,
          target: targetVal
        }
      ]
    };
  };

  calculateScale = (budget: number, opt: any) => {
    let plotBands: any[] = opt.yAxis.plotBands;
    yAxisScaleScope.map((scale: number, index: number) => {
      if (index == yAxisScaleScope.length - 1) {
        plotBands.push({
          from: budget * scale,
          to: 9e9,
          color: colors[index]
        });
      } else {
        const nextScale = yAxisScaleScope[index + 1];
        plotBands.push({
          from: budget * scale,
          to: budget * nextScale,
          color: colors[index]
        });
      }
    });
  };

  render() {
    return (
      <React.Fragment>
        <div className="bullet-chart-container">
          <div className="legend-wrap">
            <RenderLegend />
          </div>
          <div id="income-container" className="chart-container" />
          <div id="rate-container" className="chart-container" />
          <div id="user-container" className="chart-container" />
        </div>
      </React.Fragment>
    );
  }
}

export default BulletChart;

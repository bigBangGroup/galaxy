const legendOpt = [
  { color: "#4b4b4b", txt: "实际支出" },
  { color: "#20b3f7", txt: "0-20%" },
  { color: "#6ed0fd", txt: "20%-50%" },
  { color: "#95dcfd", txt: "50%-70%" },
  { color: "#c8eeff", txt: "70%-100%" },
  { color: "#FF1493", txt: "标准线" },
  { color: "#ffff00", txt: "超出预算部分" }
];

const commonOpt = {
  chart: {
    inverted: true,
    marginLeft: 135,
    type: "bullet"
  },
  title: {
    text: null
  },
  legend: {
    enabled: false
  },
  yAxis: {
    gridLineWidth: 0
  },
  plotOptions: {
    series: {
      pointPadding: 0.25,
      borderWidth: 0,
      color: "#000",
      targetOptions: {
        width: "200%",
        color: "#FF1493"
      }
    }
  },
  credits: {
    enabled: false
  },
  exporting: {
    enabled: false
  }
} as any;

const incomeOpt = {
  chart: {
    marginTop: 40
  },
  title: {
    text: "2019 项目经营情况"
  },
  xAxis: {
    categories: ['<span class="hc-cat-title">净收入</span><br/> 千元']
  },
  yAxis: {
    plotBands: [

    ],
    title: "title"
  },
  series: [
    {
      data: [
        {
          y: 375,
          target: 250
        }
      ]
    }
  ],
  tooltip: {
    pointFormat: "<b>{point.y}</b> （目标值 {point.target}）"
  }
};

const rateOpt = {
  chart: {
    marginTop: 40
  },
  title: {
    text: "2019 利润率情况"
  },
  xAxis: {
    categories: ['<span class="hc-cat-title">利润率</span><br/> %']
  },
  yAxis: {
    plotBands: [

    ],
    labels: {
      format: '{value}%'
    },
    title: "title"
  },
  series: [
    {
      data: [
        {
          y: 375,
          target: 250
        }
      ]
    }
  ],
  tooltip: {
    pointFormat: "<b>{point.y}</b> （目标值 {point.target}）"
  }
};

const userOpt = {
  chart: {
    marginTop: 40
  },
  title: {
    text: "2019 新用户数量"
  },
  xAxis: {
    categories: ['<span class="hc-cat-title">新用户</span><br/> 数量']
  },
  yAxis: {
    plotBands: [

    ],
    title: "title"
  },
  series: [
    {
      data: [
        {
          y: 375,
          target: 250
        }
      ]
    }
  ],
  tooltip: {
    pointFormat: "<b>{point.y}</b> （目标值 {point.target}）"
  }
};


export { legendOpt, commonOpt, incomeOpt, rateOpt, userOpt };


const demoOpt = {
  chart: {
    marginTop: 40
  },
  title: {
    text: "2019 项目经营情况"
  },
  xAxis: {
    categories: ['<span class="hc-cat-title">净收入</span><br/> 千元']
  },
  yAxis: {
    plotBands: [
      {
        from: 0,
        to: 150,
        color: "#20b3f7"
      },
      {
        from: 150,
        to: 225,
        color: "#6ed0fd"
      },
      {
        from: 225,
        to: 300,
        color: "#95dcfd"
      },
      {
        from: 300,
        to: 9e9,
        color: "#ffff00"
      }
    ],
    title: "title"
  },
  series: [
    {
      data: [
        {
          y: 375,
          target: 250
        }
      ]
    }
  ],
  tooltip: {
    pointFormat: "<b>{point.y}</b> （目标值 {point.target}）"
  }
};


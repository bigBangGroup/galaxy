import React, {Component} from 'react';
import { Map,  Marker} from "react-amap";
import Geolocation from 'react-amap-plugin-geolocation';
import "./../scss/Amap.scss";

const pluginProps = {
    enableHighAccuracy:true,
    timeout: 10000,
    showButton: true
}


class mapComponent extends Component {

    mapCenter = { longitude: 120, latitude: 30 };
    markerPosition = { longitude: 116.46, latitude: 39.92 };
    markerEvents = {
        created: (markerInstance:any) => {
            console.log('高德地图 Marker 实例创建成功；如果你要亲自对实例进行操作，可以从这里开始。比如：');
            console.log(markerInstance.getPosition());
        }
    }
    pluginProps = {
        enableHighAccuracy:true,
        timeout: 10000,
        showButton: true
    }

    render() {

        return (
            <Map zoom={5} center={this.mapCenter} >
                <Geolocation {...pluginProps} />
                <Marker position={this.markerPosition} events={this.markerEvents} />
            </Map>
        )
    }
}

export default mapComponent


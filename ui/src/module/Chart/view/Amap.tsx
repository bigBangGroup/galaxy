import React from "react";

import MapComponent from './mapComponent';
interface AmapProps {}

interface AmapStatus {}

class Amap extends React.Component<AmapProps, AmapStatus> {

  render() {
    return (
      <React.Fragment>
        <div className="amap-container container">
          <div className="map-wrap">
            <MapComponent />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Amap;

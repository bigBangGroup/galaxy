import React from "react";
import { Card, Switch } from "antd";
import "animate.css";
import "../scss/CssAnimation.scss";

const animations: string[] = [
  "bounce",
  "flash",
  "rubberBand",
  "shake",
  "headShake",
  "swing",
  "tada",
  "wobble",
  "jello",
  "bounceIn",
  "bounceInDown",
  "bounceInLeft",
  "bounceInRight",
  "bounceOut",
  "bounceOutDown",
  "bounceOutLeft",
  "bounceOutLeft",
  "bounceOutUp",
  "fadeIn",
  "fadeInDown",
  "fadeInDownBig",
  "fadeInLeft",
  "fadeInLeftBig",
  "fadeInRight",
  "fadeInRightBig",
  "fadeInUp",
  "fadeInUpBig",
  "fadeOut",
  "fadeOutDown",
  "fadeOutDownBig",
  "fadeOutLeft",
  "fadeOutLeftBig",
  "fadeOutRight",
  "fadeOutRightBig",
  "fadeOutUp",
  "fadeOutUpBig",
  "flipInX",
  "flipInY",
  "flipOutX",
  "flipOutY",
  "lightSpeedIn",
  "lightSpeedOut",
  "rotateIn",
  "rotateInDownLeft",
  "rotateInDownRight",
  "rotateInUpLeft",
  "rotateInUpRight",
  "rotateOut",
  "rotateOutDownLeft",
  "rotateOutDownRight",
  "rotateOutUpLeft",
  "rotateOutUpRight",
  "hinge",
  "jackInTheBox",
  "rollIn",
  "rollOut",
  "zoomIn",
  "zoomInDown",
  "zoomInLeft",
  "zoomInRight",
  "zoomInUp",
  "zoomOut",
  "zoomOutDown",
  "zoomOutLeft",
  "zoomOutRight",
  "zoomOutUp",
  "slideInDown",
  "slideInLeft",
  "slideInRight",
  "slideInUp",
  "slideOutDown",
  "slideOutLeft",
  "slideOutRight",
  "slideOutUp"
];

interface CssAnimationProps {}

interface CssAnimationStatus {
  animated: boolean;
  animatedOne: number;
}

class CssAnimation extends React.Component<
  CssAnimationProps,
  CssAnimationStatus
> {
  state = {
    animated: false,
    animatedOne: -1
  };

  animatedAll = (checked: boolean) => {
    checked && this.setState({ animated: true });
    !checked && this.setState({ animated: false });
  };

  animatedOne = (i: number) => {
    this.setState({ animatedOne: i });
  };

  animatedOneOver = () => {
    this.setState({ animatedOne: -1 });
  };

  render() {
    return (
      <React.Fragment>
        <div className="animation-container mt30">
          <div className="mt30 ml10 mb15">
            <span className="mr-s mr10">
              全部动画(单个动画请移动鼠标至相应区域)
            </span>
            <Switch onChange={this.animatedAll} />
          </div>
          {animations.map((v: string, i: number) => (
            <div className="animate-wrap cur-p" key={i}>
              <Card
                className={`animate-box 
                         ${
                           this.state.animated || this.state.animatedOne === i
                             ? "animated infinite"
                             : ""
                         }
                         ${v}`}
                onMouseEnter={() => this.animatedOne(i)}
                onMouseLeave={() => this.animatedOneOver()}
              >
                <div className="pa-m txt-center">
                  <h3>{v}</h3>
                </div>
              </Card>
            </div>
          ))}
        </div>
      </React.Fragment>
    );
  }
}

export default CssAnimation;

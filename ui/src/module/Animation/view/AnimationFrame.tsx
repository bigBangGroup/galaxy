import React from "react";
import { Button } from 'antd';
import './../scss/AnimationFrame.scss';

interface AnimationFrameProps {}

interface AnimationFrameStatus {}

class AnimationFrame extends React.Component<
  AnimationFrameProps,
  AnimationFrameStatus
> {

  importAll  = (r:any) => {
    return r.keys().map(r)
  };

  componentDidMount(): void {
    this.animationFrame();
    this.animationFrameLoop();
  }

  animationFrameLoop = () => {
    const animateWrap=document.querySelector('.animate-wrap-loop');
    const imageList = this.importAll((require as any).context('./../../../images/achu-wink', false, /\.(png|jpe?g|svg)$/));
    this.animation(imageList, animateWrap, true);
  }


  animationFrame = () =>{
    const animateWrap=document.querySelector('.animate-wrap');
    const imageList = this.importAll((require as any).context('./../../../images/slogan-animate', false, /\.(png|jpe?g|svg)$/));
    this.animation(imageList, animateWrap);
  };

  animation = (imageList:any,animateWrap:any, loop = false) => {
    let getImgTags = imageList.map(function(ele:any){
      const img = new Image();
      img.src = ele;
      return img;
    });
    let opts={
      container: animateWrap,
      imgs: getImgTags,
      width: 420,
      height: 722
    };

    const canvas = document.createElement('canvas');
    canvas.id = 'cg-canvas'+Math.random();
    if(opts.container.childNodes.length){
      opts.container.replaceChild(canvas, opts.container.childNodes[0]);
    }else{
      opts.container.appendChild(canvas);
    }
    const realImgs = opts.imgs;
    canvas.width = opts.width||realImgs[0].width;
    canvas.height = opts.height||realImgs[0].height;
    const ctx:any = canvas.getContext('2d');
    let lastStamp = 0, currImg = 0;
    let step = function(stamp:any){
      if(stamp - lastStamp > 150){
        lastStamp = stamp;
        ctx.clearRect(0,0,canvas.width,canvas.height);
        try{
          ctx.drawImage(realImgs[currImg],0,0);
        }
        catch(e){
          console.log(e);
        }
        currImg++;
      }

      if(loop){
        if(currImg >= realImgs.length){
            currImg = 0
        }
        window.requestAnimationFrame(step);
      }else{
        if(currImg >= realImgs.length){

        }else{
          window.requestAnimationFrame(step);
        }
      }


    };
    step(200);
  }

  sloganAnimate = () => {
    this.animationFrame()
  };

  render() {
    return (
      <React.Fragment>
        <div className="animation-frame-container container" >
          <h3 className='mb25'>序列帧 + canvas 绘制动画：</h3>
          <div className='fl'>
            <Button className='ml35' type="primary" onClick={this.sloganAnimate}>animate</Button>
            <div className="animate-wrap mt35 ml35"></div>
          </div>

          <div className='fl'>
            <div className="animate-wrap-loop mt35 ml35"></div>
          </div>

        </div>

      </React.Fragment>
    );
  }
}

export default AnimationFrame;

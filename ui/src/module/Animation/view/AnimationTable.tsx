import React from "react";
import { Table, Popconfirm, Button } from "antd";

interface ICol {
  title: string;
  dataIndex: string;
  width: number | string;
  render?: (val1: IDateItem[], cal2: IDateItem, val3: number) => any;
}

interface IDateItem {
  name: string;
  age: number | string;
  role: string;
  country: string;
  key: number;
}

interface AnimationTableProps {}

interface AnimationTableStatus {
  columns: Partial<ICol>[];
  dataSource: IDateItem[];
  count: number;
  deleteIndex: number;
}

class AnimationTable extends React.Component<
  AnimationTableProps,
  AnimationTableStatus
> {
  columns: Partial<ICol>[] = [
    {
      title: "姓名",
      dataIndex: "name",
      width: "20%"
    },
    {
      title: "角色",
      dataIndex: "role",
      width: "20%"
    },
    {
      title: "年龄",
      dataIndex: "age",
      width: "20%"
    },
    {
      title: "国家",
      dataIndex: "country",
      width: "20%"
    },
    {
      title: "操作",
      dataIndex: "operation",
      render: (text, record, index) => {
        return this.state.dataSource.length > 1 ? (
          <Popconfirm
            title="Sure to delete?"
            onConfirm={() => this.del(record, index)}
          >
            <Button type="primary">Delete</Button>
          </Popconfirm>
        ) : null;
      }
    }
  ];

  dataSource: IDateItem[] = [
    {
      name: "刘备",
      age: 55,
      role: "主公",
      country: "蜀国",
      key: 1
    },
    {
      name: "曹操",
      age: 52,
      role: "主公",
      country: "魏国",
      key: 2
    },
    {
      name: "孙权",
      age: 65,
      role: "主公",
      country: "东吴",
      key: 3
    }
  ];

  state: AnimationTableStatus = {
    columns: this.columns,
    dataSource: this.dataSource,
    count: 3,
    deleteIndex: -1
  };

  del = (record: IDateItem, index: number) => {
    const dataSource = [...this.state.dataSource];
    dataSource.splice(index, 1);
    this.setState({ deleteIndex: record.key });
    setTimeout(() => {
      this.setState({ dataSource });
    }, 500);
  };

  add = () => {
    const { count, dataSource } = this.state;
    const nameArr = [
      "张飞",
      "赵云",
      "马超",
      "孙策",
      "张辽",
      "许褚",
      "徐庶",
      "貂蝉",
      "大桥",
      "司马懿"
    ];
    const countryArr = ["魏国", "蜀国", "吴国"];
    const roleArr = ["文臣", "武将"];
    const radomIndex = Math.random();
    const newData = {
      key: count + 1,
      name: nameArr[Math.floor(radomIndex * 10)],
      age: Math.ceil(radomIndex * 100 + 1),
      role: roleArr[Math.floor(radomIndex * 2)],
      country: countryArr[Math.floor(radomIndex * 3)]
    };
    this.setState({
      dataSource: [...dataSource, newData],
      count: count + 1
    });
  };

  render() {
    const { dataSource, columns } = this.state;
    return (
      <React.Fragment>
        <div className="css-in-table-container container">
          <Button className="editable-add-btn mb-s mb10" onClick={this.add}>
            Add
          </Button>
          <Table
            bordered
            dataSource={dataSource}
            columns={columns}
            rowClassName={(record: IDateItem, index) => {
              if (this.state.deleteIndex === record.key)
                return "animated zoomOutLeft min-black";
              return "animated fadeInRight";
            }}
          />
        </div>
      </React.Fragment>
    );
  }
}

export default AnimationTable;

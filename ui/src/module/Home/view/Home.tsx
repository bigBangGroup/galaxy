import React from "react";
import { Route, Link } from "react-router-dom";
import "./../scss/Home.scss";
import { Layout, Menu, Icon, Modal } from "antd";
import Img from './../../../images/photo.png';
import axios from 'axios'

const { Header, Sider, Content, Footer } = Layout;
const { SubMenu } = Menu;
const msgArr = [
    '刘备三顾茅庐',
    '曹操官渡之战',
    '关于温酒斩华雄',
    '子龙只身入曹营',
    '诸葛亮神机妙算',
    '司马懿隐忍不发',
    '周瑜火烧赤壁',
    '黄忠箭无虚发',
    '鲁子敬大义为先',
    '张飞万人敌',
];

class Home extends React.Component<any, any> {

  state = {
    collapsed: false,
    visible: false
  };

  componentDidMount(): void {
    const bgColor = localStorage.getItem("bgColor");
    bgColor && this.setBgColor(bgColor);

    axios.get(`/rank/total-rank/muse/champions`,{},);

  }
  setBgColor = (bgColor:string) =>{
    const headEle:any = document.querySelector('.ant-layout-header');
    headEle && (headEle.style.backgroundColor = bgColor);
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };

  // 渲染有子路由的菜单
  renderMenuWithSub = (routeWithSub: any) => {
    const subRoutes = routeWithSub.routes;
    return (
      <SubMenu
        key={`submenu + ${routeWithSub.path}`}
        title={
          <span>
            <Icon type={routeWithSub.icon} />
            <span>{routeWithSub.name}</span>
          </span>
        }
      >
        {subRoutes.map((subRoute: any, index: number) => {
          return (
            <Menu.Item key={`submenu-item + ${subRoute.path}+ ${index}`}>
              <Link to={subRoute.path}>
                <span>{subRoute.name}</span>
              </Link>
            </Menu.Item>
          );
        })}
      </SubMenu>
    );
  };

  // 渲染菜单
  renderMenu = (routeItem: any) => {
    return (
      <Menu.Item key={`menu-id + ${routeItem.path}`}>
        <Link to={routeItem.path}>
          <Icon type={`${routeItem.icon}`} />
          <span>{routeItem.name}</span>
        </Link>
      </Menu.Item>
    );
  };

  // 渲染左侧的菜单栏
  sideBarMenuRender = () => {
    const { routes } = this.props;
    return (
      <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
        {routes.map((routeItem: any, index: number) => {
          const subRoutes = routeItem.routes;
          return subRoutes
            ? this.renderMenuWithSub(routeItem)
            : this.renderMenu(routeItem);
        })}
      </Menu>
    );
  };

  // 渲染带有子路由的路由出口
  renderRouterWithSub = (routes: any) => {
    return routes.map((routeItem: any, index: number) => (
      <Route
        key={`sub-route-${index}`}
        exact={routeItem.exact}
        path={routeItem.path}
        component={routeItem.component}
      />
    ));
  };

  // 渲染路由出口
  renderRouters = () => {
    const { routes } = this.props;
    return (
      <div className="router-wrap">
        {routes.map((routeItem: any, index: number) => {
          const routes = routeItem.routes;
          return routes ? (
            this.renderRouterWithSub(routes)
          ) : (
              <Route
                key={`route + ${index}`}
                exact={routeItem.exact}
                path={routeItem.path}
                component={routeItem.component}
              />
            );
        })}
      </div>
    );
  };

  logOut = () => {
    const _waterMark = (window as any)._waterMark;
    _waterMark && _waterMark.destroy();
    localStorage.removeItem('userName');
    window.location.hash = '#'
  };

  showMsg = () =>{
    this.setState({
      visible: true,
    });
  };

  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };

  render() {
    const { collapsed } = this.state;
    return (
      <React.Fragment>
        <div className="home-container">
          <Layout style={{ minHeight: "100vh" }}>
            <Sider trigger={null} collapsible collapsed={collapsed}>
              <div className="logo" />
              {this.sideBarMenuRender()}
            </Sider>
            <Layout>
              <Header style={{ background: "#fff", padding: 0 }}>
                <Icon
                  className="trigger"
                  type={this.state.collapsed ? "menu-unfold" : "menu-fold"}
                  onClick={this.toggle}
                />
                <div className="menu-header-container">
                  <div className="logout fr" onClick={this.logOut}>
                    log out
                  </div>
                  <div className="img-box fr">
                    <img src={Img} alt=""/>
                  </div>
                  <div className="message-box cur-p fr" onClick={this.showMsg}>
                    <Icon type="message" className='message'/>
                    <span className='msg-count'>9+</span>
                  </div>
                </div>
              </Header>
              <Content style={{ margin: "0 16px" }}>
                {this.renderRouters()}
              </Content>
              {/* <Footer style={{ textAlign: "center" }}>
                galaxy ©2019 Created by yangjian
              </Footer> */}
            </Layout>
          </Layout>
        </div>
        <div>
          <Modal
              className='msg-container'
              title="message "
              visible={this.state.visible}
              onCancel={this.handleCancel}
              footer={null}
          >
            {
              msgArr.map((msg:string, index:number)=>(
                  <div key={index}>{msg}</div>
              ))
            }

          </Modal>
        </div>
      </React.Fragment>
    );
  }
}

export default Home;

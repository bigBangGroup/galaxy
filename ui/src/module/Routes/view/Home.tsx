
import React from "react";

interface HomeProps {

}

interface HomeStatus {

}

class Home extends React.Component<HomeProps, HomeStatus> {

  render() {
    return (
        <React.Fragment>
          <div className="interface-container container">
            <div className='intro-container'>
              <h3 className='title'><strong>typescript 接口</strong></h3>
              <div className="content indent1">
                <p><strong>要点：</strong></p>
                <p>1、</p>
                <p>2、</p>
                <p>3、</p>
              </div>
            </div>

            <div className="use"></div>

            <div className="demo">
              <p><strong>Demo:</strong></p>
            </div>

          </div>
        </React.Fragment>
    );
  }
}

export default Home;

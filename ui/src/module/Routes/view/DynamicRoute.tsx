
import React from "react";

interface DynamicRouteProps {
    match:any
}

interface DynamicRouteStatus {

}

class DynamicRoute extends React.Component<DynamicRouteProps, DynamicRouteStatus> {

    componentDidMount(){
        //获取动态路由的传值
        let id = this.props.match.params.id;
        console.log(this.props.match.params.id);
    }

  render() {

    let id = this.props.match.params.id;
    return (
        <React.Fragment>
          <div className="dynamic-routes-container container">
            动态路由传值跳转,动态路由值为: <span className='red'>{id}</span>
          </div>
        </React.Fragment>
    );
  }
}

export default DynamicRoute;

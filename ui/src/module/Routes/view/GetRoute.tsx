
import React from "react";
import url from "url";

interface GetRouteProps {
    location:any
}

interface GetRouteStatus {

}

class GetRoute extends React.Component<GetRouteProps, GetRouteStatus> {

    //生命周期函数
    componentDidMount(){
        //获取get传值 第二个需要传入true,如果是false的话query则不会被解析为对象。
        console.log(url.parse(this.props.location.search,true));
    }
    render() {
        const id = (url.parse(this.props.location.search,true)).query.id;
        return (
            <React.Fragment>
              <div className="get-routes-container container">
                  动态路由传值跳转,动态路由值为:
                  <span className='red'>{id}</span>
              </div>
            </React.Fragment>
        );
  }
}

export default GetRoute;

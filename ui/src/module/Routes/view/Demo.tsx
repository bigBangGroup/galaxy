import React from "react";
import { Link } from "react-router-dom";
import './../scss/Demo.scss';

interface routeItem {
  id:string | number
  title:string
}

const listRoutes1:routeItem[]= [
  {
    id:'111111111',
    title:'点击我会携带 id = 111111111 放在路由中跳转'
  },
  { id:'22222222',
    title:'点击我会携带 id = 22222222 放在路由中跳转'
  },
  { id:'3333333',
    title:'点击我会携带 id = 3333333 放在路由中跳转'
  }
];

const listRoutes2: routeItem[] = [
  {
    id:'111111111',
    title:'点击我会携带 id = 111111111 放在url ？ 之后使用get传值'
  },
  {
    id:'22222222',
    title:'点击我会携带 id = 22222222 放在url ？ 之后使用get传值'
  },
  {
    id:'33333333',
    title:'点击我会携带 id = 33333333 放在url ？ 之后使用get传值'
  }
];

interface RouteDemoProps {

}

interface RouteDemoStatus {

}


class Demo extends React.Component<RouteDemoProps, RouteDemoStatus> {

  render() {
    return (
      <React.Fragment>
        <div className="demo-container container">
          <div className='intro-container'>
            <h3 className='title'><strong>RouteDemo</strong></h3>
            <div className="content indent1">
              <p><strong>要点：</strong></p>
              <p>1、SPA页面路由跳转，切换模块</p>
              <p>2、路由应该将其模块化，独立出去，方便维护和管理</p>
              <p>3、动态传值和 get 传值</p>
            </div>
          </div>

          <div className="demo">
            <p><strong>Demo1:</strong></p>
            <div>
              <p>点击下面的列表，会发携带id一起路由跳转，观察跳转之后的页面的url部分会有个动态的id，如下所示：</p>
              <ul>
                {
                  listRoutes1.map((route:routeItem, index) => (
                      <li key={index}>
                        <Link to={`/admin/Routes/demo/dynamic/${route.id}`}>{route.title}</Link>
                      </li>
                  ))
                }
              </ul>
            </div>
          </div>

          <div className="demo">
            <p><strong>Demo2:</strong></p>
            <div>
              <p>点击下面的列表，在url部分的最后以 '?' 结尾，后面追加参数如下所示：</p>
              <ul>
                {
                  listRoutes2.map((route:routeItem, index) => (
                      <li key={index}>
                        <Link to={`/admin/Routes/demo/get?id=${route.id}`}>{route.title}</Link>
                      </li>
                  ))
                }
              </ul>
            </div>
          </div>


        </div>
      </React.Fragment>
    );
  }
}

export default Demo;

import React from "react";


interface IRecordProp {

}

interface IRecordStatus {

}

interface RecordModule{
  props:IRecordProp,
  state:IRecordStatus
}

type petsGroup = 'dog' | 'cat' | 'fish';
interface IPetInfo {
  name:string,
  age:number,
}

type IPets = Record<petsGroup, IPetInfo>;

//省略式声明 类名和接口名同名
class RecordModule extends React.Component{

  render() {
    return (
      <React.Fragment>
        <div className="interface-container container">
          <div className='intro-container'>
            <h3 className='title'><strong>Typescript 高级类型 Record</strong></h3>
            <div className="content indent1">
              <p><strong>要点：</strong></p>
              <p>1、ts中的一个实用工具类型</p>
              <p>2、其作用是构造一个类型，其属性名的类型为K，属性值的类型为T。可将某个类型的属性映射到另一个类型上。</p>
            </div>
          </div>

          <div className="use">
            <p>看下Record的源码。</p>
            <div className="code">
              <code>
                /*
                   Construct a type with a set of properties K of type T
                */
                <p>
                  {"type Record< extendssa keyof any T>"}
                </p>
                <p className='indent1'>[P in K]: T;</p>
                <p>};</p>
              </code>
            </div>

            <p className='mt10'>好像源码也比较简单，即将K中的每个属性([P in K]),都转为T类型。常用的格式如下：</p>
            <div className='code'>
              {"type proxyKType = Record<K,T>"}
            </div>

            <div className='mt10'>会将K中的所有属性值都转换为T类型，并将返回的新类型返回给proxyKType，K可以是联合类型、对象、枚举…
              看几个demo.
            </div>
          </div>

          <div className="demo">
            <p><strong>Demo:</strong></p>

            <div className="code">
              <p><code>type petsGroup = 'dog' | 'cat';</code></p>
              <div>
                <p><code>{ "interface IPetInfo {"}</code></p>
                <p className='indent1'><code>name:string,</code></p>
                <p className='indent1'><code>age:number</code></p>
                <p>}</p>
              </div>
              <div>
                <p><code>{"type IPets = Record<petsGroup, IPetInfo>;"}</code></p>
              </div>
              <div>
                <p><code>{"const animalsInfo:IPets = {"}</code></p>
                <div className="indent1">
                  <p><code>{"dog:{"}</code></p>
                  <p className="indent2"><code>name:'dogName',</code></p>
                  <p className="indent2"><code>age:2</code></p>
                  <p>},</p>
                </div>
                <div className="indent1">
                  <p><code>{"cat:{"}</code></p>
                  <p className="indent2"><code>name:'catName',</code></p>
                  <p className="indent2"><code>age:5</code></p>
                  <p>}</p>
                </div>
                <p>}</p>
              </div>
            </div>

            <div className='mt10'>
              上面 IPets 类型是由 {"Record<petsGroup, IPetInfo>"}返回的。将petsGroup中的每个值(‘dog’ | ‘cat’ | ‘fish’)都转为 IPetInfo 类型。
            </div>

            <div>
              <p>当然也可以自己在第一个参数后追加额外的值，比如上述demo中代码改为</p>
              <div className='code'>
                {"type IPets = Record<petsGroup | 'otherAnamial', IPetInfo>;\n"}
              </div>
              <p className='mt10'>那么 animalsInfo 就需要增加 otherAnamial的值</p>
            </div>
          </div>

        </div>
      </React.Fragment>
    );
  }
}

export default RecordModule;

import React from "react";
import Login from './../../../component/Login/Login';
import "../scss/Home.scss";

class Home extends React.Component<any, any> {
  state = {
    collapsed: false
  };

  componentDidMount(): void {

  }

    render() {
    return (
      <React.Fragment>
        <div className="login-wrap">
            <div className="login-box">
                <Login />
            </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Home;

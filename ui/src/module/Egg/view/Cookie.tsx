import React from "react";
import http from "../../../utils/http";
import { Button, message } from "antd";
import {AxiosResponse} from "axios";

interface CookieProps {

}

interface CookieStatus {
    cookie:string
}

class Cookie extends React.Component<CookieProps, CookieStatus> {

    state = {
        cookie:document.cookie
    };

    setCookie = async () =>{
        const api = '/api/cookie/set';
        const res = await http.get(api);
        message.success(res.message);
        this.setState({
            cookie: document.cookie
        })
    };

    encryptCookie = async ()=>{
        const api = '/api/cookie/encrypt';
        const res = await http.get(api);
        message.success(res.message);
        this.setState({
            cookie: document.cookie
        })
    };

    render() {
        const { cookie } = this.state
        return (
            <React.Fragment>
                <div className="cookie-container container">
                    <div className='intro-container'>
                        <h3 className='title'><strong>Egg Cookie</strong></h3>
                        <p>
                            <strong className='red'>注意：</strong>
                            egg模块需要本地启动egg服务。cd egg文件夹，运行 run run dev,
                            记下egg服务在那个端口启动并在setupProxy.js 文件中修改代理的端口号
                        </p>
                        <div className="content indent1">
                            <p><strong>要点：</strong></p>
                            <p>1、解决http 请求是无状态的，但是 Web 服务通常都需要知道发起请求的人是谁</p>
                            <p>2、服务端可以通过响应头（set-cookie）将少量数据响应给客户端，浏览器会遵循协议将数据保存，并在下次请求同一个服务的时候带上</p>
                            <p>3、浏览器也会遵循协议，只在访问符合 Cookie 指定规则的网站时带上对应的 Cookie 来保证安全性</p>
                            <p>4、Cookie 在 Web 应用中经常承担标识请求方身份的功能</p>
                            <p>5、cookie 一般由服务端维护，服务端可设置是否加密、过期时间，以及js是否能访问，修改</p>
                        </div>
                    </div>

                    <div className="use">
                        <div className="">展示页面cookie:</div>
                        <div className='demo'>
                            {cookie}
                        </div>
                        <div className='mt30'>
                            <div className='mb10'>点击下面按钮，服务端会设置 x-token:testUser 进入cookie</div>
                            <Button className='mg15 mr20' type="primary" onClick={this.setCookie}>set cookie</Button>
                            <Button className='mg15' type="primary" onClick={this.encryptCookie}>encrypt cookie</Button>
                        </div>
                    </div>

                </div>
            </React.Fragment>
        );
    }
}

export default Cookie;

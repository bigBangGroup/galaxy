import React from "react";
import http from "../../../utils/http";
import {Table, Button, Popconfirm, message} from "antd";
import { exportListToXlsx } from './../../../utils/excel';
import UserInfo from './UserInfo';
import './../scss/Home.scss';

// const register
const apiUserList = `/api/user/list`;
const apiUserAdd = `/api/user/add`;
const apiUserDel = `/api/user/del`;
const apiUserUpdate = `/api/user/update`;
const apiUserRestore = `/api/user/restore`;
const excelTitle:string[] = ['序号', '姓名', '角色', '年龄', '国家'];

const UserInfoContext = React.createContext({});

interface IUserInfo {
    id?: number;
    name: string;
    age: number;
    role: string;
    country: string;
}

interface ICol {
    id: number;
    title: string;
    dataIndex: string;
    width: number | string;
    render?: (val1: IUserInfo[], cal2: IUserInfo, val3: number) => any;
}

interface EggProps {

}

interface EggStatus {
    dataSource: IUserInfo[] | [];
    showUserInfo: boolean;
    deleteIndex: number;
    editUserInfo: IUserInfo | null
}

class Egg extends React.Component<EggProps, EggStatus> {

    columns: Partial<ICol>[] = [
        {
            title: "序号",
            dataIndex: "index",
            width: "100px",
            render:(text, record, index) => (<div>{index + 1}</div>)
        },
        {
            title: "姓名",
            dataIndex: "name",
            width: "15%"
        },
        {
            title: "角色",
            dataIndex: "role",
            width: "15%"
        },
        {
            title: "年龄",
            dataIndex: "age",
            width: "15%"
        },
        {
            title: "国家",
            dataIndex: "country",
            width: "20%"
        },
        {
            title: "操作",
            dataIndex: "operation",
            render: (text, record, index) => {
                return this.state.dataSource.length > 0 ? (
                   <div style={{textAlign:"center"}}>
                       <Popconfirm
                           title="Sure to delete?"
                           onConfirm={() => this.del(record, index)}
                       >
                           <Button type="primary">Delete</Button>
                       </Popconfirm>
                       <Button
                           type="primary"
                           className='ml20'
                           onClick={() => this.openUserInfo(record)}
                       >Update</Button>
                   </div>
                ) : null;
            }
        }
    ];

    state = {
        showUserInfo:false,
        dataSource:[],
        deleteIndex:-1,
        editUserInfo:null
    };

    componentDidMount(): void {
        this.getUserList()
    }

    async getUserList() {
        const sendParams = {
            from: 0,
            to:999
        };
        const userList = await http.get(apiUserList, {params: sendParams});
        this.setState({
            dataSource:userList
        })
    }

    updateUserInfo = async (editUserInfo:IUserInfo) => {
        const api:string = (editUserInfo.id) ? apiUserUpdate : apiUserAdd;
        const res = await http.post(api, {params: editUserInfo});
        message.success(res.message);
        this.getUserList();
        this.setState({
            showUserInfo: false,
            editUserInfo: null
        })
    };

    restoreUserList = async () =>{
        const sendParams = {
            type:'userList'
        };
        const dataSource = await http.put(apiUserRestore, {params:sendParams});
        this.setState({dataSource});
        message.success('恢复数据成功');
    };

    openUserInfo = async (userInfo:IUserInfo) =>{
        this.setState({
            showUserInfo:true,
            editUserInfo:userInfo
        })
    };

    exportExcel = () => {
        const { dataSource } = this.state;
        const excelArr:any = [ excelTitle ];
        dataSource.map((userInfo:IUserInfo) => {
            excelArr.push([
                userInfo.id,
                userInfo.name,
                userInfo.role,
                userInfo.age,
                userInfo.country
            ])
        });
        exportListToXlsx(excelArr, '新三国用户信息.xlsx');
    };

    addUser = async () => {
        this.setState({
            showUserInfo:true,
            editUserInfo:null
        })
    };

    del = async (record: IUserInfo, index: number) => {
        const sendParams = {
            id:record.id
        };
        const res = await http.delete(apiUserDel, {params: sendParams});
        this.getUserList();
        message.success(res.message);
    };

    render() {
        const { dataSource, showUserInfo, editUserInfo } = this.state;
        return (
            <React.Fragment>
                <div className="interface-container container">
                    <div className='intro-container'>
                        <h3 className='title'><strong>Egg</strong></h3>
                        <div className="content">
                            <p>
                                <strong className='red'>注意：</strong>
                                egg模块需要本地启动egg服务。cd egg文件夹，运行 run run dev,
                                记下egg服务在那个端口启动并在setupProxy.js 文件中修改代理的端口号
                            </p>
                        </div>
                    </div>

                    <div className="">
                        <p><strong>Demo:</strong></p>
                        <p>操作下面列表，会分别使用get(获取列表)、post(修改用户信息)、put(恢复用户列表)、del(删除用户)接口与egg交互</p>
                        <div className='opt-wrap mb15 mt10 mr10'>
                            <Button className='fr mg15' type="primary" onClick={this.exportExcel}>export excel</Button>
                            <Button className='fr mg15 mr20' type="primary" onClick={this.restoreUserList}>restore</Button>
                            <Button className='fr mg15 mr20' type="primary" onClick={this.addUser}>add user</Button>
                        </div>
                        <div className="table-wrap">
                            <Table
                                rowKey="id"
                                bordered
                                dataSource={dataSource}
                                columns={this.columns}
                                rowClassName={(record: IUserInfo, index:any) => {
                                    if (this.state.deleteIndex === record.id)
                                        return "animated zoomOutLeft min-black";
                                    return "animated fadeInRight";
                                }}
                            />
                        </div>
                    </div>

                    {
                        showUserInfo ?
                            <UserInfo
                            userInfo={editUserInfo}
                            updateUser={this.updateUserInfo}
                            handleCancel={() => this.setState({showUserInfo:false})}
                            /> :null
                    }

                </div>
            </React.Fragment>
        );
    }
}

export default Egg;

import React from "react";
import {Button, message} from "antd";
import http from "../../../utils/http";

interface SessionProps {

}

interface SessionStatus {

}

class Session extends React.Component<SessionProps, SessionStatus> {

    setSession = async () => {
        const api = '/api/session/set';
        const res = await http.get(api);
        message.success(res.message);
    };

    destroySession = async () => {
        const api = '/api/session/destroy';
        const res = await http.get(api);
        message.success(res.message);
    };

    render() {
        return (
            <React.Fragment>
                <div className="session-container container">
                    <div className='intro-container'>
                        <h3 className='title'><strong>Egg session</strong></h3>
                        <p>
                            <strong className='red'>注意：</strong>
                            egg模块需要本地启动egg服务。cd egg文件夹，运行 run run dev,
                            记下egg服务在那个端口启动并在setupProxy.js 文件中修改代理的端口号
                        </p>
                        <div className="content indent1">
                            <p><strong>要点：</strong></p>
                            <p>1、在 Cookie 的基础上封装了 Session 的概念，专门用做用户身份识别。</p>
                            <p>2、Session 的实现是基于 Cookie 的，默认配置下，用户 Session 的内容加密后直接存储在 Cookie 中的一个字段中</p>
                            <p>3、用户每次请求我们网站的时候都会带上这个 Cookie，我们在服务端解密后使用</p>
                        </div>
                    </div>
                    <div style={{paddingLeft: 20}}>
                        <div className='mb10'>查看浏览器中的 Application 中的Cookie 其中没有 sessionId字段，点击下面 set 按钮服务端会设置 sessionId
                            并将其加密
                        </div>
                        <div className='mb25'>点击下面 destroy 按钮服务端会清空 sessionId</div>
                        <Button className='mg15 mr20' type="primary" onClick={this.setSession}>set session</Button>
                        <Button className='mg15 mr20' type="primary" onClick={this.destroySession}>destroy session</Button>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Session;

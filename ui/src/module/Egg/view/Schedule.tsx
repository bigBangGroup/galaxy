
import React from "react";
import http from "../../../utils/http";
import echarts from 'echarts';

const opt:any = {
    color: ['#3398DB'],
    tooltip : {
        trigger: 'axis',
        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            data : ['亲和度'],
            axisTick: {
                alignWithLabel: true
            }
        }
    ],
    yAxis : [
        {
            type : 'value'
        }
    ],
    series : [
        {
            name:'直接访问',
            type:'bar',
            barWidth: '60%',
            data:[100]
        }
    ]
};


interface ScheduleProps {

}

interface ScheduleStatus {

}
let a = 100;
class Schedule extends React.Component<ScheduleProps, ScheduleStatus> {

    timerId:any = null
    componentDidMount(): void {
        this.initGraph();
        this.timerId = setInterval(this.getUserAffinity, 10 * 1000)
    }

    getUserAffinity = async () => {
        const api = '/api/user/affinity';
        const userAffinity = await http.get(api);
        console.log(userAffinity);
        this.initGraph(userAffinity.affinity);
    };

    initGraph = (affinity?:number) => {
        // 基于准备好的dom，初始化echarts实例
        let myChart = echarts.init((document as any).getElementById('bar-graph-wrap'));
        myChart && myChart.clear();
        affinity && (opt.series[0].data = [affinity]);
        console.log(opt);
        myChart.setOption(opt);
    };

    componentWillUnmount(): void {
        this.timerId && (window.clearTimeout(this.timerId))
    }

    render() {
        return (
            <React.Fragment>
                <div className="schedule-container container">
                    <div className='intro-container'>
                        <h3 className='title'><strong>Egg 定时任务</strong></h3>
                        <p>
                            <strong className='red'>注意：</strong>
                            egg模块需要本地启动egg服务。cd egg文件夹，运行 run run dev,
                            记下egg服务在那个端口启动并在setupProxy.js 文件中修改代理的端口号
                        </p>
                        <div>下面的例子表示用户和本网站的亲和度，通过egg定时任务每隔10s亲和度就会 +26，返回到前端，重新渲染柱状图</div>
                        <div className="">
                            <div className="chart-wrap ml35">
                                <div className="bar-graph-wrap mr35 graph-wrap" id="bar-graph-wrap" style={{ width: 100, height: 530 }}></div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Schedule;

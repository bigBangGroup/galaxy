import React from "react";
import { Modal, Form, Input, Select } from 'antd';
import { FormComponentProps } from 'antd/lib/form';

const Option = Select.Option;
const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 4 }
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 }
    }
};

interface IUserInfo {
    id?: number;
    name: string;
    role: string;
    age: number;
    country: string;
}

interface UserInfoProps extends FormComponentProps{
    updateUser:(userInfo:IUserInfo) => void
    handleCancel:() => void
    userInfo:IUserInfo | null
}

interface UserInfoStatus {
    name:string
    role:string
    age:number | null
    country:string
}

class UserInfo extends React.Component<UserInfoProps, UserInfoStatus, any> {
    state = {
        name:'',
        role:'',
        age:null,
        country:''
    };

    changeUserName = (e:any) => {
        const name = e.target.value;
        this.setState({
            name
        });
    };

    changeUserRole = (e:any) => {
        const role = e.target.value;
        this.setState({
            role
        });
    };

    changeUserAge = (e:any) => {
        const age = e.target.value;
        this.setState({
            age
        });
    };

    changeUserCountry = async (val:any, param:any) =>{
        this.setState({
            country:val
        });
    };

    renderCountryOption = () => {
        return ['魏国', '蜀国', '吴国'].map((item:string, index:number) => (
            <Option key={index} value={item} data-k={item}>
                {item}
            </Option>
        ))
    };

    handleOk = (e:React.MouseEvent) => {
        const userInfo:IUserInfo = this.validateForm(e);
        const { updateUser } = this.props;
        userInfo && updateUser && updateUser(userInfo);
    };

    handleCancel = (e:React.MouseEvent) => {
        const { handleCancel } = this.props;
        handleCancel && handleCancel();
    };

    validateForm = (e:any):IUserInfo => {
        e.preventDefault();
        let userInfo:IUserInfo | null = null;
        this.props.form.validateFields((err:any, values:IUserInfo) => {
            if (err) {
                // 错误处理
            } else {
                userInfo = values;
                this.props.userInfo && (userInfo.id = this.props.userInfo.id)
            }
        });
        return userInfo!
    };

    render() {
        const { userInfo } = this.props;
        const { handleOk, handleCancel } = this;
        const { getFieldDecorator } = this.props.form;

        return (
            <React.Fragment>
                <div className="user-info-container container">
                    <Modal
                        title="user info"
                        visible={true}
                        onOk={handleOk}
                        onCancel={handleCancel}
                    >
                        <Form
                            {...formItemLayout}
                            className="login-form"
                        >
                            <Form.Item label="姓名">
                                {getFieldDecorator("name", {
                                    initialValue: userInfo && userInfo.name,
                                    rules: [
                                        {
                                            required: true,
                                            message: "请输入人物姓名"
                                        },
                                    ]
                                })(
                                    <Input
                                        placeholder="请输入人物姓名"
                                        onChange={this.changeUserName}
                                    />
                                )}
                            </Form.Item>

                            <Form.Item label="角色">
                                {getFieldDecorator("role", {
                                    initialValue: userInfo && userInfo.role,
                                    rules: [
                                        {
                                            required: true,
                                            message: "角色"
                                        },
                                    ]
                                })(
                                    <Input
                                        placeholder="请输入人物角色"
                                        onChange={this.changeUserRole}
                                    />
                                )}
                            </Form.Item>

                            <Form.Item label="年龄">
                                {getFieldDecorator("age", {
                                    initialValue: userInfo && userInfo.age,
                                    rules: [
                                        {
                                            required: true,
                                            message: "年龄"
                                        },
                                        {
                                            type: "integer",
                                            message: "请输入整数",
                                            transform: value => {
                                                return Number(value);
                                            }
                                        }
                                    ]
                                })(
                                    <Input
                                        placeholder="请输入人物年龄"
                                        onChange={this.changeUserAge}
                                    />
                                )}
                            </Form.Item>

                            <Form.Item label="国家">
                                {getFieldDecorator("country", {
                                    validateTrigger: ["onBlur"],
                                    initialValue: userInfo && userInfo.country,
                                    rules: [
                                        {
                                            required: true,
                                            message: "请选人物所属国家"
                                        }
                                    ]
                                })(
                                    <Select onChange={this.changeUserCountry}>
                                        {this.renderCountryOption()}
                                    </Select>
                                )}
                            </Form.Item>
                        </Form>
                    </Modal>
                </div>
            </React.Fragment>
        );
    }
}

const WrapCollect = Form.create<UserInfoProps>()(UserInfo);
export default WrapCollect

import React from "react";
import http from "../../../utils/http";

import { Button, message } from "antd";

interface MiddlewareProps {

}

interface MiddlewareStatus {

}

class Middleware extends React.Component<MiddlewareProps, MiddlewareStatus> {

    auth = async () =>{
        const api = '/api/auth/authorization';
        const res = await http.get(api);
        message.success(res.message)
    };

    getData = async () =>{
        const api = '/api/auth/getData';
        const res = await http.get(api);
        message.success(res.message)
    };

    authRemove = async () =>{
        const api = '/api/auth/remove';
        const res = await http.get(api);
        message.success(res.message)
    };

    render() {
        return (
            <React.Fragment>
                <div className="interface-container container">
                    <div className='intro-container'>
                        <h3 className='title'><strong>Egg Middleware</strong></h3>
                        <p>
                            <strong className='red'>注意：</strong>
                            egg模块需要本地启动egg服务。cd egg文件夹，运行 run run dev,
                            记下egg服务在那个端口启动并在setupProxy.js 文件中修改代理的端口号
                        </p>
                        <div className="content indent1">
                            <p><strong>要点：</strong></p>
                            <p>1、Egg 的中间件形式和 Koa 的中间件形式是一样的，都是基于洋葱圈模型。</p>
                            <p>2、用于在匹配路由进入 controller 层之前或之后做一些业务处理</p>
                            <p>3、全局的中间件，访问会处理每一次请求。 如果只想针对单个路由生效，可以直接在 app/router.js 中实例化和挂载</p>
                            <p>4、可用 match/ignore 针对某一类(个)路由设置特定类型的中间件</p>
                        </div>
                    </div>

                    <div className="">
                        <p><strong>Demo:</strong></p>
                        <p>点击下面按钮请求服务端数据会由中间件进行权限验证，没有权限则直接在中间件返回无权限，访问不了该接口</p>
                        <p>点击下面授权按钮，则会在cookie中增加验证字段赋予用户权限，通过中间件的验证，获得数据</p>
                        <p>点击下面移除授权的按钮，怎会将该权限去除。</p>
                        <Button type="primary" className='mr20' onClick={this.getData}>get msg</Button>
                        <Button type="primary" className='mr20' onClick={this.auth}>auth btn</Button>
                        <Button type="primary" className='mr20' onClick={this.authRemove}>remove auth</Button>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Middleware;

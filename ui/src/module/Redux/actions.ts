
import { CHANGE_PAGE, GET_USER_INFO } from './actionTypes'

export const changePage = (paramDate:any) => (dispatch:any) =>{
    dispatch({
        type: CHANGE_PAGE,
        payload:paramDate
    })
}

export const getUserInfo  = () => (dispatch:any) => {
    setTimeout(() => {
        const userInfo = {
            userName:'yangjian',
            mail:'yangjian_web@163.com',
            uid:88888888
        };
        dispatch({
            type: GET_USER_INFO,
            payload:userInfo
        })
    }, 500)


}

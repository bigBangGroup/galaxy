
import { GET_USER_INFO } from './actionTypes';
import { createReducer } from '../../utils/redux/reducer'

//存储登录用户的基本信息.

interface IUserInfo{
    userName:string
    mail:string
    uid:number | null
}

let userInfo:IUserInfo = {
    userName:'',
    mail:'',
    uid:null
};

const getUserInfo = (state:any, action:any) =>{
    let payload =  action.payload;
    return { ...state.userInfo, ...payload}
}

// Root根组件的reducer
export default createReducer({
    userInfo
}, {
   [GET_USER_INFO]:getUserInfo
});

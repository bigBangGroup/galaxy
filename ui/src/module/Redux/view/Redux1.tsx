import React from "react";
import { connect } from 'react-redux';        //使当前组件和该组件对应的actions连接起来
import { getUserInfo } from "../actions";
import { Button } from 'antd';
import classNames from 'classnames';

interface IUserInfo{
  userName:string
  mail:string,
  uid:null | number
}

//redux 的属性
type PageStateProps = {

}

//redux 的dispatch 方法
type PageDispatchProps = {
  getUserInfo:() => void
}

//组件props （自己的或者外面传入）
type PageOwnProps = {
  changePage?:(val:string) => any,
  userInfo:IUserInfo
}

// 自己页面的 state, state 一定是自给自足的，无需外面传入进来.
type PageState = {

}

type IProps = PageStateProps & PageDispatchProps & PageOwnProps;

interface Redux1{
  props:IProps,
  state:PageState
}

//省略式声明：接口和类名相同
class Redux1 extends React.Component {

  componentDidMount(): void {

  }

  getUserInfo = () =>{
    this.props.getUserInfo();
  };

  render() {

    const {userInfo} = this.props;

    return (
        <React.Fragment>
          <div className="redux1-container container">
            <div className='intro-container'>
              <h3 className='title'><strong>Redux1</strong></h3>
              <div className="content indent1">
                <p><strong>使用redux 的原因：</strong></p>
                <p>1、在react中有些问题处理不好：比如两个平级组件 A B 在A组件中需要使用B组件。</p>
                <p className='indent2'>那唯一的方法就是 给A,B增加一个公用父组件 N，提升A中的state到 N 上，然后N传属性给A,B</p>
                <p>2、react 中数据没有向上回溯的能力，数据只能向下分发：父组件可以通过props传值给子组件。假如需要子组件去影响父组件的状态</p>
                <p className='indent2'>只能通过回调的方式（即父组件定义好相应的方法，然后子组件去调用这个方法，在回调方法中去改变转态，再分发给子组件。）</p>
                <p>3、某个转态需要在任何地方（组件中）都可以被拿到，比如token, userInfo...</p>
              </div>
            </div>

            <div className="use">
              <div className="content indent1 mt30">
                <p><strong>redux的使用方式：</strong></p>
                <p>1、配置 reducer 并将各个小的reducer 合并为一个大的 rootReducer</p>
                <p>2、配置 store 将 大的 rootReducer和 middleware 组装</p>
                <p>3、某个转态需要在任何地方（组件中）都可以被拿到，比如token根节点（APP.tsx）中配置 store redux 即redux盛放数据的地方</p>
                <p>4、对应写action, actionTypes</p>
                <p>5、UI层操作触发action操作，会由此通过 redux 分发到 action 层，action再dispatcher到reducer处理业务逻辑，再返回给UI层</p>
              </div>
            </div>

            <div className="demo">
              <p><strong>Demo1:</strong></p>
              <p> 点击下面的按钮获取通过 redux 获取用户信息</p>
              <Button className='mt10 mb10' type="primary" onClick={this.getUserInfo}>getUserInfo</Button>
              <div className={classNames('user-info-warp hide', {'show':userInfo.uid})}>
                <p><span>userName:</span>{userInfo.userName}</p>
                <p><span>mail:</span>{userInfo.mail}</p>
                <p><span>uid:</span>{userInfo.uid}</p>
              </div>
            </div>

          </div>
        </React.Fragment>
    );
  }
}

const mapStateToProps = (state:any) => {
    return {
      userInfo: state.userInfo
    }
};

export default connect(mapStateToProps, {getUserInfo})(Redux1);


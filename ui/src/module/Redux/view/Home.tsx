
//该文件纯属模板文件，无任何功能作用

import React from "react";

interface xxxProps {

}

interface xxxStatus {

}

class xxx extends React.Component<xxxProps, xxxStatus> {

  render() {
    return (
        <React.Fragment>
          <div className="interface-container container">
            <div className='intro-container'>
              <h3 className='title'><strong>xxx</strong></h3>
              <div className="content indent1">
                <p><strong>要点：</strong></p>
                <p>1、</p>
                <p>2、</p>
                <p>3、</p>
              </div>
            </div>

            <div className="use"></div>

            <div className="demo">
              <p><strong>Demo:</strong></p>
            </div>

          </div>
        </React.Fragment>
    );
  }
}

export default xxx;

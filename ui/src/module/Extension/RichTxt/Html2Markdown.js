import React, {Component} from 'react';
import {Card} from "antd";
const html2md=require('html-to-md');

class Html2Markdown extends Component {

    state = {};

    render() {
        const {htmlContents} = this.props;
        return (
            <React.Fragment>
                <div className="html2markdown-wrap">
                    <Card title="同步转换Markdown" bordered={true} className='show-box'>
                        <div>{
                            html2md(htmlContents)
                        }</div>
                    </Card>
                </div>
            </React.Fragment>
        )
    }
}

export default Html2Markdown


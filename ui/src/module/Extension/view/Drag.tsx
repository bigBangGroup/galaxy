import React from "react";
import {DragDropContext, Droppable, Draggable, DragUpdate, ResponderProvided} from "react-beautiful-dnd";
import './../scss/Drag.scss';
import MyDrag from './MyDrag/index.jsx';

// fake data generator
const getItems = (count: number, content?:string) =>
    Array.from({length: count}, (v, k) => k).map(k => ({
        id: `select and drag me -${k}`,
        content: content ? `${content + '-' + k}` : `select and drag me ${k}`
    }));

// fake data generator
const getItemsWithOffset = (count:number, offset:number = 0) =>
    Array.from({ length: count }, (v, k) => k).map(k => ({
        id: `item-${k + offset}`,
        content: `drag me to next box -- ${k + offset}`
    }));


// a little function to help us with reordering the result
const reorder = (list: any, startIndex: number, endIndex: number) => {
    const result:IDragItem[] = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);
    return result;
};

const grid = 10;

const getItemStyle = (isDragging: boolean, draggableStyle: any) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: "none",
    padding: grid * 2,
    margin: `0 0 ${grid}px 0`,
    textAlign:'center',
    // change background colour if dragging
    background: isDragging ? "Moccasin" : "MintCream",

    // styles we need to apply on draggables
    ...draggableStyle
});

const getListStyle = (isDraggingOver: boolean) => ({
    background: isDraggingOver ? "Aqua" : "lightblue",
    padding: grid,
    width: 250
});

const getItemStyleHorizontal = (isDragging:boolean, draggableStyle:any) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: 'none',
    padding: grid * 2,
    margin: `0 ${grid}px 0 0`,

    // change background colour if dragging
    background: isDragging ? 'Moccasin' : 'MintCream',

    // styles we need to apply on draggables
    ...draggableStyle,
});

const getListStyleHorizontal = (isDraggingOver:boolean) => ({
    background: isDraggingOver ? 'Aqua' : 'lightblue',
    display: 'flex',
    padding: grid*2,
    overflow: 'auto',
});

/**
 * Moves an item from one list to another list.
 */
const move = (source:any, destination:any, droppableSource:any, droppableDestination:any) => {
    const sourceClone = Array.from(source);
    const destClone = Array.from(destination);
    const [removed] = sourceClone.splice(droppableSource.index, 1);

    destClone.splice(droppableDestination.index, 0, removed);

    const result:any = {};
    result[droppableSource.droppableId] = sourceClone;
    result[droppableDestination.droppableId] = destClone;

    return result;
};


interface IDragItem{
    id:string
    content:string
}

interface DragProps {

}

interface DragStatus {
    items: IDragItem[]
    itemsHorizontal: IDragItem[]
    itemsRaw: IDragItem[]
    itemsSelected: IDragItem[]
}

class Drag extends React.Component<DragProps, DragStatus> {

    constructor(props: any) {
        super(props);
        this.state = {
            items: getItems(5, 'drag me vertical'),
            itemsHorizontal: getItems(6, 'drag me horizontal'),
            itemsRaw: getItemsWithOffset(10),
            itemsSelected: getItemsWithOffset(5,10),
        };
        this.onDragEnd = this.onDragEnd.bind(this);
        this.onDragEndHorizontal = this.onDragEndHorizontal.bind(this);
        this.onDragEndWithTwoList = this.onDragEndWithTwoList.bind(this);
        this.onDragStart = this.onDragStart.bind(this);
        this.onDragUpdate = this.onDragUpdate.bind(this);
    }
    /**
     * A semi-generic way to handle multiple lists. Matches
     * the IDs of the droppable container to the names of the
     * source arrays stored in the state.
     */
    id2List:any = {
        droppable: 'itemsRaw',
        droppable2: 'itemsSelected'
    };

    getList = (id:any) => (this as any).state[this.id2List[id]];

    onDragEndWithTwoList = (result:any) => {
        const { source, destination } = result;

        // dropped outside the list
        if (!destination) {
            return;
        }

        if (source.droppableId === destination.droppableId) {
            const itemsRaw = reorder(
                this.getList(source.droppableId),
                source.index,
                destination.index
            );

            let state:any = { itemsRaw };

            if (source.droppableId === 'droppable2') {
                state = { itemsSelected: itemsRaw };
            }

            this.setState(state);
        } else {
            const result = move(
                this.getList(source.droppableId),
                this.getList(destination.droppableId),
                source,
                destination
            );

            this.setState({
                itemsRaw: result.droppable,
                itemsSelected: result.droppable2
            });
        }
    };

    //拖动开始触发
    onDragStart(initial:DragUpdate,provided:ResponderProvided){
        console.log('onDragStart', initial)
        console.log('onDragStart', provided)
    }

    //拖动过程中持续触发
    onDragUpdate(initial:DragUpdate,provided:ResponderProvided){
        console.log('onDragUpdate', initial)
        console.log('onDragUpdate', provided)
    }

    //拖动结束后触发
    onDragEnd(result: DragUpdate,provided:ResponderProvided) {
        console.log('onDragEnd', result);
        console.log('onDragEnd', provided);
        // dropped outside the list
        if (!result.destination) {
            return;
        }
        const items:IDragItem[] = reorder(
            this.state.items,
            result.source.index,
            result.destination.index
        );

        this.setState({
            items
        });
    }

    onDragEndHorizontal(result: DragUpdate,provided:ResponderProvided) {
        // dropped outside the list
        if (!result.destination) {
            return;
        }
        const itemsHorizontal:IDragItem[] = reorder(
            this.state.itemsHorizontal,
            result.source.index,
            result.destination.index
        );

        this.setState({
            itemsHorizontal
        });
    }

    render() {
        return (
            <React.Fragment>
                <div className="drag-container container">

                <div className="my-drag-container demo demo4">
                    <MyDrag />
                </div>

                <div className="demo-container demo">

                    <div className="demo1">
                        <DragDropContext onDragEnd={this.onDragEnd}>
                            <Droppable droppableId="droppable">
                                {(provided, snapshot) => (
                                    <div
                                        className='drag-drop-container'
                                        {...provided.droppableProps}
                                        ref={provided.innerRef}
                                        style={getListStyle(snapshot.isDraggingOver)}
                                    >
                                        {this.state.items.map((item: any, index: number) => (
                                            <Draggable key={item.id} draggableId={item.id} index={index}>
                                                {(provided, snapshot) => (
                                                    <div
                                                        ref={provided.innerRef}
                                                        {...provided.draggableProps}
                                                        {...provided.dragHandleProps}
                                                        style={getItemStyle(
                                                            snapshot.isDragging,
                                                            provided.draggableProps.style
                                                        )}
                                                    >
                                                        {item.content}
                                                    </div>
                                                )}
                                            </Draggable>
                                        ))}
                                        {provided.placeholder}
                                    </div>
                                )}
                            </Droppable>
                        </DragDropContext>
                    </div>

                    <div className=" demo2">
                        <DragDropContext onDragEnd={this.onDragEndHorizontal} onDragStart={this.onDragStart} onDragUpdate={this.onDragUpdate}>
                            <Droppable droppableId="droppable" direction="horizontal">
                                {(provided, snapshot) => (
                                    <div
                                        className='drag-drop-container horizontal'
                                        {...provided.droppableProps}
                                        ref={provided.innerRef}
                                        style={getListStyleHorizontal(snapshot.isDraggingOver)}
                                    >
                                        {this.state.itemsHorizontal.map((item: any, index: number) => (
                                            <Draggable key={item.id} draggableId={item.id} index={index}>
                                                {(provided, snapshot) => (
                                                    <div
                                                        ref={provided.innerRef}
                                                        {...provided.draggableProps}
                                                        {...provided.dragHandleProps}
                                                        style={getItemStyleHorizontal(
                                                            snapshot.isDragging,
                                                            provided.draggableProps.style
                                                        )}
                                                    >
                                                        {item.content}
                                                    </div>
                                                )}
                                            </Draggable>
                                        ))}
                                        {provided.placeholder}
                                    </div>
                                )}
                            </Droppable>
                        </DragDropContext>
                    </div>

                </div>

                 <div className="demo demo3">
                     <DragDropContext onDragEnd={this.onDragEndWithTwoList}>
                         <Droppable droppableId="droppable">
                             {(provided, snapshot) => (
                                 <div
                                     className="selected-items"
                                     ref={provided.innerRef}
                                     style={getListStyle(snapshot.isDraggingOver)}>
                                     {this.state.itemsRaw.map((item, index) => (
                                         <Draggable
                                             key={item.id}
                                             draggableId={item.id}
                                             index={index}>
                                             {(provided, snapshot) => (
                                                 <div
                                                     ref={provided.innerRef}
                                                     {...provided.draggableProps}
                                                     {...provided.dragHandleProps}
                                                     style={getItemStyle(
                                                         snapshot.isDragging,
                                                         provided.draggableProps.style
                                                     )}>
                                                     {item.content}
                                                 </div>
                                             )}
                                         </Draggable>
                                     ))}
                                     {provided.placeholder}
                                 </div>
                             )}
                         </Droppable>
                         <Droppable droppableId="droppable2">
                             {(provided, snapshot) => (
                                 <div
                                     ref={provided.innerRef}
                                     style={getListStyle(snapshot.isDraggingOver)}>
                                     {this.state.itemsSelected.map((item:IDragItem, index:number) => (
                                         <Draggable
                                             key={item.id}
                                             draggableId={item.id}
                                             index={index}>
                                             {(provided, snapshot) => (
                                                 <div
                                                     ref={provided.innerRef}
                                                     {...provided.draggableProps}
                                                     {...provided.dragHandleProps}
                                                     style={getItemStyle(
                                                         snapshot.isDragging,
                                                         provided.draggableProps.style
                                                     )}>
                                                     {item.content}
                                                 </div>
                                             )}
                                         </Draggable>
                                     ))}
                                     {provided.placeholder}
                                 </div>
                             )}
                         </Droppable>
                     </DragDropContext>

                 </div>

                </div>
            </React.Fragment>
        );
    }
}

export default Drag;

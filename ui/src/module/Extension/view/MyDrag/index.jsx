import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import './index.scss';

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
};

class MyDrag extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [
                {content:'item1', id:1},
                {content:'item2', id:2},
                {content:'item3', id:3},
                {content:'item4', id:4},
                {content:'item5', id:5},
                {content:'item6', id:6},
            ],
        };
        this.onDragEnd = this.onDragEnd.bind(this);
    }

    onDragEnd(result) {
        // dropped outside the list
        if (!result.destination) {
            return;
        }

        const items = reorder(
            this.state.items,
            result.source.index,
            result.destination.index
        );

        this.setState({
            items,
        });
    }

    // Normally you would want to split things out into separate components.
    // But in this example everything is just done in one place for simplicity
    render() {
        return (
          <div className="my-drag-container">
              <DragDropContext onDragEnd={this.onDragEnd}>
                  <Droppable droppableId="droppable" direction="horizontal">
                      {(provided, snapshot) => (
                          <div
                              className='drag-items-wrap'
                              ref={provided.innerRef}
                              {...provided.droppableProps}
                          >
                              {this.state.items.map((item, index) => (
                                  <Draggable key={item.id} draggableId={item.id} index={index}>
                                      {(provided, snapshot) => (
                                          <div
                                              className='drag-item'
                                              ref={provided.innerRef}
                                              {...provided.draggableProps}
                                              {...provided.dragHandleProps}
                                          >
                                              {item.content}
                                          </div>
                                      )}
                                  </Draggable>
                              ))}

                              {/*{provided.placeholder}*/}
                          </div>
                      )}
                  </Droppable>
              </DragDropContext>
          </div>
        );
    }
}


export default MyDrag;

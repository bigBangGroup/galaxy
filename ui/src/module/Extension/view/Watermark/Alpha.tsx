import React from "react";
import Block from "dxc-flex";
import { AlphaPicker } from "react-color";

export default class Alpha extends React.Component<any, any> {
  render() {
    const { color, onChange, labelWidth } = this.props;
    return (
      <Block style={{ float:'left', marginLeft:20 }}>
        <div style={{ width: labelWidth, marginRight: 2, float:'left', height:32, display:'flex', alignItems:'center' }}>透明度:</div>
        <div className='alpha-picker-wrap' style={{float:'left', marginRight:20, height:32, display:'flex', alignItems:'center'}}>
            <AlphaPicker onChange={onChange} color={color}/>
        </div>
      </Block>
    );
  }
}


import React from 'react';
import './iconfont';
import './../scss/emoji.scss';

const Emoji = ({type}:any) => {
    const useTag = `<use xlink:href=${'#icon-' + type} />`;
    return (
        <i className="emoji">
            <svg className="emoji" dangerouslySetInnerHTML={{__html: useTag }} />
            <style>{`
            .emoji {
                display: inline-block;
                overflow: hidden;
            }
            .emoji svg {
                width: 3em;
                height: 3em;
                vertical-align: -0.15em;
                fill: currentColor;
                overflow: hidden;
            }
        `}</style>
        </i>

    );
};

const emojiList = () => {
    let _elements = [];
    for (let i = 1; i < 30; i++) {
        _elements.push(
            <li key={i}>
                <Emoji type={'emoji-' + i} />
                <p>{'emoji-' + i}</p>
            </li>
        )
    }
    return (
        <div className='emoji-container container'>
           <ul>
               {_elements}
           </ul>
        </div>
    );
};


export default emojiList;

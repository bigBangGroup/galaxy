import React from "react";
import Upload, {filesToDataURL} from "dxc-upload";
import {Button, InputNumber} from 'antd';
import './../scss/Watermark.scss';
import Watermark from "watermark-image";
import IDCard from "./../../../images/IDCard.jpg";
import ColorPicker from "./Watermark/ColorPicker";
import Alpha from "./Watermark/Alpha";
import {ColorResult} from "react-color";


interface WatermarkModuleProps {

}

interface WatermarkModuleStatus {
    text: string
    hex: string
    fontSize: number
    watermarkHeight: number
    watermarkWidth: number
    rgb: any
}

class WatermarkModule extends React.Component<WatermarkModuleProps, WatermarkModuleStatus> {

    mainCanvas: HTMLCanvasElement | null = null;
    watermark: any = '';
    state = {
        text: "yangjian",
        hex: "#000000",
        rgb: {r: 0, g: 0, b: 0, a: 0.4},
        fontSize: 23,
        watermarkHeight: 180,
        watermarkWidth: 280
    };

    componentDidMount(): void {
        const {text, rgb, fontSize, watermarkWidth, watermarkHeight} = this.state;
        const fillStyle = `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, ${rgb.a})`;
        this.watermark = new Watermark((this.mainCanvas as HTMLCanvasElement));
        this.watermark.draw(IDCard, {text, fillStyle, fontSize, watermarkWidth, watermarkHeight});
    }

    setOptions = () => {
        const {text, rgb, fontSize, watermarkWidth, watermarkHeight} = this.state;
        const fillStyle = `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, ${rgb.a})`;
        this.watermark.setOptions({text, fillStyle, fontSize, watermarkWidth, watermarkHeight});
    };

    onChangeFile = (files: FileList) => {
        filesToDataURL(files).then((dataUrls: any) => {
            this.watermark.draw(dataUrls[0]);
        });
    };

    rotate = () => {
        this.watermark.rotate();
    };

    save = () => {
        this.watermark.save();
    };

    onChangeColor = ({rgb, hex}: ColorResult) => {
        rgb.a = this.state.rgb.a;
        this.setState({rgb, hex}, () => {
            this.setOptions();
        });
    };

    onChangeAlpha = (color: ColorResult) => {
        const {rgb, hex} = color;
        this.setState({rgb, hex}, () => {
            this.setOptions();
        });
    };

    componentWillUnmount(): void {
        console.log(111)
    }

    onChangeText = (key: string, value: number | undefined) => {
        (this as any).setState({[key]: value}, () => {
            this.setOptions();
        });
    };

    render() {
        const {hex, rgb, fontSize, watermarkWidth, watermarkHeight} = this.state;
        const labelWidth = 62;
        return (
            <React.Fragment>
                <div className="watermark-container container">
                    <div className="select-opts-wrap" style={{marginBottom: '20px'}}>
                        <ColorPicker labelWidth={labelWidth} color={hex} onChange={this.onChangeColor}/>
                        <Alpha labelWidth={labelWidth} color={rgb} onChange={this.onChangeAlpha}/>
                    </div>

                    <div className=''>
                        <div style={{}} className='mt20 mb25'>
                            <span className='mr5'>字体大小:</span>
                            <InputNumber
                                value={fontSize}
                                onChange={this.onChangeText.bind(this, "fontSize")}
                            />
                            <span className='mr5 ml20'>水印框宽:</span>
                            <InputNumber
                                value={watermarkWidth}
                                onChange={this.onChangeText.bind(this, "watermarkWidth")}
                            />
                            <span className='mr5 ml20'>水印框高:</span>
                            <InputNumber
                                value={watermarkHeight}
                                onChange={this.onChangeText.bind(this, "watermarkHeight")}
                            />
                        </div>
                    </div>

                    <div className='mb20' style={{overflow: "hidden"}}>
                        <Upload onChange={this.onChangeFile} className='fl'>
                            <Button type="primary">选择图片</Button>
                        </Upload>
                        <Button type="primary" className='fl ml10 mr10' onClick={this.rotate}>旋转</Button>
                        <Button type="primary" className='fl ml10 mr10' onClick={this.save}>下载至本地</Button>
                    </div>

                    <div className="canvas-wrap" style={{paddingTop: '30px'}}>
                        <div className='' style={{flex: 1, minWidth: 345, width: 900, height: "auto"}}>
                            <canvas id='canvas' style={{width: "100%"}} ref={mainCanvas => (this.mainCanvas = mainCanvas)}/>
                        </div>
                    </div>

                </div>
            </React.Fragment>
        );
    }
}

export default WatermarkModule;

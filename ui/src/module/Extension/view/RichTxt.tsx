import React from "react";
import { Card } from 'antd';
import BraftEditor from 'braft-editor';
import 'braft-editor/dist/index.css';
import './../scss/RichTxt.scss';
import Html2Markdown from './../RichTxt/Html2Markdown';

interface RichTxtProps {}

interface RichTxtStatus {
  editorState:any,
  outputHTML:string
}

class RichTxt extends React.Component<RichTxtProps, RichTxtStatus> {
  isLivinig = false
  state = {
    editorState: BraftEditor.createEditorState('<p>hey, 少侠好久不见<strong>请输入内容</strong></p>'), // 设置编辑器初始内容
    outputHTML: '<div></div>'
  }

  componentDidMount () {
    this.isLivinig = true
    // 3秒后更改编辑器内容
  }

  componentWillUnmount () {
    this.isLivinig = false
  }

  handleChange = (editorState:any) => {
    this.setState({
      editorState: editorState,
      outputHTML: editorState.toHTML()
    })
  }

  setEditorContentAsync = () => {
    this.isLivinig && this.setState({
      editorState: BraftEditor.createEditorState('<p>你好，<b>世界!</b><p>')
    })
  }

  render() {
    const { editorState, outputHTML } = this.state;
    return (
      <React.Fragment>
        <div className="rich-txt-container container">

          <div className="editor-wrapper">
            <BraftEditor
                value={editorState}
                onChange={this.handleChange}
            />
          </div>

          <Card title="同步转换HTML" bordered={true} className='show-box'>
            <div>{outputHTML}</div>
          </Card>

          <Card title="同步转换Markdown" bordered={true} className='show-box'>
            <div>{JSON.stringify(outputHTML)}</div>
          </Card>

          <Html2Markdown htmlContents = {outputHTML} />

        </div>
      </React.Fragment>
    );
  }
}

export default RichTxt;

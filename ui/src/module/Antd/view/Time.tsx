import React, { Component } from 'react';
import { TimePicker, Calendar } from 'antd';
import moment from 'moment';
import './../scss/Time.scss';

class TimeComponent extends Component<any,any> {
    constructor(props:any){
        super(props);
        this.state = {
            time1:'',
            time2:'',
            time3:'',
            time4:''
        }

    }

    onChange = (time:any, timeString:any) => {
        console.log(time, timeString);

        this.setState({
            time1:timeString
        })
    }

    onChange2 = (time:any, timeString:any) => {
        console.log(time, timeString);

        this.setState({
            time2:timeString
        })
    }

    onChange3 = (time:any, timeString:string) => {
        console.log(time, timeString);

        this.setState({
            time3:timeString
        })
    }

    onChange4 = (time:any, timeString:string) => {
        console.log(time, timeString);

        this.setState({
            time4:timeString
        })
    }

    onPanelChange = (value:any, mode:any) => {
        console.log(value, mode);
    }

    onPanelChange2 = (value:any, mode:any) => {
        console.log(value, mode);
    }

    render() {
        return (
           <div className='time-component-container container'>

               <div className="demo-wrap">
                   <p className="title">1：时间选择器</p>
                   <div className="content">
                       <p>
                           时间选择器是业务开发中基本都会用到的组件之一，下面就来诺列下常用的时间选择器的几种使用方式。
                           这里安装了 moment。请先 npm i moment安装该模块
                       </p>
                       <p>常见的配置如下</p>
                       <p>
                           defaultOpenValue表默认时间、disabled表不可选、minuteStep表分钟的步伐、
                           secondStep表秒步伐、onChange表时间选择之后的回调、format表是解析 时分，还是时分秒、
                           use12Hours表使用12小时制
                       </p>

                       <div className="demo-show">
                           <span className='des'>常规使用方式：</span>
                           <TimePicker onChange={this.onChange} defaultOpenValue={moment('00:00:00', 'HH:mm:ss')} />
                           <span className='ml10 mr10'>
                               选择的时间为:
                               <span className="val-show">
                                   {this.state.time1}
                               </span>
                           </span>
                       </div>

                       <div className="demo-show">
                           <span className='des'>禁止选择：</span>
                           <TimePicker defaultValue={moment('12:08:23', 'HH:mm:ss')} disabled />
                           <span className='ml10 mr10'>

                           </span>
                       </div>

                       <div className="demo-show">
                           <span className='des'>设置步长：</span>
                           <TimePicker minuteStep={15} secondStep={10} onChange={this.onChange2}/>
                           <span className='ml10 mr10'>
                               选择的时间为:
                               <span className="val-show">
                                   {this.state.time2}
                               </span>
                           </span>
                       </div>

                       <div className="demo-show">
                           <span className='des'>设置时分：</span>
                           <TimePicker defaultValue={moment('12:08', 'HH:mm')} format='HH:mm' onChange={this.onChange3}/>
                           <span className='ml10 mr10'>
                               选择的时间为:
                               <span className="val-show">
                                   {this.state.time3}
                               </span>
                           </span>
                       </div>

                       <div className="demo-show">
                           <span className='des'>十二小时：</span>
                           <TimePicker use12Hours format="h:mm:ss A" onChange={this.onChange4} />
                           <span className='ml10 mr10'>
                               选择的时间为:
                               <span className="val-show">
                                   {this.state.time4}
                               </span>
                           </span>
                       </div>

                   </div>
               </div>

               <div className="demo-wrap">
                   <p className="title">2：日历选择器</p>
                   <div className="content">
                       <p>
                           日历选择器是业务开发中基本都会用到的组件之一，下面就来诺列下常用的时间选择器的几种使用方式。
                           这里安装了 moment。请先 npm i moment安装该模块
                       </p>

                       <div className="demo-show">
                           <div style={{ width: 300, border: '1px solid #d9d9d9', borderRadius: 4 }}>
                              <div className="calendar">
                                  <Calendar fullscreen={false} onPanelChange={this.onPanelChange} />
                              </div>
                           </div>
                       </div>

                       <div className="demo-show">
                           <p className='mt20 mb10'>大型的日历选择组件，支持年/月切换：</p>
                           <div className="calendar">
                               <Calendar onPanelChange={this.onPanelChange2} />
                           </div>
                       </div>

                   </div>
               </div>
           </div>
        );
    }
}

export default TimeComponent;

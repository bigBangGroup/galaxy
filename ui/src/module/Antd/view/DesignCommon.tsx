import React, {Component} from 'react';
import {Input, Select, Radio, Checkbox} from 'antd';
import './../scss/DesignCommon.scss';

class DesignCommon extends Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            radioVal: '',
            val: '',
            val2: '',
            val3: '',
            btnVal: 'China',
            beforeVal: 'Http://',
            afterVal: '.com',
            searchVal: '',
            checkedBoxVal: '',
            selectVal: 'Lucy',
            multiSelectVal: ['a10', 'c12'],
            groupSelectVal: ''
        }

    }

    changeVal = (e: any) => {
        let val = e.target.value;
        this.setState({
            val: val
        })
    }
    changeVal2 = (e: any) => {
        let val = e.target.value;
        this.setState({
            val2: val
        })
    }

    changeVal3 = (e: any) => {
        let val = e.target.value;
        this.setState({
            val3: val
        })
    }

    changeBeforeVal = (val: any) => {
        this.setState({
            beforeVal: val
        })
    }

    changeAfterVal = (val: any) => {
        this.setState({
            afterVal: val
        })
    }

    onChangeRadio = (e: any) => {
        console.log('radio checked', e.target.value);
        this.setState({
            radioVal: e.target.value,
        });
    }

    changeBtnVal = (e: any) => {
        let val = e.target.value;
        this.setState({
            btnVal: val
        })
    }

    onChangeCheckBox = (checkedValues: any) => {
        console.log('checked = ', checkedValues);
        this.setState({
            checkedBoxVal: checkedValues
        })
    }

    handleChangeSelect = (value: any) => {
        console.log(`selected ${value}`);
        this.setState({
            selectVal: value
        })
    }

    handleChangeMultiSelect = (value: any) => {
        console.log(`selected ${value}`);
        this.setState({
            multiSelectVal: value
        })
    }

    handleGroupSelectChange = (value: any) => {
        console.log(`selected ${value}`);
        this.setState({
            groupSelectVal: value
        })
    }

    render() {

        const RadioGroup = Radio.Group;

        const Option = Select.Option;
        const selectBefore = (
            <Select defaultValue="Http://" style={{width: 100}} value={this.state.beforeVal}
                    onChange={this.changeBeforeVal}>
                <Option value="Http://">Http://</Option>
                <Option value="Https://">Https://</Option>
            </Select>
        );
        const selectAfter = (
            <Select defaultValue=".com" style={{width: 80}} value={this.state.afterVal} onChange={this.changeAfterVal}>
                <Option value=".com">.com</Option>
                <Option value=".jp">.jp</Option>
                <Option value=".cn">.cn</Option>
                <Option value=".org">.org</Option>
            </Select>
        );

        const Search = Input.Search;

        const {TextArea} = Input;

        const children = [];
        const {OptGroup} = Select;
        for (let i = 10; i < 36; i++) {
            children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
        }

        return (
            <div className='design-common-container container'>

                <div className="content">
                    <p className="title">1：Ant Design基础组件-Checkbox</p>
                    <p className='pl20'>一些常用的属性配置如下：</p>
                    <p className='pl20'>
                        value表被绑定的值、onChangeRadio表改变之后的回调、defaultChecked表是否被选中
                        disabled表是否被禁用
                    </p>

                    <div className="pl20 mt20">
                        <div className="demo-item">
                            <span className='mr20 des'>简单用法</span>
                            <div className="check-box-group">
                                <Checkbox.Group style={{width: '100%'}} onChange={this.onChangeCheckBox}>
                                    <Checkbox value="A">A</Checkbox>
                                    <Checkbox value="B">B</Checkbox>
                                    <Checkbox value="C">C</Checkbox>
                                    <Checkbox value="D">D</Checkbox>
                                    <Checkbox value="E">E</Checkbox>
                                </Checkbox.Group>
                            </div>
                            <span className='ml10 mr10'>选择的值：</span><span
                            className='val-show'>{this.state.checkedBoxVal}</span>
                        </div>
                    </div>

                    <div className="pl20 mt20">
                        <div className="demo-item">
                            <span className='mr20 des'>各种状态</span>
                            <div className="check-box-group">
                                <Checkbox value="A">A</Checkbox>
                                <Checkbox value="B" disabled>不可选</Checkbox>
                                <Checkbox value="C" defaultChecked={true}>默认勾选</Checkbox>
                                <Checkbox value="D" disabled defaultChecked>默认选中不可选</Checkbox>
                                <Checkbox value="E">E</Checkbox>
                            </div>
                        </div>
                    </div>

                </div>

                <div className="content">
                    <p className="title">2：Ant Design基础组件-Radio</p>
                    <p className='pl20'>一些常用的属性配置如下：</p>
                    <p className='pl20'>
                        value表被绑定的值、onChangeRadio表改变之后的回调、defaultChecked表是否被选中
                        disabled表是否被禁用
                    </p>

                    <div className="pl20 mt20">
                        <div className="demo-item">
                            <span className='mr20 des'>简单互斥用法</span>
                            <RadioGroup onChange={this.onChangeRadio} value={this.state.radioVal}>
                                <Radio value={'A'}>A</Radio>
                                <Radio value={'B'}>B</Radio>
                                <Radio value={'C'}>C</Radio>
                                <Radio value={'D'}>D</Radio>
                            </RadioGroup>
                            <span className='ml10 mr10'>选择的值：</span><span
                            className='val-show'>{this.state.radioVal}</span>
                        </div>
                    </div>

                    <div className="pl20 mt20">
                        <div className="demo-item">
                            <span className='mr20 des'>四中状态</span>
                            <Radio value={'A'}>默认没选中</Radio>
                            <Radio value={'B'} defaultChecked={true}>默认选中</Radio>
                            <Radio value={'C'} defaultChecked={true} disabled>选中不可点击</Radio>
                            <Radio value={'D'} disabled>没选中不可点击</Radio>
                        </div>
                    </div>

                    <div className="pl20 mt20">
                        <div className="demo-item">
                            <span className='mr20 des'>按钮单选</span>
                            <Radio.Group defaultValue="c" buttonStyle="solid" onChange={this.changeBtnVal}
                                         value={this.state.btnVal}>
                                <Radio.Button value="China">中国</Radio.Button>
                                <Radio.Button value="America" disabled>美国</Radio.Button>
                                <Radio.Button value="Russia">俄罗斯</Radio.Button>
                                <Radio.Button value="Australia">澳大利亚</Radio.Button>
                            </Radio.Group>
                            <span className='ml10 mr10'>选择的值：</span><span
                            className='val-show'>{this.state.btnVal}</span>
                        </div>
                    </div>

                </div>

                <div className="content">
                    <p className="title">3：Ant Design基础组件-Select</p>
                    <p className='pl20'>一些常用的属性配置如下：</p>
                    <p className='pl20'>
                        value表被绑定的值、onChange 表改变之后的回调、
                        disabled表是否被禁用
                    </p>

                    <div className="pl20 mt20">
                        <div className="demo-item">
                            <span className='mr20 des'>简单用法</span>
                            <Select defaultValue="lucy" style={{width: 120}} onChange={this.handleChangeSelect}
                                    value={this.state.selectVal}>
                                <Option value="jack">Jack</Option>
                                <Option value="lucy">Lucy</Option>
                                <Option value="disabled" disabled>我不可被选中</Option>
                                <Option value="Yiminghe">yiminghe</Option>
                            </Select>
                            <span className='ml10 mr10'>选择的值：</span><span
                            className='val-show'>{this.state.selectVal}</span>
                        </div>

                        <div className="demo-item">
                            <span className='mr20 des'>不可选中</span>
                            <Select defaultValue="lucy" style={{width: 120}} disabled>
                                <Option value="lucy">Lucy</Option>
                            </Select>
                        </div>

                        <div className="demo-item">
                            <span className='mr20 des'>分组下拉列表</span>

                            <Select
                                defaultValue="lucy"
                                style={{width: 200}}
                                onChange={this.handleGroupSelectChange}>

                                <OptGroup label="Manager">
                                    <Option value="jack">Jack</Option>
                                    <Option value="lucy">Lucy</Option>
                                </OptGroup>

                                <OptGroup label="Engineer">
                                    <Option value="Yiminghe">yiminghe</Option>
                                </OptGroup>

                            </Select>

                            <span className='ml10 mr10'>选择的值：</span><span
                            className='val-show'>{this.state.groupSelectVal}</span>

                        </div>

                        <div className="demo-item">
                            <span className='mr20 des'>下拉多选</span>
                            <div className="multi-select-wrap w400">
                                <Select
                                    className='w400'
                                    mode="multiple"
                                    style={{width: '100%'}}
                                    placeholder="Please select"
                                    defaultValue={['a10', 'c12']}
                                    onChange={this.handleChangeMultiSelect}>
                                    {children}
                                </Select>
                            </div>
                            <span className='ml10 mr10'>选择的值：</span><span
                            className='val-show'>{this.state.multiSelectVal}</span>
                        </div>

                    </div>

                </div>


                <div className="content">
                    <p className="title">4：Ant Design基础组件-输入框</p>
                    <p className='pl20'>一些常用的属性配置如下：</p>
                    <p className='mb10 pl20'>
                        value表被绑定的值、disabled表是否被禁止输入、onChange表输入时的回调、defaultValue表默认值、prefix表带有前缀图标的
                        suffix表带有后缀图标的 input
                    </p>
                    <div className='pl20'>
                        <div className='demo-item'>
                            <span className='mr20 des'>基本的输入框</span>
                            <Input placeholder="Basic usage" className='w400' value={this.state.val}
                                   onChange={this.changeVal}/>
                            <span className='ml10 mr10'>输入的值：</span><span className='val-show'>{this.state.val}</span>
                        </div>

                        <div className='demo-item'>
                            <span className='mr20 des'>禁止输入</span>
                            <Input placeholder="disable input" className='w400' disabled value={this.state.val}
                                   onChange={this.changeVal}/>
                        </div>

                        <div className='demo-item'>
                            <span className='mr20 des'>前后搭配</span>
                            <Input addonBefore={selectBefore} className='w400' addonAfter={selectAfter}
                                   defaultValue="mysite" value={this.state.val2} onChange={this.changeVal2}/>
                            <span className='ml10 mr10'>得到的值：</span><span
                            className='val-show'>{this.state.beforeVal}{this.state.val2}{this.state.afterVal}</span>
                        </div>

                        <div className='demo-item'>
                            <span className='mr20 des'>带有图标</span>
                            <Search
                                placeholder="input search text"
                                onSearch={value => this.setState({searchVal: value})}
                                style={{width: 400}}
                            />
                            <span className='ml10 mr10'>点击搜索或者回车：</span><span
                            className='val-show'>{this.state.searchVal}</span>
                        </div>

                        <div className='demo-item'>
                            <span className='mr20 des'>密码输入</span>
                            <Input.Password className='w400' placeholder="input password"/>
                            <span className='ml10 mr10'>点击右边图标，切换是否显示密码明文</span>
                        </div>

                        <div className='demo-item'>
                            <span className='mr20 des' style={{verticalAlign: 'top'}}>文本域</span>
                            <TextArea className='w400' rows={4} onChange={this.changeVal3} value={this.state.val3}/>
                            <span className='ml10 mr10' style={{verticalAlign: 'top'}}>得到的值：</span><span
                            style={{verticalAlign: 'top'}} className='val-show'>{this.state.val3}</span>
                        </div>


                    </div>
                </div>
            </div>
        );
    }
}

export default DesignCommon;

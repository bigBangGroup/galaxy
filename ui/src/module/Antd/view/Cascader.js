import React from "react";
import { Cascader } from 'antd';
import './../scss/Cascader.scss'

const options = [
    {
      value: '1-1',
      label: '类人型模型',
      children: [
        {
          value: '2-1',
          label: '成男',
          children: [
            {
              value: '3-1',
              label: '头发',
            },
          ],
        },
        {
            value: '2-2', 
            label: '成女',
        },
        {
            value: '2-3',
            label: '萝莉',
            children: [
              {
                value: '头发',
                label: '头发',
              },{
                value: '头/脸',
                label: '头/脸',
              },{
                value: '外观',
                label: '外观',
              },{
                value: 'NPC模型',
                label: 'NPC模型',
              }
            ],
          },
      ],
    },
    {
      value: '1-2',
      label: '非人类模型',
      children: [
        {
          value: 'nanjing',
          label: 'Nanjing',
          children: [
            {
              value: 'zhonghuamen',
              label: 'Zhong Hua Men',
            },
          ],
        },
      ],
    },
    {
      value: '1-3',
      label: '道具',
    },
    {
      value: '1-3',
      label: '道具',
    },
    {
      value: '1-3',
      label: '道具',
    },{
      value: '1-3',
      label: '道具',
    },
    {
      value: '1-3',
      label: '道具',
    },{
      value: '1-3',
      label: '道具',
    },
    {
      value: '1-3',
      label: '道具',
    },{
      value: '1-3',
      label: '道具',
    },{
      value: '1-3',
      label: '道具',
    }
  ];

const displayrender = (labels, selectedOptions) =>{
    labels.forEach((label, index)=>{
      const markPosi = label.indexOf('(');
      if(markPosi > -1){
        labels[index] = label.substring(0, markPosi)
      }
    })
    return labels.join('/')
}

const onChange = (value)=>{
    console.log(value);
}

const dealShowOptions = (options)=>{
  options.forEach(node=>{
    recursionNode(node)
  })
  return options
}

const recursionNode = (node)=>{
  if(node.children && node.children.length){
    node.label = `${node.label}(${node.children.length})`;
    node.children.map(childNode =>{
      recursionNode(childNode)
    })
  }
}

const ShowCascader = () =>{
    console.log(dealShowOptions(options))
    return (
        <div className='cascader-container'>
            <div className='upload-cascader-container'>
                <div className="item">
                  <div className='des'>
                    显示的标签和展示标签值不一样，滚动条变色
                  </div>
                  <Cascader 
                    popupClassName='cascader-selecte-wrap'
                    options={options} 
                    onChange={onChange} 
                    displayRender={displayrender} 
                    placeholder="角色模型(900)" 
                  />
                </div>
            </div>
        </div>
    )
}

export default ShowCascader
import React, {Component} from 'react';
import {Menu, Dropdown, Button, Icon, message} from 'antd';
import './../scss/AntdBasic.scss';

const menu = (
    <Menu>
      <Menu.Item key="0">
        <a href="http://www.baidu.com/" target="_blank">第一个item</a>
      </Menu.Item>
      <Menu.Item key="1">
        <a href="http://www.163.com/" target="_blank">第二个item</a>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item key="3">第三个item</Menu.Item>
    </Menu>
);

const menu2 = (
    <Menu>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href="http://www.alipay.com/">1st menu item</a>
      </Menu.Item>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href="http://www.taobao.com/">2nd menu item</a>
      </Menu.Item>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href="http://www.tmall.com/">3rd menu item</a>
      </Menu.Item>
    </Menu>
);

const menu3 = (
    <Menu>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href="http://www.baidu.com/">第一个item</a>
      </Menu.Item>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href="http://www.sina.com/">第二个item</a>
      </Menu.Item>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href="http://www.163.com/">第三个item</a>
      </Menu.Item>
    </Menu>
)

const SubMenu = Menu.SubMenu;
const menu4 = (
    <Menu>
      <Menu.Item>一级菜单第一个</Menu.Item>
      <Menu.Item>一级菜单第二个</Menu.Item>
      <SubMenu title="一级菜单第三个（嵌套菜单）">
        <Menu.Item>二级菜单3.1</Menu.Item>
        <Menu.Item>二级菜单3.2</Menu.Item>
      </SubMenu>
      <SubMenu title="不可点击的嵌套菜单" disabled>
        <Menu.Item>5d menu item</Menu.Item>
        <Menu.Item>6th menu item</Menu.Item>
      </SubMenu>
    </Menu>
);

const menu5 = (
    <Menu>
      <Menu.Item key="1">自定义菜单第一个</Menu.Item>
      <Menu.Item key="2">自定义菜单第二个</Menu.Item>
      <Menu.Item key="3">自定义菜单第三个</Menu.Item>
    </Menu>
);

function handleButtonClick(e:any) {
  message.info('Click on left button.');
  console.log('click left button', e);
}

function handleMenuClick(e:any) {
  message.info('Click on menu item.');
  console.log('click', e);
}

const menu6 = (
    <Menu onClick={handleMenuClick}>
      <Menu.Item key="1"><Icon type="user" />1st menu item</Menu.Item>
      <Menu.Item key="2"><Icon type="user" />2nd menu item</Menu.Item>
      <Menu.Item key="3"><Icon type="user" />3rd item</Menu.Item>
    </Menu>
);

class Basic extends Component<any,any> {
  constructor(props:any) {
    super(props);
    this.state = {
      count:1
    }
  }


  render() {
    return (
        <div className='basic-container container'>

          <div className='demo-wrap'>
            <p className='title'>1：Ant Design - button</p>
            <div className="demo-content pl20">
              <p>这里展示 Ant Design 中一些按钮的用法</p>
              <p>用起来很简单，在项目中安装好 Ant Design 之后，在你需要的地方按需引用即可</p>
              <p>每个 Button 中都有type 属性，传入不同的类型即可展示不同的Button样式,传入 loading 属性则会显示按钮处于加载状态</p>
              <Button type="primary" className='mr10'>Primary</Button>
              <Button className='mg10'>Default</Button>
              <Button type="dashed" className='mg10'>Dashed</Button>
              <Button type="danger" className='mg10'>Danger</Button>
              <Button type="primary" shape="circle" icon="search" className='mg10' />
              <Button type="primary" icon="search" className='mg10'>Search</Button>
              <Button shape="circle" icon="search" className='mg10' />
              <Button icon="search">Search</Button>
              <Button type="primary" loading className='mg10'>
                Loading
              </Button>

            </div>
          </div>

          <div className="demo-wrap">
            <p className='title'>2：Ant Design - Icon</p>
            <div className="demo-content pl20">
              <p>这里展示 Ant Design 中一些 Icon 的用法</p>
              <p>
                用法同样简单，在哪儿需要，就在哪儿引入，然后按照属性配置即可,这里给的 Icon 是矢量图，意味着可以给font-size使其放大而不失真
                同样也可以按需给某一个颜色，主要常用属性type表图标类型、theme表图标主题风格(theme 属性为 twoTone 来渲染双色图标)、spin表是否有旋转动画、class追加图标样式（大小，颜色...）
              </p>

              <div className='demo-show'>
                <Icon type="android" className='green' />
                <Icon type="android" className='green' theme="filled"/>
                <Icon type="apple" className='red' style={{color: 'silver'}}/>
                <Icon type="apple" className='red' theme="filled" style={{color: 'silver'}}/>
                <Icon type="windows" className='blue'/>
                <Icon type="ie" className='blue'/>
                <Icon type="alipay" className='blue'/>
                <Icon type="chrome" spin theme="filled"/>
                <Icon type="chrome" spin />
                <Icon type="github" />
                <Icon type="yuque" />
                <Icon type="yuque" theme="filled" style={{color:'#1890ff'}}/>
                <Icon type="skype" style={{color:'#1890ff'}}/>
                <Icon type="gitlab" />
                <Icon type="smile" theme="twoTone" />
                <Icon type="heart" theme="twoTone" twoToneColor="#eb2f96" />
                <Icon type="check-circle" theme="twoTone" twoToneColor="#52c41a" />
                <Icon type="setting" theme="twoTone" twoToneColor="#1890ff" />
                <Icon type="loading" />
              </div>
            </div>

          </div>

          <div className="demo-wrap">
            <p className='title'>3：Ant Design - Dropdown</p>
            <div className="demo-content pl20">
              <p>这里展示 Ant Design 中一些常见的Dropdown按钮的用法</p>
              <p>
                trigger属性可设置弹出方式、disabled表是否禁用、placement表弹出位置、overlayStyle表下拉根元素的样式
                visible表菜单是否显示、onVisibleChange表菜单显示状态改变时调用，参数为 visible
              </p>
              <div className='mt20'>
                <Dropdown overlay={menu} trigger={['click']}>
                  <a className="ant-dropdown-link mr20">
                    Click me <Icon type="down" />
                  </a>
                </Dropdown>

                <Dropdown overlay={menu} placement="topLeft" className='mr20'>
                  <Button>topLeft</Button>
                </Dropdown>

                <Dropdown overlay={menu3}>
                  <a className="ant-dropdown-link mr20">
                    Hover me <Icon type="down" />
                  </a>
                </Dropdown>

                <Dropdown overlay={menu4}>
                  <a className="ant-dropdown-link mr20">
                    嵌套下拉列表 <Icon type="down" />
                  </a>
                </Dropdown>

                <Dropdown overlay={menu5} trigger={['contextMenu']} className='mr20'>
                  <span style={{ userSelect: 'none' }}>请右键点击我</span>
                </Dropdown>

                <Dropdown.Button onClick={handleButtonClick} overlay={menu6}>
                  Dropdown
                </Dropdown.Button>

                <Dropdown overlay={menu6}>
                  <Button style={{ marginLeft: 20 }}>
                    Button <Icon type="down" />
                  </Button>
                </Dropdown>

              </div>
            </div>
          </div>

        </div>
    );
  }
}

export default Basic;

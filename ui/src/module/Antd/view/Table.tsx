import React from "react";
import { Table, Button, Popconfirm} from "antd";
import {exportListToXlsx} from "../../../utils/excel";
import './../scss/Table.scss';

const excelTitle0:string[] = ['Name', 'Age', 'Address', 'Tags','','', 'Action'];
const columns0 = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    render: (text:any) => <a>{text}</a>,
  },
  {
    title: 'Age',
    dataIndex: 'age',
    key: 'age',
  },
  {
    title: 'Address',
    dataIndex: 'address',
    key: 'address',
  },
  {
    title: 'Tags',
    key: 'tags',
    dataIndex: 'tags',
  },
  {
    title: 'Action',
    key: 'action',
    render: (text:any, record:any) => (
        <span>
        <a>Delete</a>
      </span>
    ),
  },
];
const data0 = [
  {
    key: '1',
    name: 'John Brown',
    age: 32,
    address: 'New York No. 1 Lake Park',
    tags: 'nice developer',
  },
  {
    key: '2',
    name: 'Jim Green',
    age: 42,
    address: 'London No. 1 Lake Park',
    tags: 'loser',
  },
  {
    key: '3',
    name: 'Joe Black',
    age: 32,
    address: 'Sidney No. 1 Lake Park',
    tags: 'cool teacher',
  },
];

const excelTitle1 = ['Name', 'Other', '', '', '', 'Company', '', 'Gender'];
const excelTitle2 = ['', 'Age', 'Address','', '', 'Company Address',  'Company Name', ''];
const excelTitle3 = ['', '', 'Street', 'Block', '', '', '',''];
const excelTitle4 = ['', '', '', 'Building', 'Door No.', '', '', ''];


const columns:any = [
  {
    title: 'Name',
    dataIndex: 'name',
    width: 100,
    // fixed: 'left',
  },
  {
    title: 'Other',
    children: [
      {
        title: 'Age',
        dataIndex: 'age',
        width: 150,
      },
      {
        title: 'Address',
        children: [
          {
            title: 'Street',
            dataIndex: 'street',
            width: 150,
          },
          {
            title: 'Block',
            children: [
              {
                title: 'Building',
                dataIndex: 'building',
                width: 100,
              },
              {
                title: 'Door No.',
                dataIndex: 'number',
                width: 100,
              },
            ],
          },
        ],
      },
    ],
  },
  {
    title: 'Company',
    children: [
      {
        title: 'Company Address',
        dataIndex: 'companyAddress',
        width: 200,
      },
      {
        title: 'Company Name',
        dataIndex: 'companyName',
      },
    ],
  },
  {
    title: 'Gender',
    dataIndex: 'gender',
    width: 80,
    // fixed: 'right',
  },
];
const data:any = [];
for (let i = 0; i < 100; i++) {
  data.push({
    key: i,
    name: 'John Brown',
    age: i + 1,
    street: 'Lake Park',
    building: 'C',
    number: 2035,
    companyAddress: 'Lake Street 42',
    companyName: 'SoftLake Co',
    gender: 'M',
  });
}

const columnsNest = [
  {
    title: "Company",
    children: [
      {
        title: "Company Address",
        dataIndex: "companyAddress",
        key: "companyAddress",
        width: 200
      },
      {
        title: "Company Name",
        dataIndex: "companyName",
        key: "companyName",
        width: 200
      },

    ]
  },
  {
    title: "Address",
    children: [
      {
        title: "Street",
        dataIndex: "street",
        key: "street",
        width: 200
      },
      {
        title: "Block",
        dataIndex: "block",
        key: "block",
        width: 200
      },
      {
        title: "Building",
        dataIndex: "building",
        key: "building",
        width: 200
      },
      {
        title: "Door No.",
        dataIndex: "door",
        key: "door",
        width: 200
      }
    ]
  }
];

const dataNest = [
  {
    companyAddress:"companyAddress1", companyName:"companyName1", companyNo:"companyNo1",
    street:"street1", block:"block1", building:"building1", door:"door1"
  },
  {
    companyAddress:"companyAddress2", companyName:"companyName2", companyNo:"companyNo2",
    street:"street2", block:"block2", building:"building2", door:"door2"
  },
  {
    companyAddress:"companyAddress3", companyName:"companyName3", companyNo:"companyNo3",
    street:"street2", block:"block3", building:"building3", door:"door3"
  }
]

class MyTable extends React.Component<any, any> {

  state = {
    columnsNest:columnsNest,
    dataNest:dataNest,
    data3:data
  }

  exportExcel0 = () => {
    console.log(999);
    const dataSource = data0;
    const excelArr:any = [ excelTitle0 ];
    // dataSource.map((userInfo:any) => {
    //   excelArr.push([
    //     userInfo.name,
    //     userInfo.age,
    //     userInfo.address,
    //     userInfo.tags,
    //   ])
    // });
    const tBodyData = [
      "yang","26","netease","it","","","dev"
    ];
    excelArr.push([]);
    excelArr.push(tBodyData);
    const merger = [
      {  //合并第四行（C4）第三列到第五列
        s: {//s为开始
          c: 0,//开始列
          r: 0//开始取值范围
        },
        e: {//e结束
          c: 0,//结束列
          r: 1//结束范围
        }
      },
      {  //合并A1A2单元格
        s: {//s为开始
          c: 3,//开始列
          r: 0//开始取值范围
        },
        e: {//e结束
          c: 5,//结束列
          r: 0//结束范围
        }
      },
    ]
    exportListToXlsx(excelArr,merger, 'testFile.xlsx');
  };

  exportExcel = () => {
    console.log(999);
    const dataSource = data0;
    const excelArr:any = [ excelTitle1, excelTitle2, excelTitle3, excelTitle4 ];
    const merger = [
      {  //合并第一列 0-3行
        s: {//s为开始
          c: 0,//开始列
          r: 0//开始取值范围
        },
        e: {//e结束
          c: 0,//结束列
          r: 3//结束范围
        }
      },
      {  //合并第一行 1-4列
        s: {//s为开始
          c: 1,//开始列
          r: 0//开始取值范围
        },
        e: {//e结束
          c: 4,//结束列
          r: 0//结束范围
        }
      },
      {  //合并第一行 5-6列
        s: {//s为开始
          c: 5,//开始列
          r: 0//开始取值范围
        },
        e: {//e结束
          c: 6,//结束列
          r: 0//结束范围
        }
      },
      {  //合并第一列 1-3 行
        s: {//s为开始
          c: 1,//开始列
          r: 1//开始取值范围
        },
        e: {//e结束
          c: 1,//结束列
          r: 3//结束范围
        }
      },
      {  //合并第 一行 2-3列
        s: {//s为开始
          c: 2,//开始列
          r: 1//开始取值范围
        },
        e: {//e结束
          c: 3,//结束列
          r: 1//结束范围
        }
      },
      {  //合并第 五列 1-3 行
        s: {//s为开始
          c: 5,//开始列
          r: 1//开始取值范围
        },
        e: {//e结束
          c: 5,//结束列
          r: 3//结束范围
        }
      },
      {  //合并第 六列 1-3 行
        s: {//s为开始
          c: 6,//开始列
          r: 1//开始取值范围
        },
        e: {//e结束
          c: 6,//结束列
          r: 3//结束范围
        }
      },
      {  //合并第 2列 2-3 行
        s: {//s为开始
          c: 2,//开始列
          r: 2//开始取值范围
        },
        e: {//e结束
          c: 2,//结束列
          r: 3//结束范围
        }
      },
      {  //合并第 2行 3-4 列
        s: {//s为开始
          c: 3,//开始列
          r: 2//开始取值范围
        },
        e: {//e结束
          c: 4,//结束列
          r: 2//结束范围
        }
      },
      {  //合并第 7列 0-3行
        s: {//s为开始
          c: 7,//开始列
          r: 0//开始取值范围
        },
        e: {//e结束
          c: 7,//结束列
          r: 3//结束范围
        }
      },
    ];
    const tBodyData1 = [
        'John Brown', '1','Lake Park', 'C', '2035', 'Lake Street 42', 'SoftLake Co', 'M'
    ];
    const tBodyData2 = [
      'John Brown', '2','Lake Park', 'C', '2036', 'Lake Street 43', 'SoftLake Co', 'M'
    ];
    excelArr.push(tBodyData1);
    excelArr.push(tBodyData2);
    exportListToXlsx(excelArr, merger);
  };

  changeSubMenu = () =>{
    const companyNo = {
      title: "Company NO.",
      dataIndex: "companyNo",
      key: "companyNo",
      width: 200
    };

    columnsNest[0].children.push(companyNo);

    this.setState({
      columnsNest
    })
  }

  render() {
    const { columnsNest, dataNest,  data3 } = this.state;
    return (
      <React.Fragment>
        <div className="table-container">

          <div className="opt-wrap mb15 mt10 mr10">
            <Button className='fl mg15' type="primary" onClick={this.changeSubMenu}>add sub menu</Button>
          </div>
          <div className="table-wrap tb-wrap1">
            <Table
                rowKey='companyAddress'
                columns={columnsNest}
                dataSource={dataNest}
                bordered
                // size="middle"
                scroll={{ x: 'calc(700px + 50%)', y: 240 }}
            />
          </div>

          <div className='opt-wrap mb15 mt10 mr10'>
            <Button className='fr mg15' type="primary" onClick={this.exportExcel0}>export excel</Button>
          </div>

          <div className="table-wrap">
            <Table columns={columns0}
                   dataSource={data0}
                   bordered
                   size="middle" />
          </div>

          <div className='opt-wrap mb15 mt10 mr10'>
            <Button className='fr mg15' type="primary" onClick={this.exportExcel}>export excel</Button>
          </div>
          <div className="table-wrap">
            <Table
                columns={columns}
                dataSource={data3}
                bordered
                size="middle"
                scroll={{ x: 'calc(700px + 50%)', y: 240 }}
            />
          </div>

        </div>
      </React.Fragment>
    );
  }
}

export default MyTable;

import React from "react";
import { Select,  Button, Input, Radio, DatePicker, message, Form, Row, Col, Dropdown, Icon, Cascader, Checkbox, Upload } from 'antd';
import moment from 'moment';
import cityData from './cityData';
import './../scss/Form.scss';

const { Option } = Select;
const { MonthPicker, RangePicker } = DatePicker;

const dateFormat = 'YYYY/MM/DD';
class FormModule extends React.Component<any, any> {
  state = {
    collapsed: false,
    initData:{
      inputVal:'initialValue',
      province:['浙江省', '杭州市', '滨江区'],
      addrDetail:'initalAddrDetail',
      gender:"male",
      radioGroup2:'b',
      rangePicker:[moment('2018/01/01', dateFormat), moment('2019/10/12', dateFormat)],
    }
  };

  normFile = (e:any) => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  }

  validatorDIY = (rule:any, value:any, callback:any) => {
    const { form } = this.props;
    console.log([rule, value, form.getFieldValue('inputVal')])
    //form.getFieldValue('inputVal') 通过 getFieldValue 获取表单的值
    if (value && value !== form.getFieldValue('inputVal')) {
      // callback 有参数传入用就会报错.无参数传入就不会报错
      callback('自定义校验报错!');
    } else {
      callback();
    }
  };

  getTime = () =>{
    const { getFieldValue } = this.props.form;
    console.log(getFieldValue('range-picker'));
  }

  setTime = () =>{
    const { setFieldsValue } = this.props.form;
    setFieldsValue({
      'range-picker': [moment('2015/05/01', dateFormat), moment('2020/2/16', dateFormat)]
    })
  }

  save = (e:any) => {
    e.preventDefault();
    const { validateFieldsAndScroll, setFields } = this.props.form;
    validateFieldsAndScroll((err:any, values:any) => {
      console.log(values)
      if (!err) {
        // do something send data to server...
        // setFields 设置某一组输入控件的值与错误状态
        setFields({
          addrDetail: {
            errors: [new Error('业务判断可以自己抛出的错误')], 
            value: values[`addrDetail`]     // 设置对应表单项的值...
          }
        });
      }
    });
  };

  render() {
    const { initData } = this.state;
    const { getFieldDecorator, setFieldsValue, getFieldValue } = this.props.form;

    return (
      <React.Fragment>
        <div className="form-container container">
          <Form className='form-items-wrap demo-wrap'>

            <Form.Item
                className='input-form-item form-item'
                label={<span>输入框</span>}
                labelAlign='left'
            >
              {
                getFieldDecorator(`inputVal`, {
                  initialValue: initData['inputVal'],
                  rules: [
                    {
                      max: 12,
                      message: '长度不能超过12个字符',
                    },
                    {
                      min: 5,
                      message: '长度不能小于5个字符',
                    },
                    {
                      required: true,
                      message: '必填字段',
                    },
                    {
                      validator: this.validatorDIY,
                    },
                  ],
                })
                (
                    <Input placeholder="请输入广告名称" style={{width: 500}}/>
                )
              }
            </Form.Item>

            <Form.Item
                label="省市区选择"
                className='form-item'
                labelAlign='left'
            >
              {
                getFieldDecorator('province', {
                  initialValue: initData['province'],
                  rules: [
                    { type: 'array', required: true, message: '省市区必选!' },
                  ],
                })
                (
                  <Cascader options={cityData} placeholder="请选择所在省市区"/>
                )
              }
            </Form.Item>

            <Form.Item
                className='input-form-item form-item'
                label={<span>详细地址</span>}
                labelAlign='left'
            >
              {
                getFieldDecorator(`addrDetail`, {
                  initialValue: initData['addrDetail'],
                  rules: [
                    {
                      min: 5,
                      message: '长度不能小于5个字符',
                    },
                    {
                      required: true,
                      message: '必填字段',
                    },
                  ],
                })
                (
                    <Input placeholder="请输入详细地址" style={{width: 500}}/>
                )
              }
            </Form.Item>

            <Form.Item 
              className='form-item'
              label="性别"
              labelAlign='left'
            >
              {
                getFieldDecorator('gender', {
                    initialValue: initData['gender'],
                    rules: [
                      { required: true, message: '请选择性别!' }
                    ],
                  })
                  (
                    <Select
                      placeholder="选择性别"
                    >
                      <Option value="male">男</Option>
                      <Option value="female">女</Option>
                    </Select>,
                )
              }
            </Form.Item>
            
            <Form.Item 
              label="时间选择器(月份)"
              className='form-item'
              labelAlign='left'
            >
              {
                getFieldDecorator('month-picker', {
                    rules: [
                      { type: 'object', required: true, message: 'Please select time!' }
                    ],
                  }
                )
                (
                  <MonthPicker />
                )
              }
            </Form.Item>

            <Form.Item 
              label="RangePicker"
              className='form-item'
              labelAlign='left'
            >
              {
                getFieldDecorator('range-picker', {
                    initialValue: initData['rangePicker'],
                    rules: [
                      { type: 'array', required: true, message: 'Please select time!' }
                    ],
                  })
                  (
                    <RangePicker />
                  )
              }
            </Form.Item>

            <Form.Item 
              label="Radio.Group"
              className='form-item'
              labelAlign='left'
            >
              {
                getFieldDecorator('radio-group',{
                  rules: [
                    { required: true, message: 'Please select radio!' }
                  ],
                })
                (
                <Radio.Group>
                  <Radio value="a">item 1</Radio>
                  <Radio value="b">item 2</Radio>
                  <Radio value="c">item 3</Radio>
                </Radio.Group>,
              )}
            </Form.Item>

            <Form.Item 
              label="Radio.Group2"
              className='form-item'
              labelAlign='left'
            >
              {
                getFieldDecorator('radio-group2',{
                  initialValue: initData['radioGroup2'],
                })
                (
                  <Radio.Group>
                    <Radio value="a">item 1</Radio>
                    <Radio value="b">item 2</Radio>
                    <Radio value="c">item 3</Radio>
                  </Radio.Group>
              )}
            </Form.Item>

            <Form.Item 
              label="Checkbox.Group"
              className='form-item'
              labelAlign='left'
            >
              {
                getFieldDecorator('checkbox-group', {
                initialValue: ['A', 'C'],
                rules: [
                    { required: true, message: 'Please select checkbox!' }
                  ],
                })
                (
                  <Checkbox.Group style={{ width: '100%' }}>
                    <Checkbox value="A">A</Checkbox>
                    <Checkbox disabled value="B">B</Checkbox>
                    <Checkbox value="C">C</Checkbox>
                    <Checkbox value="D">D</Checkbox>
                    <Checkbox value="E">E</Checkbox>
                  </Checkbox.Group>
              )}
            </Form.Item>
            
            <Form.Item 
              label="Upload" 
              extra="longgggggggggggggggggggggggggggggggggg"
              className='form-item'
              labelAlign='left'
            >
              {
                getFieldDecorator('upload', {
                  rules: [
                    { required: true, message: 'Please upload file!' }
                  ],
                  valuePropName: 'fileList',
                  getValueFromEvent: this.normFile,
                })
                (
                  <Upload name="logo" action="/upload.do" listType="picture">
                    <Button>
                      <Icon type="upload" /> Click to upload
                    </Button>
                  </Upload>,
              )}
            </Form.Item>

            <Form.Item>
              <div className='btns-wrap'>
                <Button className='btn' type="primary" onClick={this.save}>
                  Submit
                </Button>
                <Button className='btn' type="primary" onClick={this.getTime}>
                  getRangeTime
                </Button>
                <Button className='btn' type="primary" onClick={this.setTime}>
                  setRangeTime
                </Button>
              </div>
            </Form.Item>

          </Form>
        </div>
      </React.Fragment>
    );
  }
}
//
const FormModuleWrap = Form.create<any>({
})(FormModule);
export default FormModuleWrap;


/*
*   redux的使用方式:
*   1:配置 reducer 并将各个小的reducer 合并为一个大的 rootReducer
*   2:配置 store 将 大的 rootReducer和 middleware 组装
*   3:根节点（APP.tsx）中配置 store redux 即redux盛放数据的地方
*   4:对应写action, actionTypes
*   5:UI层操作触发action操作，会由此通过 redux 分发到 action 层，action再dispatcher到reducer处理业务逻辑，再返回给UI层
*
* */


export { default as store } from "./store";

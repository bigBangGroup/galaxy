
/*
*   combineReducers: redux 提供的一种方法，作用就是把不同的 reducer 合成在一起成为大的
*   下面是将 postReducer 这个reducer 中返回的最新转态（新对象）用 posts 接住， 然后返回给组件（返回给组件的值也是挂在 posts 属性下面的）
*/

import { combineReducers } from 'redux';
import { reducer as userInfo } from './../../module/Redux'
export default combineReducers({
    userInfo:userInfo
})

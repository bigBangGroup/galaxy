import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers/index';


const initialState = {};

/*
*   middleware:   一个中间件，就是一个操作，所使用的某个中间件就会有对应的一个功能，就可以使用这个功能
*   redux-thunk:  其实处理的就是异步。  作用就是异步执行所分发的 action。
*                   比如有多个action 会放在一个链里面，然后一个一个去执行，执行完一个会自动执行下一个，到时候 dispatch就可以实现这种异步操作了
*   reducers:   Store 收到 Action 以后，必须给出一个新的 State，这样 View 才会发生变化。这种 State 的计算过程就叫做 Reducer。
*                Reducer 是一个函数，它接受 Action 和当前 State 作为参数，返回一个新的 State。
*
*/
const middleware = [thunk];

/*
*   三个参数 reducer, state, enhancer
*   rootReducer: 一个数组，数组中会包含很多我们需要用到的 reduce, 比如项目中可能有十个 reducer, 这是个reducer 会合成一个大的 rootReducer
*   applyMiddleware: redux 提供的一个原生方法，将所有的中间件组成一个数组，然后依次执行他
*
*/
const store = createStore(
  rootReducer,
  initialState,
  compose(
    applyMiddleware(...middleware),
      (<any>window).__REDUX_DEVTOOLS_EXTENSION__ && (<any>window).__REDUX_DEVTOOLS_EXTENSION__()
  )
)

//const store = createStore( combineReducers({ root, ...reducers }), {}, applyMiddleware(...reduxMiddlewares) );

export default store;

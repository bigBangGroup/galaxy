import { views as Login } from "../module/Login";
import { views as Home } from "../module/Home";
import { views as Dashboard } from "../module/Dashboard";
import { views as RctBasic } from "../module/RctBasic";
import { views as Rct } from "../module/Rct";
import { views as Redux } from "../module/Redux";
import { views as Routes } from "../module/Routes";
import { views as Ts } from "../module/Ts";
import { views as Antd } from "../module/Antd";
import { views as Chart } from "../module/Chart";
import { views as Animation } from "../module/Animation";
import { views as Extension } from "../module/Extension";
import { views as Egg } from "../module/Egg";

// 路由配置数组，支持一层嵌套
const routes = [
  {
    path: "/",
    component: Login.Home,
    exact: true
  },
  {
    path: "/login",
    component: Login.Home,
    exact: true
  },
  {
    path: "/admin",
    component: Home.Home,
    exact: false,
    routes: [
      {
        path: "/admin/dashboard",
        component: Dashboard.Home,
        icon: "dashboard",
        name: "dashboard"
      },
      {
        path: "/admin/reactBasic",
        component: RctBasic.Home,
        icon: "read",
        name: "React 基础",
        routes:[
          {
            path: "/admin/reactBasic/basic",
            component: RctBasic.Basic,
            name: "Basic"
          },
          {
            path: "/admin/reactBasic/form",
            component: RctBasic.Form,
            name: "Form"
          },
          {
            path: "/admin/reactBasic/constraint",
            component: RctBasic.Constraint,
            name: "Constraint"
          },
          {
            path: "/admin/reactBasic/communication",
            component: RctBasic.Communication,
            name: "Communication"
          },
          {
            path: "/admin/reactBasic/defaultProps",
            component: RctBasic.DefaultProps,
            name: "DefaultProps"
          },
          {
            path: "/admin/reactBasic/lifeCycle",
            component: RctBasic.LifeCycle,
            name: "LifeCycle"
          }
        ]
      },
      {
        path: "/admin/react",
        component: Rct.Home,
        icon: "code",
        name: "React 进阶",
        routes: [
          {
            path: "/admin/react/HOC",
            component: Rct.HOC,
            name: "HOC高阶组件"
          },
          {
            path: "/admin/react/fnAsChild",
            component: Rct.FnAsChild,
            name: "函数作为子组件"
          },
          {
            path: "/admin/react/hooks",
            component: Rct.ReactHooks,
            name: "React Hooks"
          },
          {
            path: "/admin/react/contextAPI",
            component: Rct.ContextAPI,
            name: "ContextAPI"
          },
          {
            path: "/admin/react/ref",
            component: Rct.Ref,
            name: "Ref"
          },
          {
            path: "/admin/react/asynchronous-rendering",
            component: Rct.ReactRendering,
            name: "React rendering"
          }
        ]
      },
      {
        path: "/admin/redux",
        component: Redux.Home,
        icon: "dropbox",
        name: "Redux",
        routes:[
          {
            path: "/admin/redux/demo1",
            component: Redux.Redux1,
            name: "Redux demo1"
          },
        ]
      },
      {
        path: "/admin/routes",
        component: Routes.Home,
        icon: "deployment-unit",
        name: "Routes",
        routes:[
          {
            path: "/admin/Routes/demo",
            component: Routes.Demo,
            name: "Routes demo",
            exact:true
          },
          {
            path: "/admin/Routes/demo/dynamic/:id",
            component: Routes.DynamicRoute,
            name: "Dynamic Route",
            exact:true
          },
          {
            path: "/admin/Routes/demo/get",
            component: Routes.GetRoute,
            name: "Get value Route",
            exact:true
          },
        ]
      },
      {
        path: "/admin/ts",
        component: Ts.Home,
        icon: "crown",
        name: "Ts",
        routes: [
          {
            path: "/admin/ts/interface",
            component: Ts.Interface,
            name: "Interface"
          },
          {
            path: "/admin/ts/record",
            component: Ts.Record,
            name: "Record"
          }
        ]
      },
      {
        path: "/admin/antd",
        component: Antd.Home,
        icon: "instagram",
        name: "Antd",
        routes: [
          {
            path: "/admin/antd/basic",
            component: Antd.Basic,
            name: "Basic"
          },
          {
            path: "/admin/antd/DesignCommon",
            component: Antd.DesignCommon,
            name: "DesignCommon"
          },
          {
            path: "/admin/antd/time",
            component: Antd.Time,
            name: "Time"
          },
          {
            path: "/admin/antd/table",
            component: Antd.Table,
            name: "Table"
          },
          {
            path: "/admin/antd/form",
            component: Antd.Form,
            name: "Form"
          },
          {
            path: "/admin/antd/cascader",
            component: Antd.Cascader,
            name: "Cascader"
          }
        ]
      },
      {
        path: "/admin/animation",
        component: Animation.Home,
        icon: "rocket",
        name: "Animation",
        routes: [
          {
            path: "/admin/Animation/css-animation",
            component: Animation.CssAnimation,
            name: "CssAnimation"
          },
          {
            path: "/admin/Animation/animation-table",
            component: Animation.AnimationTable,
            name: "AnimationTable"
          },
          {
            path: "/admin/Animation/animation-frame",
            component: Animation.AnimationFrame,
            name: "AnimationFrame"
          }
        ]
      },
      {
        path: "/admin/chart",
        component: Chart.Home,
        icon: "pie-chart",
        name: "Chart",
        routes: [
          {
            path: "/admin/chart/bullet",
            component: Chart.Bullet,
            name: "Bullet"
          },
          {
            path: "/admin/chart/bar",
            component: Chart.BarGraph,
            name: "BarGraph"
          },
          {
            path: "/admin/chart/pie",
            component: Chart.PieChart,
            name: "PieChart"
          },
          {
            path: "/admin/chart/line",
            component: Chart.LineChart,
            name: "LineChart"
          },
          {
            path: "/admin/chart/map",
            component: Chart.Map,
            name: "Map"
          },
          {
            path: "/admin/chart/amap",
            component: Chart.Amap,
            name: "Amap"
          }
        ]
      },
      {
        path: "/admin/extension",
        component: Extension.Home,
        icon: "bars",
        name: "Extension UI",
        routes: [
          {
            path: "/admin/extension/drag",
            component: Extension.Drag,
            name: "Drag"
          },
          {
            path: "/admin/extension/richtxt",
            component: Extension.RichTxt,
            name: "RichTxt"
          },
          {
            path: "/admin/extension/watermark",
            component: Extension.Watermark,
            name: "Watermark"
          },
          {
            path: "/admin/extension/emoji",
            component: Extension.Emoji,
            name: "Emoji"
          }
        ]
      },
      {
        path: "/admin/egg",
        component: Egg.Home,
        icon: "robot",
        name: "Egg",
        routes:[
          {
            path: "/admin/extension/request",
            component: Egg.Request,
            name: "接口请求"
          },
          {
            path: "/admin/extension/schedule",
            component: Egg.Schedule,
            name: "定时任务"
          },
          {
            path: "/admin/extension/middleware",
            component: Egg.Middleware,
            name: "中间件"
          },
          {
            path: "/admin/extension/cookie",
            component: Egg.Cookie,
            name: "Cookie"
          },
          {
            path: "/admin/extension/session",
            component: Egg.Session,
            name: "Session"
          }
        ]
      }
    ]
  }
];

export default routes;

import * as React from "react";
import { Route } from "react-router-dom";

interface RouteProps {
  path: string; // 路由地址
  component: any; // 路由匹配组件
  name?: string; // 名称
  exact?: boolean; // 是否全匹配
  disable?: boolean; // 是否显示
  routes?: Array<RouteProps>; // 子路由数据
}

/**
 * 含有子路由的路由视图组件
 * 用于页面嵌套路由使用
 * @param {RouteProps} route 路由配置
 */
const RouteWithSubRoutes = (route: RouteProps) => {
  if (route.disable) {
    return null;
  }
  return (
    <Route
      exact={route.exact}
      path={route.path}
      render={props => (
        // 返回一个匹配路由，并将子路由传入
        <route.component routeData={route} {...props} routes={route.routes} />
      )}
    />
  );
};

export default RouteWithSubRoutes;

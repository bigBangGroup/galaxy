// 可使用app.use配置多个代理
// 通过这种代理的方式，开发的时候再这里就能解决跨域问题，不需要服务端配置CORS了
const proxy = require("http-proxy-middleware");
module.exports = function(app) {
  app.use(
    proxy("/api/*/*", {
      target: "http://0.0.0.0:8999/",
      changeOrigin: true
    })
  );
  app.use(
    proxy("/api2/**", {
      target: "https://easy-mock.com/mock/5c0f31837214cf627b8d43f0/",
      changeOrigin: true
    })
  );
  app.use(
    proxy("/v1/**", {
        target: "http://budgettest.leihuo.netease.com/",
        changeOrigin: true
    })
  );
  app.use(
    proxy("/rank/**", {
        target: "http://10.246.195.216:8088/",
        changeOrigin: true
    })
  );
};


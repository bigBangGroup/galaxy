import React from "react";
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import './Login.scss';

//import
interface LoginProps extends FormComponentProps {

}

interface LoginStatus {

}

class Login extends React.Component<LoginProps, LoginStatus> {

  handleSubmit = (e:any) => {
    e.preventDefault();
    this.props.form.validateFields((err:any, values:any) => {
      if (!err) {
        localStorage.setItem("userName",values.username);
        window.location.href += 'admin/dashboard';
      }else{

      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="login-container">
        <Form onSubmit={this.handleSubmit} className="login-form">
          <Form.Item className='username'>
            {getFieldDecorator('username', {
              rules: [{ required: true, message: 'Please input "admin" or some string' }],
            })(
                <Input
                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    placeholder="input 'admin' or some string"
                />,
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('password', {
              rules: [{ required: true, message: 'Please input "admin" or some string' }],
            })(
                <Input
                    prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    type="password"
                    placeholder="input 'admin' or some string"
                />,
            )}
          </Form.Item>
          <Form.Item className='bottom'>
            {getFieldDecorator('remember', {
              valuePropName: 'checked',
              initialValue: true,
            })(<Checkbox className='color'>Remember me</Checkbox>)}
            <a className="login-form-forgot" href="">
              Forgot password
            </a>
            <div className='mt10'>
              <Button type="primary" htmlType="submit" className="login-form-button mr10">
              Log in
            </Button>
            <span className='color'>Or </span><a href="">register now!</a>
            </div>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

const WrapLogin = Form.create<LoginProps>({})(Login);
export default WrapLogin;

import React, { Component } from 'react';
import { Icon } from 'antd';
import { SketchPicker } from 'react-color';
import classNames from 'classnames';
import './ThemePicker.scss';

class Index extends Component {
    state = {
        switcherOn: false,
        background: localStorage.getItem('@primary-color') || '#313653',
    };
    _switcherOn = () => {
        this.setState({
            switcherOn: !this.state.switcherOn
        })
    };
    _handleChangeComplete = (color:any) => {
        localStorage.setItem("bgColor",color.hex);
        const headEle:any = document.querySelector('.ant-layout-header');
        headEle.style.backgroundColor = color.hex;
    };
    render() {
        const { switcherOn, background } = this.state;
        return (
            <div className={classNames('switcher dark-white theme-picker-container', { active: switcherOn })}>
                <span className="sw-btn dark-white" onClick={this._switcherOn}>
                    <Icon type="setting" className="text-dark" />
                </span>
                <div style={{ padding: 10 }} className={classNames('clear pick-theme-wrap', { active: switcherOn })}>
                    <SketchPicker
                        color={ background }
                        onChangeComplete={ this._handleChangeComplete }
                    />
                </div>
            </div>
        )
    }
}

export default Index;

/*
 *       React入口文件
 *       React react核心库
 *       ReactDom react Dom的相关方法。
 *       registerServiceWorker:加快react运行的一个js文件
 */
import React from "react";
import { HashRouter as Router, Route } from "react-router-dom";
import { Routes, RouteWithSubRoutes } from "./route";
import ThemePicker from './component/ThemePicker';
import { Provider } from 'react-redux';
import {store} from './redux';
import cookie from "./module/Dashboard/view/Home";
import watermark from 'watermark-webpage';

interface RouteProps {
  path: string; // 路由地址
  component: any; // 路由匹配组件
  exact?: boolean; // 是否全匹配
  disable?: boolean; // 是否显示
  routes?: Array<RouteProps>; // 子路由数据
}

const addWatermark = (wmContent:string) =>{
    const waterContainer:HTMLElement = (document.getElementsByClassName('router-wrap')[0] as HTMLElement);
    (window as any)._waterMark = new watermark({
        container: waterContainer || document.body,
        content: wmContent,
        fillStyle: "rgba(184, 184, 184, 0.4)",
        width: 250,
        height: 200
    });
};

//
// const App: React.FC = () => {
//     const userName = localStorage.getItem("userName");
//     userName && addWatermark(userName);
//     return (
//       <Provider store={store}>
//           <Router>
//               <div className="App">
//                   <ThemePicker />
//                   {Routes.map((route: RouteProps, index: number) => {
//                       return <RouteWithSubRoutes key={index} {...route} />;
//                   })}
//               </div>
//           </Router>
//       </Provider>
//   );
// };

class App extends React.Component{

    componentDidMount(): void {
        const userName = localStorage.getItem("userName");
        const hash = window.location.hash;
        (hash !== '#/') && userName && addWatermark(userName);
    }

    render(){
        return(
            <Provider store={store}>
                <Router>
                    <div className="App">
                        <ThemePicker />
                        {Routes.map((route: RouteProps, index: number) => {
                            return <RouteWithSubRoutes key={index} {...route} />;
                        })}
                    </div>
                </Router>
            </Provider>
        )
    }
}

export default App;

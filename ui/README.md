# note

## init project

run command

```
npx create-react-app ui --typescript
```

## install antd

1:run command

```
npm i antd --save
```

2:在 index.css 全局样式中引入 antd 的样式

```
@import '~antd/dist/antd.css';
```

3: 看文档直接使用

## import scss

To import Sass files, you first need to install node-sass.
Run `npm install node-sass` or `yarn add node-sass` inside your workspace.

```
npm install node-sass --save-dev
```

安装完之后重新 npm start 一下即可.

## prettier 插件的使用

1：下载之后在 src 文件夹下新建 .prettierrc 文件，增加 code format 的文件配置
2：为了可以在每次保存的时候自动使用 prettier 插件格式化，可在 setting.json 中加入
"editor.formatOnSave": true,

## 路由设置的一点小坑

在 module/Home 中设置路由的时候，由于页面都是在 admin 这个路由之下，
在 APP.tsx 设置管理后台的时候，应该加一个 admin（或者其他）路由，而不是直接是根路由 ’/‘
且 admin 路由的匹配是非准确匹配的，因为在所有其他的二级路由(admin/react)都需要先找到这个 admin 路由，因为二级路由的出口
是设置在 module/Home 内的右侧，只有这样先匹配到 admin 路由才能接着匹配二级路由，才能把二级路由的出口设置在 module/Home
的内部(右侧的 content 部分)

## 嵌套路由

route/Route 中是登记了该项目中所有的前段路由。
在 App.tsx 中就按照嵌套路由的固定写法配置即可。
在 admin 这个子路由中，module/Home 模块中只应该也只需要拿到 admin 及其子路由。
admin 及其子路由就是在 App.tsx 中通过固定写法的嵌套路由配置传入的。

## instal redux

run commadn

```
npm i redux react-redux redux-thunk --save
```
## egg
+ egg项目先启动
+ 本地代理 setupProxy.js 文件中重置好本地的egg服务的端口号
+ 如果重置了代理的端口号，前端需要重启一下即可


## setupProxy.js
小坑note:看egg提供的是几级接口
比如服务端提供的接口是 api/user/list
那么配置就是需要 "/api/*/*"

比如服务端提供的接口是 api/user/
那么配置就是需要 "/api/*"

## creat-react-app 打包路径问题
npm run build 之后的 build 文件夹下的index.html 路径引用不正确
可以在 package.json 中增加一个配置  
```
 "homepage": ".",
```
再打包即可解决问题

## excel
https://juejin.im/post/5d1dc5cbe51d45775f516ad0
